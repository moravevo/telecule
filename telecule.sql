-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

CREATE DATABASE `telecule` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci */;
USE `telecule`;

DELIMITER ;;

DROP EVENT IF EXISTS `Reset movie and tv show visits every month`;;
CREATE EVENT `Reset movie and tv show visits every month` ON SCHEDULE EVERY 1 MONTH STARTS '2019-02-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Aby se každý mesíc resetovaly návštevy filmu a serialu' DO UPDATE movie_and_tv_show SET visits = 0;;

DELIMITER ;

SET NAMES utf8mb4;

CREATE TABLE `cast_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `celebrity_id` int(11) NOT NULL,
  `movie_or_tv_show_id` int(11) NOT NULL,
  `premiere` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_691B8C649D12EF95` (`celebrity_id`),
  KEY `IDX_691B8C64783B55A` (`movie_or_tv_show_id`),
  CONSTRAINT `FK_691B8C64783B55A` FOREIGN KEY (`movie_or_tv_show_id`) REFERENCES `movie_and_tv_show` (`id`),
  CONSTRAINT `FK_691B8C649D12EF95` FOREIGN KEY (`celebrity_id`) REFERENCES `celebrity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=610 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `cast_member_role` (
  `cast_member_id` int(11) NOT NULL,
  `cast_role_id` int(11) NOT NULL,
  PRIMARY KEY (`cast_member_id`,`cast_role_id`),
  KEY `IDX_653DBE0034CCB80E` (`cast_member_id`),
  KEY `IDX_653DBE00C21B8195` (`cast_role_id`),
  CONSTRAINT `FK_653DBE0034CCB80E` FOREIGN KEY (`cast_member_id`) REFERENCES `cast_member` (`id`),
  CONSTRAINT `FK_653DBE00C21B8195` FOREIGN KEY (`cast_role_id`) REFERENCES `cast_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `cast_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `celebrity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` longtext COLLATE utf8mb4_unicode_ci,
  `bio` longtext COLLATE utf8mb4_unicode_ci,
  `birthday` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `visits` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=642 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `homepage_recommendation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_or_tv_show_id` int(11) DEFAULT NULL,
  `date_updated` datetime NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CABC31F783B55A` (`movie_or_tv_show_id`),
  CONSTRAINT `FK_CABC31F783B55A` FOREIGN KEY (`movie_or_tv_show_id`) REFERENCES `movie_and_tv_show` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `movie_and_tv_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` longtext COLLATE utf8mb4_unicode_ci,
  `short_description` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `premiere` datetime DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` int(11) DEFAULT NULL,
  `visits` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `movie_and_tv_show_genre` (
  `movie_or_tv_show_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_or_tv_show_id`,`genre_id`),
  KEY `IDX_2207F1894296D31F` (`genre_id`),
  KEY `IDX_2207F189783B55A` (`movie_or_tv_show_id`),
  CONSTRAINT `FK_2207F1894296D31F` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_2207F189783B55A` FOREIGN KEY (`movie_or_tv_show_id`) REFERENCES `movie_and_tv_show` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verification_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B1017252B08E074E` (`email_address`),
  UNIQUE KEY `UNIQ_B1017252E821C39F` (`verification_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `trailer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_picture` longtext COLLATE utf8mb4_unicode_ci,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci,
  `birthday` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649B08E074E` (`email_address`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  KEY `IDX_8D93D649D60322AC` (`role_id`),
  CONSTRAINT `FK_8D93D649D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `user_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `date_created` datetime NOT NULL,
  `movie_or_tv_show_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1C119AFBA76ED395` (`user_id`),
  KEY `IDX_1C119AFB783B55A` (`movie_or_tv_show_id`),
  CONSTRAINT `FK_1C119AFB783B55A` FOREIGN KEY (`movie_or_tv_show_id`) REFERENCES `movie_and_tv_show` (`id`),
  CONSTRAINT `FK_1C119AFBA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2019-03-05 22:58:45
