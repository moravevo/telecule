$(function () {
	$('[data-toggle="tooltip"]').tooltip();

	$('.selectpicker').selectpicker({
		size: 6
	});

	$('.selectpicker-live-search').selectpicker({
		liveSearch: true,
		size: 6
	});

	$('[data-toggle="datepicker"]').datepicker({
		format: 'dd. mm. yyyy'
	});

	let clicked = false;
	let dragAndScroll = $('.drag-and-scroll, .dragscroll');

	dragAndScroll.addClass('dragscroll');

	dragAndScroll.on('mousedown', function () {
		clicked = true;
	});


	dragAndScroll.parents('.dragscroll-wrapper').find('.dragscroll').append('<div class="arrow prev hidden"><i class="fas fa-arrow-left"></i></div>');
	dragAndScroll.parents('.dragscroll-wrapper').find('.dragscroll').append('<div class="arrow next hidden"><i class="fas fa-arrow-right"></i></div>');

	let dragAndScrollWidth = -12;

	dragAndScroll.find('.drag-and-scroll-item').each(function () {
		dragAndScrollWidth += $(this).width() + 12;
	});

	$(window).on('resize', function () {
		dragAndScrollWidth = -12;

		dragAndScroll.find('.drag-and-scroll-item').each(function () {
			dragAndScrollWidth += $(this).width() + 12;
		});

		if (dragAndScroll.scrollLeft() <= 0) {
			dragAndScroll.find('.arrow.prev').addClass('hidden');
		} else {
			dragAndScroll.find('.arrow.prev').removeClass('hidden');
		}

		if (dragAndScroll.scrollLeft() + dragAndScroll.parents('[class*=col]').width() === dragAndScrollWidth) {
			dragAndScroll.find('.arrow.next').addClass('hidden');
		} else {
			dragAndScroll.find('.arrow.next').removeClass('hidden');
		}
	});

	if (dragAndScroll.scrollLeft() <= 0) {
		dragAndScroll.find('.arrow.prev').addClass('hidden');
	} else {
		dragAndScroll.find('.arrow.prev').removeClass('hidden');
	}

	if (dragAndScroll.scrollLeft() + dragAndScroll.parents('[class*=col]').width() === dragAndScrollWidth) {
		dragAndScroll.find('.arrow.next').addClass('hidden');
	} else {
		dragAndScroll.find('.arrow.next').removeClass('hidden');
	}

	let animating = false;

	dragAndScroll.find('.arrow.next').on('click', function () {
		let offset = $(dragAndScroll).width();
		if (!animating) {
			dragAndScroll.animate({scrollLeft: dragAndScroll.scrollLeft() + offset - 32}, 500, function () {
				animating = false;
			});
		}
		animating = true;
	});

	dragAndScroll.find('.arrow.prev').on('click', function () {
		let offset = $(dragAndScroll).width();
		if (!animating) {
			dragAndScroll.animate({scrollLeft: dragAndScroll.scrollLeft() - offset + 32}, 500, function () {
				animating = false;
			});
		}
		animating = true;
	});

	dragAndScroll.on('scroll', function () {
		if (dragAndScroll.scrollLeft() <= 0) {
			dragAndScroll.find('.arrow.prev').addClass('hidden');
		} else {
			dragAndScroll.find('.arrow.prev').removeClass('hidden');
		}

		if (dragAndScroll.scrollLeft() + dragAndScroll.parents('[class*=col]').width() === dragAndScrollWidth) {
			dragAndScroll.find('.arrow.next').addClass('hidden');
		} else {
			dragAndScroll.find('.arrow.next').removeClass('hidden');
		}
	});

	$(window).on('mouseup', function () {
		clicked = false;
	});

	dragAndScroll.on('mousemove', function () {
		if (clicked) {
			dragAndScroll.find('a').css('pointer-events', 'none');
		} else {
			dragAndScroll.find('a').css('pointer-events', 'all');
		}
	}).on('mouseup', function () {
		dragAndScroll.find('a').css('pointer-events', 'all');
	});

	let fileInput = $('input[type=file]');

	fileInput.each(function () {
		let rawValue = $(this).val();

		if (rawValue === '') {
			$(this).siblings('label').text('Choose file...');
		} else {
			let fileName = rawValue.replace('C:\\fakepath\\', '');
			let shortenedFilename = fileName.slice(0, 35);

			if (fileName.length > 35) {
				shortenedFilename = shortenedFilename + '...';
			}

			$(this).siblings('label').text(shortenedFilename);
		}
	});

	fileInput.on('change', function () {
		let rawValue = $(this).val();
		let fileName = rawValue.replace('C:\\fakepath\\', '');
		let shortenedFilename = fileName.slice(0, 35);

		if (fileName.length > 35) {
			shortenedFilename = shortenedFilename + '...';
		}

		$(this).siblings('label').text(shortenedFilename);
	});

	let withoutRating = $('.without-rating');

	withoutRating.each(function () {
		let checked = $(this).is(':checked');

		if (checked) {
			$('#movie_and_tv_show_rating').attr('disabled', true).parents('.form-group').css('opacity', '0.5');
		} else {
			$('#movie_and_tv_show_rating').attr('disabled', false).parents('.form-group').css('opacity', '1');
		}
	});

	withoutRating.on('change', function () {
		let checked = $(this).is(':checked');

		if (checked) {
			$('#movie_and_tv_show_rating').attr('disabled', true).parents('.form-group').css('opacity', '0.5');
		} else {
			$('#movie_and_tv_show_rating').attr('disabled', false).parents('.form-group').css('opacity', '1');
		}
	});

	let rangeInput = $('input[type=range]');

	rangeInput.each(function () {
		let rawValue = $(this).val();

		$(this).removeClass('form-control');
		$(this).attr('data-original-title', rawValue);
	});

	rangeInput.on('mousemove change', function () {
		let rawValue = $(this).val();
		let tooltip = $(this).attr('aria-describedby');

		$(this).attr('data-original-title', rawValue);
		$('#' + tooltip).find('.tooltip-inner').text(rawValue);
	});

	let checkbox = $('input[type=checkbox]');

	checkbox.each(function () {
		$(this).addClass('custom-control-input').parent().addClass('custom-control custom-checkbox').find('label').addClass('custom-control-label');
	});

	let radio = $('input[type=radio]');

	radio.each(function () {
		$(this).addClass('custom-control-input').parent().addClass('custom-control custom-radio').find('label').addClass('custom-control-label');
	});

	let advancedSearch = $('.advanced-search');
	let hideable = $('.form-hideable');

	if (advancedSearch.is(':checked')) {
		hideable.show();
	} else {
		hideable.hide();
	}

	advancedSearch.on('change', function () {
		if ($(this).is(':checked')) {
			hideable.slideDown();
		} else {
			hideable.slideUp();
		}
	});

	let rangeSlider = $('.range-slider');
	let rangeSliderInput = $('.range-input');

	rangeSlider.each(function () {
		let wrapper = $(this).parents('.form-group');
		let range1 = wrapper.find('.range-input[data-input=1]').find('input');
		let range2 = wrapper.find('.range-input[data-input=2]').find('input');
		let step = range1.data('step');

		if (step === undefined) {
			step = 1;
		}

		if (range1.val() === '') {
			range1.val(range1.data('limit'));
		}

		if (range2.val() === '') {
			range2.val(range2.data('limit'));
		}

		$(this).slider({
			range: true,
			min: range1.data('limit'),
			max: range2.data('limit'),
			values: [range1.val(), range2.val()],
			step: step,
			slide: function (e, f) {
				let val1 = f.values[0];
				let val2 = f.values[1];

				range1.val(val1);
				range2.val(val2);
			}
		})
	});

	function updateSlider(e, f) {
		let target = $(f);
		let range1 = e.find('.range-input[data-input=1]');
		let range2 = e.find('.range-input[data-input=2]');
		let limit1 = parseInt(range1.find('input').attr('data-limit'));
		let limit2 = parseInt(range2.find('input').attr('data-limit'));
		let val1 = parseInt(range1.find('input').val());
		let val2 = parseInt(range2.find('input').val());

		if (val1 < limit1 || !Number.isInteger(val1)) {
			range1.find('input').val(limit1);
		}

		if (val2 > limit2 || !Number.isInteger(val2)) {
			range2.find('input').val(limit2);
		}

		if (target.attr('data-input') === range1.attr('data-input')) {
			if (val1 > val2) {
				val1 = val2;
				range1.find('input').val(val2);
			}
		} else if (target.attr('data-input') === range2.attr('data-input')) {
			if (val2 < val1) {
				val2 = val1;
				range2.find('input').val(val1);
			}
		}

		e.find('.range-slider').slider("values", [val1, val2]);
	}

	rangeSliderInput.on('change', function (e) {
		let rangeSlider = $(e.target).parents('.form-group');
		let rangeOrigin = $(this);

		updateSlider(rangeSlider, rangeOrigin);
	});

	let linkInLink = $('.link-in-link');

	linkInLink.on('click', function (e) {
		e.preventDefault();
		window.location.href = $(this).data('href');
	});

	let quickSearchButton = $('.quick-search-button');

	quickSearchButton.on('click', function () {
		$('.quick-search').addClass('displayed');
		$('.menu').find('.navbar-collapse').addClass('show');
		$('.quick-search-field').focus();
	});

	$('.quick-search-button-close').on('click', function () {
		$('.quick-search').removeClass('displayed');
		$('.menu').find('.navbar-collapse').removeClass('show');
	});

	let invalid = $('body').find('.complaint-form .is-invalid');

	if (invalid.length > 0) {
		setTimeout(function () {
			$(window).scrollTop(invalid.offset().top - 212);
		}, 10);
	}
});