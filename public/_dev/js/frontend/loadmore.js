$(function () {
	let loadmore = $('.load-more');
	let xhr;

	loadmore.on('click', function (e) {
		e.preventDefault();

		$(this).hide();

		let href = $(this).attr('href');

		if (loadmore.hasClass('load-more-celebrity')) {
			let wrapper = $(this).parents('.appearances').find('.apperances-wrapper');
			let val = wrapper.children().length;

			wrapper.append('<div class="d-flex justify-content-center mt-4 loader"><div class="spinner-border" role="status"><span class="sr-only"></span></div></div>');

			xhr = $.ajax({
				url: href,
				type: 'GET',
				dataType: 'json',
				async: true,
				data: {'count': val},
				success: function (data) {
					let count = data[0].length;
					if(count > 0) {
						for (i = 0; i < count; i++) {
							let type = data[0][i].type;
							let title = data[0][i].title;
							let thumbnail = data[0][i].thumbnail;
							let imgUrl = data[0][i].imgUrl;
							let premiere = data[0][i].premiere;
							let genres = data[0][i].genres;
							let slug = data[0][i].slug;
							let chips;

							if (genres.length > 0) {
								let start = '<div class="genres mt-2 d-none d-lg-block">';
								let end = '</div>';
								for (j = 0; j < genres.length; j++) {
									start += '<div class="chip mr-1">' + genres[j] + '</div>';
								}
								chips = start + end;
							} else {
								chips = '';
							}

							wrapper.find('.loader').remove();
							loadmore.show();

							wrapper.append(`
							<a class="card search-result-card mb-2" href="/` + type + `/` + slug + `">
								<div class="card-image">
									<img src="/uploads/` + imgUrl + `/small/thumbnail.jpeg" alt="` + thumbnail + `" data-src="/uploads/` + imgUrl + `/small/` + thumbnail + `" class="lazyload">
								</div>
								<div class="card-content">
									<h5 class="text-primary mb-1">` + title + `</h5>
									<span class="text-muted">` + premiere + `</span>
									` + chips + `
								</div>
							</a>
						`);

						}

						if (data[1]) {
							loadmore.remove();
						}
					}
				},
				error: function () {
					console.error('AJAX Request failed');
				}
			})
		} else if (loadmore.hasClass('load-more-reviews')) {
			let wrapper = $(this).parents('.user-reviews').find('.reviews-wrapper');
			let val = wrapper.children().length;

			wrapper.append('<div class="d-flex justify-content-center mt-4 loader"><div class="spinner-border" role="status"><span class="sr-only"></span></div></div>');

			xhr = $.ajax({
				url: href,
				type: 'GET',
				dataType: 'json',
				async: true,
				data: {'count': val},
				success: function (data) {
					let count = data[0].length;
					if(count > 0) {
						for (i = 0; i < count; i++) {
							let type =  data[0][i].type;
							let title = data[0][i].title;
							let thumbnail = data[0][i].thumbnail;
							let imgUrl = data[0][i].imgUrl;
							let slug = data[0][i].slug;
							let body = data[0][i].body;

							wrapper.find('.loader').remove();
							loadmore.show();

							wrapper.append(`
							<a class="card search-result-card mb-2" href="/` + type + `/` + slug + `">
								<div class="card-image">
									<img src="/uploads/` + imgUrl + `/small/thumbnail.jpeg" alt="` + thumbnail + `" data-src="/uploads/` + imgUrl + `/small/` + thumbnail + `" class="lazyload">
								</div>
								<div class="card-content">
									<h5 class="text-primary mb-1">` + title + `</h5>
									<p>` + body + `</p>
								</div>
							</a>
						`);

						}

						if (data[1]) {
							loadmore.remove();
						}
					}
				},
				error: function () {
					console.error('AJAX Request failed');
				}
			})
		} else if (loadmore.hasClass('load-more-movie')) {
			let wrapper = $(this).parents('.user-reviews').find('.reviews-wrapper');
			let val = wrapper.children().length;
			let highlightedReview = $('.highlighted-review').find('.info a').text();

			wrapper.append('<div class="d-flex justify-content-center mt-4 loader"><div class="spinner-border" role="status"><span class="sr-only"></span></div></div>');

			xhr = $.ajax({
				url: href,
				type: 'GET',
				dataType: 'json',
				async: true,
				data: {
					'count': val,
					'highlightedReview': highlightedReview
				},
				success: function (data) {
					let count = data[0].length;
					if(count > 0) {
						for (i = 0; i < count; i++) {
							let body = data[0][i].body;
							let photo = data[0][i].photo;
							let username = data[0][i].username;
							let rating = data[0][i].rating;
							let date = data[0][i].date;

							wrapper.find('.loader').remove();
							loadmore.show();

							wrapper.append(`
							<div class="card mb-3 py-3 py-md-3 px-md-4 px-3">
								<div class="card-top d-flex justify-content-between">
									<div class="left d-flex">
										<img src="/uploads/user/small/thumbnail.jpeg" data-src="/uploads/user/small/` + photo + `" alt="` + photo + `" style="width: 48px; height: 48px;" class="rounded-circle mr-3 lazyload">
										<div class="info d-flex flex-column justify-content-center align-items-start">
											<a href="/user/` + username + `">` + username +`</a>
											<span class="text-muted text-small">` + date + `</span>
										</div>
									</div>
									<div class="right d-flex">
										<div class="rating">
											<i class="fas fa-star rating-icon"></i> ` + rating + `
										</div>
									</div>
								</div>
								<div class="card-bottom mt-2">` + body + `</div>
							</div>
						`);
						}

						if (data[1]) {
							loadmore.remove();
						}
					}
				},
				error: function () {
					console.error('AJAX Request failed');
				}
			})
		}
	});
});