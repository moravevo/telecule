$(function () {
	let search = $('input[name=search]');
	let searchGroup = $('.search-group');
	let body = $('body');
	let ajaxUrl = search.attr('data-url');
	let resultTargetUrl = search.attr('data-url-target');
	let searchResultsWrapper = search.siblings('.dropdown-menu');
	let xhr;

	function searchDatabase(val) {
		if (xhr && xhr.readyState != 4) {
			xhr.abort();
		}
		xhr = $.ajax({
			url: ajaxUrl,
			type: 'POST',
			dataType: 'json',
			async: true,
			data: val,
			success: function (data) {
				if (val.length > 0) {
					let count = data[0].length;

					if (count > 0 || data[0].movies.length > 0 || data[0].tvShows.length > 0 || data[0].celebrities.length > 0) {
						searchResultsWrapper.html('<div class="hidden current-focus"></div>');

						if (data[0].type === 'quick-search') {
							let movies = $.parseJSON(data[0].movies);
							let tvShows = $.parseJSON(data[0].tvShows);
							let celebrities = $.parseJSON(data[0].celebrities);

							if (movies[0].length > 0) {
								searchResultsWrapper.append(`
									<span class="dropdown-header optgroup-1">Movies</span>
								`);

								for (let i = 0; i < movies[0].length; i++) {
									let targetUrl = resultTargetUrl;
									let premiere = movies[0][i].premiere;

									if (resultTargetUrl === undefined) {
										targetUrl = '/' + movies[0][i].targetUrl;
									} else {
										targetUrl = resultTargetUrl + '/' + movies[0][i].id;
									}

									if (premiere === null) {
										premiere = '';
									} else {
										premiere = `<span class="text-muted text-small"> (` + premiere + `)</span>`;
									}

									searchResultsWrapper.append(`
									<a class="dropdown-item" href="` + targetUrl + `">` + movies[0][i].displayValue + premiere + `</a>
								`);
								}
							}

							if (tvShows[0].length > 0) {
								searchResultsWrapper.append(`
									<span class="dropdown-header optgroup-1">TV Shows</span>
								`);

								for (let i = 0; i < tvShows[0].length; i++) {
									let targetUrl = resultTargetUrl;
									let premiere = tvShows[0][i].premiere;

									if (resultTargetUrl === undefined) {
										targetUrl = '/' + tvShows[0][i].targetUrl;
									} else {
										targetUrl = resultTargetUrl + '/' + tvShows[0][i].id;
									}

									if (premiere === null) {
										premiere = '';
									} else {
										premiere = `<span class="text-muted text-small"> (` + premiere + `)</span>`;
									}

									searchResultsWrapper.append(`
									<a class="dropdown-item" href="` + targetUrl + `">` + tvShows[0][i].displayValue + premiere + `</a>
								`);
								}
							}

							if (celebrities[0].length > 0) {
								searchResultsWrapper.append(`
									<span class="dropdown-header optgroup-1">Celebrities</span>
								`);

								for (let i = 0; i < celebrities[0].length; i++) {
									let targetUrl = resultTargetUrl;

									if (resultTargetUrl === undefined) {
										targetUrl = '/' + celebrities[0][i].targetUrl;
									} else {
										targetUrl = resultTargetUrl + '/' + celebrities[0][i].id;
									}

									searchResultsWrapper.append(`
									<a class="dropdown-item" href="` + targetUrl + `">` + celebrities[0][i].displayValue + `</a>
								`);
								}
							}

							if (movies[0].length <= 0 && tvShows[0].length <= 0 && celebrities[0].length <= 0) {
								searchResultsWrapper.text('');
								searchResultsWrapper.append(`
						<h6 class="dropdown-header">No results found</h6>
					`);
								searchResultsWrapper.css('overflow-y', 'auto');
							}
						} else {
							for (let i = 0; i < count; i++) {
								let targetUrl = resultTargetUrl;

								if (resultTargetUrl === undefined) {
									targetUrl = data[0][i].targetUrl;
								} else {
									targetUrl = resultTargetUrl + '/' + data[0][i].id;
								}

								searchResultsWrapper.append(`
									<a class="dropdown-item" href="` + targetUrl + `">` + data[0][i].displayValue + `</a>
								`);
							}
						}

						if (count > 6) {
							searchResultsWrapper.css('overflow-y', 'scroll');
						} else {
							searchResultsWrapper.css('overflow-y', 'auto');
						}
					} else {
						searchResultsWrapper.text('');
						searchResultsWrapper.append(`
							<h6 class="dropdown-header">No movies found</h6>
						`)
					}
				} else {
					searchResultsWrapper.text('');
					searchResultsWrapper.append(`
						<h6 class="dropdown-header d-none d-lg-block">Type to search...</h6>
					`);
					searchResultsWrapper.css('overflow-y', 'auto');
				}
			},
			error: function () {
				console.error('AJAX Request failed');
			}
		});
	}

	if (search.length > 0) {
		search.on('keyup', _.debounce(function () {
			let dropdown = $(this).parents('.quick-search').find('.dropdown-menu');

			if (search.val().length > 0) {
				dropdown.children().remove();
				$(this).parents('.quick-search').find('.dropdown-menu').append('<div class="d-flex justify-content-center"><div class="spinner-border text-secondary" role="status"><span class="sr-only"></span></div></div>');
				searchDatabase(search.val());
			} else {
				dropdown.children().remove();
				dropdown.append('<h6 class="dropdown-header d-none d-lg-block">Type to search...</h6>');
				if (xhr && xhr.readyState != 4) {
					xhr.abort();
				}
			}
		}, 200));
	}

	let focused = false;
	let searchParent;

	body.on('mousedown', function (e) {
		if ($(e.target).hasClass('search-group') || $(e.target).parents('.search-group').length > 0) {
			focused = true;
			$(e.target).parents('.search-group').addClass('search-group-expanded');
			searchParent = $(e.target).parents('.search-group');
		} else {
			focused = false;
			searchGroup.removeClass('search-group-expanded');
		}
	});

	body.on('keydown', function (e) {
		if (focused) {
			if (e.which === 38) {
				e.preventDefault();

				if (searchParent.find('.current-focus').prev().hasClass('dropdown-header')) {
					searchParent.find('.current-focus').removeClass('current-focus').prev().focus().addClass('current-focus');
				}

				if (!searchParent.find('.current-focus').prev('.dropdown-item').length < 1) {
					searchParent.find('.current-focus').removeClass('current-focus').prev('.dropdown-item').focus().addClass('current-focus');
				} else if (searchParent.find('.current-focus').attr('href') === undefined) {
					searchParent.find('.current-focus').removeClass('current-focus').next('.dropdown-item').focus().addClass('current-focus');
				}
			} else if (e.which === 40) {
				e.preventDefault();

				if (searchParent.find('.current-focus').next().hasClass('dropdown-header')) {
					searchParent.find('.current-focus').removeClass('current-focus').next().focus().addClass('current-focus');
				}

				if (!searchParent.find('.current-focus').next('.dropdown-item').length < 1) {
					searchParent.find('.current-focus').removeClass('current-focus').next('.dropdown-item').focus().addClass('current-focus');
				}
			} else if (e.which === 27) {
				focused = false;
				searchGroup.removeClass('search-group-expanded');
				searchGroup.find('input').blur();
			} else if (e.which != 13) {
				searchParent.find('input').focus();
			}
		}
	});
});