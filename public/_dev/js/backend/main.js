$(function () {
	$('[data-toggle="tooltip"]').tooltip();

	$('[data-toggle="datepicker"]').datepicker({
		format: 'dd. mm. yyyy'
	});

	$('.selectpicker').selectpicker({
		size: 6
	});

	$('.selectpicker-live-search').selectpicker({
		liveSearch: true,
		size: 6
	});

	let $anchor = window.location.hash.substr(1);
	let $anchorHolder = $('a.anchor-holder');

	if ($anchor != undefined && $anchor.indexOf("tab") === 0) {
		$(".tab-pane").hide();
		$("#" + $anchor).show();
		$("#tabs a.active").removeClass("active");
		$("#tabs a[href='#" + $anchor + "']").addClass("active");

		$(window).scrollTop(0);
	} else {
		$anchor = 'tab-information';
	}

	$anchorHolder.each(function () {
		$(this).attr('data-anchor', $(this).attr('href') + '/' + $anchor);
		$(this).attr('href', $(this).attr('data-anchor'));
	});

	$("#tabs a").click(function (e) {
		let toShow = $(this).attr('href').substr(1);
		$(".tab-pane").hide();
		$("#tabs a.active").removeClass("active");
		$(this).addClass("active");
		$("#" + toShow).show();
		$anchorHolder.each(function () {
			$(this).attr('data-anchor', $(this).attr('href').substr(0, $(this).attr('href').lastIndexOf('/tab')) + '/' + toShow);
			$(this).attr('href', $(this).attr('data-anchor'));
		});

		e.preventDefault();
		window.location = '#' + toShow;
		$(window).scrollTop(0);
	});

	$(window).on('hashchange', function () {
		$anchor = window.location.hash.substr(1);

		if ($anchor === '') {
			$anchor = 'tab-information';
		}

		$(".tab-pane").hide();
		$("#" + $anchor).show();
		$("#tabs a.active").removeClass("active");
		$("#tabs a[href='#" + $anchor + "']").addClass("active");

		$(window).scrollTop(0);
	});

	let $clicked = false;
	let $dragscroll = $('.dragscroll');

	$dragscroll.on('mousedown', function () {
		$clicked = true;
	});

	$(window).on('mouseup', function () {
		$clicked = false;
	});

	$dragscroll.on('mousemove', function () {
		if ($clicked) {
			$dragscroll.find('a').addClass('disable-pointer-events');
		} else {
			$dragscroll.find('a').removeClass('disable-pointer-events');
		}
	}).on('mouseup', function () {
		$dragscroll.find('a').removeClass('disable-pointer-events');
	});

	let rangeSlider = $('.range-slider');
	let rangeInput = $('.range-input');

	rangeSlider.each(function () {
		let wrapper = $(this).parents('.form-group');
		let range1 = wrapper.find('.range-input[data-input=1]').find('input');
		let range2 = wrapper.find('.range-input[data-input=2]').find('input');
		let step = range1.data('step');

		if (step === undefined) {
			step = 1;
		}

		if (range1.val() === '') {
			range1.val(range1.data('limit'));
		}

		if (range2.val() === '') {
			range2.val(range2.data('limit'));
		}

		$(this).slider({
			range: true,
			min: range1.data('limit'),
			max: range2.data('limit'),
			values: [range1.val(), range2.val()],
			step: step,
			slide: function (e, f) {
				let val1 = f.values[0];
				let val2 = f.values[1];

				range1.val(val1);
				range2.val(val2);
			}
		})
	});

	function updateSlider(e, f) {
		let target = $(f);
		let range1 = e.find('.range-input[data-input=1]');
		let range2 = e.find('.range-input[data-input=2]');
		let val1 = parseInt(range1.find('input').val());
		let val2 = parseInt(range2.find('input').val());

		if (target.attr('data-input') === range1.attr('data-input')) {
			if (val1 > val2) {
				val1 = val2;
				range1.find('input').val(val2);
			}
		} else if (target.attr('data-input') === range2.attr('data-input')) {
			if (val2 < val1) {
				val2 = val1;
				range2.find('input').val(val1);
			}
		}

		e.find('.range-slider').slider("values", [val1, val2]);
	}

	rangeInput.on('change', function (e) {
		let rangeSlider = $(e.target).parents('.form-group');
		let rangeOrigin = $(this);

		updateSlider(rangeSlider, rangeOrigin);
	});

	let customCheckAll = $('#customCheckAll');
	let customCheck = $('input[id*="customCheck"]');

	customCheckAll.on('change', function () {
		$('table').find('input[id*="customCheck"]').prop('checked', $(this).is(':checked'));
	});

	customCheck.on('change', function () {
		let boxes = $('table').find('input[id*="customCheck"]:not("#customCheckAll")');
		let checked = [];
		let ids = [];
		let href = $('table').find('input[id="customCheckAll"]').data('url');

		boxes.each(function () {
			if ($(this).is(':checked')) {
				checked.push($(this));
				ids.push($(this).data('id'));
			}
		});

		if (!_.isEmpty(checked)) {
			if ($('.checkedRowsHolder').length > 0) {
				$('.checkedRowsHolder').html('<span class="text-secondary">Selected items: ' + checked.length + '</span>');
				$('.checkedRowsHolder').append('<a class="btn btn-sm btn-danger text-white ml-4" href="' + href + '?ids=[' + ids + ']">Delete</a>');

				if ($('table').hasClass('trailers')) {
					$('.checkedRowsHolder').append('<a class="btn btn-sm btn-secondary text-white ml-1" href="' + href.replace('remove', 'toggle') + '?ids=[' + ids + ']">Toggle state</a>');
				}
			} else {
				$('body .container-fluid').append('<div class="checkedRowsHolder"></div>');
				$('.checkedRowsHolder').html('<span class="text-secondary">Selected items: ' + checked.length + '</span>');
				$('.checkedRowsHolder').append('<a class="btn btn-sm btn-danger text-white ml-4" href="' + href + '?ids=[' + ids + ']">Delete</a>');

				if ($('table').hasClass('trailers')) {
					$('.checkedRowsHolder').append('<a class="btn btn-sm btn-secondary text-white ml-1" href="' + href.replace('remove', 'toggle') + '?ids=[' + ids + ']">Toggle state</a>');
				}
			}
		} else {
			$('.checkedRowsHolder').remove();
		}
	});

	let save = $('#save');
	let href;

	if (save.length > 0) {
		href = save.attr('href');
	}

	$("#sortable").sortable({
		update: function () {
			let ids = [];
			$('#sortable').find('.ui-state-default').each(function () {
				ids.push($(this).data('id'));
			});

			save.attr('href', href + '?ids=[' + ids + ']')
		}
	});
});
