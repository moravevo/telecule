$(function () {
	let fileInput = $('input[type=file]');

	fileInput.each(function () {
		let rawValue = $(this).val();

		if (rawValue === '') {
			$(this).siblings('label').text('Choose file...');
		} else {
			let fileName = rawValue.replace('C:\\fakepath\\', '');
			let shortenedFilename = fileName.slice(0, 35);

			if (fileName.length > 35) {
				shortenedFilename = shortenedFilename + '...';
			}

			$(this).siblings('label').text(shortenedFilename);
		}
	});

	fileInput.on('change', function () {
		let rawValue = $(this).val();
		let fileName = rawValue.replace('C:\\fakepath\\', '');
		let shortenedFilename = fileName.slice(0, 35);

		if (fileName.length > 35) {
			shortenedFilename = shortenedFilename + '...';
		}

		$(this).siblings('label').text(shortenedFilename);
	});

	let withoutRating = $('.without-rating');

	withoutRating.each(function () {
		let checked = $(this).is(':checked');

		if (checked) {
			$('#movie_and_tv_show_rating').attr('disabled', true).parents('.form-group').css('opacity', '0.5');
		} else {
			$('#movie_and_tv_show_rating').attr('disabled', false).parents('.form-group').css('opacity', '1');
		}
	});

	withoutRating.on('change', function () {
		let checked = $(this).is(':checked');

		if (checked) {
			$('#movie_and_tv_show_rating').attr('disabled', true).parents('.form-group').css('opacity', '0.5');
		} else {
			$('#movie_and_tv_show_rating').attr('disabled', false).parents('.form-group').css('opacity', '1');
		}
	});

	let rangeInput = $('input[type=range]');

	rangeInput.each(function () {
		let rawValue = $(this).val();

		$(this).removeClass('form-control');
		$(this).attr('data-original-title', rawValue);
	});

	rangeInput.on('mousemove change', function () {
		let rawValue = $(this).val();
		let tooltip = $(this).attr('aria-describedby');

		$(this).attr('data-original-title', rawValue);
		$('#' + tooltip).find('.tooltip-inner').text(rawValue);
	});

	let checkbox = $('input[type=checkbox]');

	checkbox.each(function () {
		$(this).addClass('custom-control-input').parent().addClass('custom-control custom-checkbox').find('label').addClass('custom-control-label');
	});

	let radio = $('input[type=radio]');

	radio.each(function () {
		$(this).addClass('custom-control-input').parent().addClass('custom-control custom-radio').find('label').addClass('custom-control-label');
	});

	let advancedSearch = $('.advanced-search');
	let hideable = $('.form-hideable');

	if (advancedSearch.is(':checked')) {
		hideable.show();
	} else {
		hideable.hide();
	}

	advancedSearch.on('change', function () {
		if ($(this).is(':checked')) {
			hideable.slideDown();
		} else {
			hideable.slideUp();
		}
	});
});
