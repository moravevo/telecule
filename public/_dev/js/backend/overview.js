$(function () {
	let movieVisitsChart = $('#movieVisitsChart');
	let titles = [];
	let visits = [];

	if (movieVisitsChart.length > 0) {
		let movies = $.ajax({
			url: '/chart/movieAndTvShowVisits',
			type: 'POST',
			dataType: 'json',
			async: true,
			success: function (data) {
				for (let i = 0; i < data[0].length; i++) {
					titles.push(data[0][i]['title']);
					visits.push(data[0][i]['visits']);
				}

				let chart = new Chart(movieVisitsChart, {
					type: 'bar',
					data: {
						labels: titles,
						datasets: [{
							label: 'Visits',
							data: visits,
							backgroundColor: [
								'rgba(255,99,132,1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255,99,132,1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)'
							]
						}]
					},
					options: {
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true,
									callback: function(value) {if (value % 1 === 0) {return value;}}
								}
							}]
						}
					}
				});
			},
			error: function () {
				console.error('AJAX Request failed');
			}
		});
	}
});