$(function () {
	let $table = $('table');

	if ($table.length > 0) {
		$table.each(function () {
			let $url = $(this).data('url');
			let $get = $(this).data('get');
			let $sort;
			let $order;
			let $page;
			let $sorter;

			if ($get) {
				$sort = '?sort=' + $get['sort'];
				$order = '&order=' + $get['order'];
				$page = '&page=';
				$sorter = $('a[data-sortable]');

				if ($get.pageCount > 1) {
					$(this).after(`
					<nav id="pagination" class="d-flex align-items-start">
						<ul class="pagination"></ul>
						<div class="go-to-page d-none align-items-center ml-4">
							<label for="go-to-page" class="m-0 mr-1">Page</label>
							<input type="text" class="form-control" id="go-to-page" style="width: 64px;">
						</div>
					</nav>
				`);

					let $pagination = $(this).siblings('#pagination').find('.pagination');
					let $previousPage = $get['page'] - 1;
					let $disablePreviousPage = '';
					let $nextPage = parseInt($get['page']) + 1;
					let $disableNextPage = '';

					if ($previousPage < 1) {
						$previousPage = 1;
						$disablePreviousPage = ' disabled';
					}

					if ($nextPage > $get['pageCount']) {
						$nextPage = $get['pageCount'];
						$disableNextPage = ' disabled';
					}

					$pagination.append(`
					<li class="page-item` + $disablePreviousPage + `"><a class="page-link" href="` + $url + '/' + $sort + $order + $page + $previousPage + `">Previous</a></li>
				`);

					let $i = 1;
					let $limit = 2;
					let $limit2 = $get.pageCount;

					if ($limit2 >= 5) {
						$limit2 = 5;
					}

					if ($limit - $get.page < 0) {
						$i = -($limit - $get.page);
						$limit2 = parseInt($get.page) + 2;

						if ($limit2 >= $get.pageCount) {
							$limit2 = $get.pageCount;
						}

						if ($i > $get.pageCount - 5) {
							$i = $get.pageCount - 4;
						}
					}

					for ($i; $i <= $limit2; $i++) {
						let $active = '';
						if ($i === parseInt($get['page'])) {
							$active = ' active';
						}

						$pagination.append(`
						<li class="page-item` + $active + `"><a class="page-link" href="` + $url + '/' + $sort + $order + $page + $i + `">` + $i + `</a></li>
				`);
					}

					$pagination.append(`
					<li class="page-item` + $disableNextPage + `"><a class="page-link" href="` + $url + '/' + $sort + $order + $page + $nextPage + `">Next</a></li>
				`);
				}
			}

			if ($sorter) {
				$sorter.each(function () {
					if (!$get['sortParameter']) {
						$(this).append(`
						<i class="fas fa-sort"></i>
					`);
					}

					let $sorterOrder = 'asc';

					if ($(this).data('sortable') === $get['sortParameter']) {
						if ($get['order'] === 'asc') {
							$(this).find('i.fas.fa-sort').remove();
							$(this).append(`
							<i class="fas fa-sort-up" style="position: relative; top: 3px;"></i>
						`);

							$sorterOrder = 'desc';
						} else if ($get['order'] === 'desc') {
							$(this).find('i.fas.fa-sort').remove();
							$(this).append(`
							<i class="fas fa-sort-down" style="position: relative; bottom: 3px;"></i>
						`);
						}
					}

					$(this).attr('href', $url + '/' + '?sort=' + $(this).data('sortable') + '&order=' + $sorterOrder + $page + $get['page']);
				});
			}
		});
	}
});