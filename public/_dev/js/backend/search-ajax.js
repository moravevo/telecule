$(function () {
	let search = $('input[name=search], .quick-search-field');
	let searchGroup = $('.search-group');
	let body = $('body');
	let ajaxUrl = search.attr('data-url');
	let resultTargetUrl = search.attr('data-url-target');
	let searchResultsWrapper = search.siblings('.dropdown-menu');
	let xhr;

	function searchDatabase(val) {
		if (xhr && xhr.readyState != 4) {
			xhr.abort();
		}
		xhr = $.ajax({
			url: ajaxUrl,
			type: 'POST',
			dataType: 'json',
			async: true,
			data: val,
			success: function (data) {
				if (val.length > 0) {
					let count = data[0].length;

					if (count > 0) {
						searchResultsWrapper.html('<div class="hidden current-focus"></div>');

						for (let i = 0; i < count; i++) {
							searchResultsWrapper.append(`
								<a class="dropdown-item" href="` + resultTargetUrl + '/' + data[0][i].id + `">` + data[0][i].displayValue + `</a>
							`);
						}

						if (count > 6) {
							searchResultsWrapper.css('overflow-y', 'scroll');
						} else {
							searchResultsWrapper.css('overflow-y', 'auto');
						}
					} else {
						searchResultsWrapper.text('');
						searchResultsWrapper.append(`
							<h6 class="dropdown-header">No movies found</h6>
						`)
					}
				} else {
					searchResultsWrapper.text('');
					searchResultsWrapper.append(`
						<h6 class="dropdown-header">Type to search...</h6>
					`);
					searchResultsWrapper.css('overflow-y', 'auto');
				}
			},
			error: function () {
				console.error('AJAX Request failed');
			}
		});
	}

	if (search.length > 0) {
		search.on('keyup', _.debounce(function () {
			let dropdown = $(this).parents('.search-group').find('.dropdown-menu');

			if (search.val().length > 0) {
				dropdown.children().remove();
				$(this).parents('.search-group').find('.dropdown-menu').append('<div class="d-flex justify-content-center"><div class="spinner-border text-secondary" role="status"><span class="sr-only"></span></div></div>');
				searchDatabase(search.val());
			} else {
				dropdown.children().remove();
				dropdown.append('<h6 class="dropdown-header d-none d-lg-block">Type to search...</h6>');
				if (xhr && xhr.readyState != 4) {
					xhr.abort();
				}
			}
		}, 200));
	}

	let focused = false;
	let searchParent;

	body.on('mousedown', function (e) {
		if ($(e.target).hasClass('search-group') || $(e.target).parents('.search-group').length > 0) {
			focused = true;
			$(e.target).parents('.search-group').addClass('search-group-expanded');
			searchParent = $(e.target).parents('.search-group');
		} else {
			focused = false;
			searchGroup.removeClass('search-group-expanded');
		}
	});

	body.on('keydown', function (e) {
		if (focused) {
			$('.search-group').addClass('search-group-expanded');
			if (e.which === 38) {
				e.preventDefault();
				if ( ! searchParent.find('.current-focus').prev('.dropdown-item').length < 1) {
					searchParent.find('.current-focus').removeClass('current-focus').prev('.dropdown-item').focus().addClass('current-focus');
				} else if (searchParent.find('.current-focus').attr('href') === undefined) {
					searchParent.find('.current-focus').removeClass('current-focus').next('.dropdown-item').focus().addClass('current-focus');
				}
			} else if (e.which === 40) {
				e.preventDefault();
				if ( ! searchParent.find('.current-focus').next('.dropdown-item').length < 1) {
					searchParent.find('.current-focus').removeClass('current-focus').next('.dropdown-item').focus().addClass('current-focus');
				}
			} else if (e.which === 8 || e.which === 32 || e.which === 37 || e.which === 39) {
				searchParent.find('input').focus();
			}
		}
	});
});