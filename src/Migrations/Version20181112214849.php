<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181112214849 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            UPDATE celebrity SET picture = \'thumbnail.png\' WHERE picture IS NULL
        ');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
