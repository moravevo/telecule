<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181108152529 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie_and_tv_show_cast (movie_or_tv_show_id INT NOT NULL, celebrity_id INT NOT NULL, INDEX IDX_94E9B657783B55A (movie_or_tv_show_id), INDEX IDX_94E9B6579D12EF95 (celebrity_id), PRIMARY KEY(movie_or_tv_show_id, celebrity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_and_tv_show_cast ADD CONSTRAINT FK_94E9B657783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_cast ADD CONSTRAINT FK_94E9B6579D12EF95 FOREIGN KEY (celebrity_id) REFERENCES celebrity (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F1896669CB06');
        $this->addSql('DROP INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre CHANGE movie_or_tvshow_id movie_or_tv_show_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F189783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('CREATE INDEX IDX_2207F189783B55A ON movie_and_tv_show_genre (movie_or_tv_show_id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD PRIMARY KEY (movie_or_tv_show_id, genre_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE movie_and_tv_show_cast');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F189783B55A');
        $this->addSql('DROP INDEX IDX_2207F189783B55A ON movie_and_tv_show_genre');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre CHANGE movie_or_tv_show_id movie_or_tvshow_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F1896669CB06 FOREIGN KEY (movie_or_tvshow_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('CREATE INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre (movie_or_tvshow_id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD PRIMARY KEY (movie_or_tvshow_id, genre_id)');
    }
}
