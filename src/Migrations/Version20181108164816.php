<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181108164816 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cast_member (id INT AUTO_INCREMENT NOT NULL, celebrity_id INT NOT NULL, movie_or_tv_show_id INT NOT NULL, celebrity_role VARCHAR(255) DEFAULT NULL, INDEX IDX_691B8C649D12EF95 (celebrity_id), INDEX IDX_691B8C64783B55A (movie_or_tv_show_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cast_member ADD CONSTRAINT FK_691B8C649D12EF95 FOREIGN KEY (celebrity_id) REFERENCES celebrity (id)');
        $this->addSql('ALTER TABLE cast_member ADD CONSTRAINT FK_691B8C64783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('DROP TABLE movie_and_tv_show_cast');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE movie_and_tv_show_cast (movie_or_tv_show_id INT NOT NULL, celebrity_id INT NOT NULL, INDEX IDX_94E9B657783B55A (movie_or_tv_show_id), INDEX IDX_94E9B6579D12EF95 (celebrity_id), PRIMARY KEY(movie_or_tv_show_id, celebrity_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_and_tv_show_cast ADD CONSTRAINT FK_94E9B657783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_cast ADD CONSTRAINT FK_94E9B6579D12EF95 FOREIGN KEY (celebrity_id) REFERENCES celebrity (id)');
        $this->addSql('DROP TABLE cast_member');
    }
}
