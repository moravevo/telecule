<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012153748 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_review DROP FOREIGN KEY FK_1C119AFB783B55A');
        $this->addSql('ALTER TABLE user_review CHANGE movie_or_tv_show_id movie_or_tv_show_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_review ADD CONSTRAINT FK_1C119AFB783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_review DROP FOREIGN KEY FK_1C119AFB783B55A');
        $this->addSql('ALTER TABLE user_review CHANGE movie_or_tv_show_id movie_or_tv_show_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_review ADD CONSTRAINT FK_1C119AFB783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES role (id)');
    }
}
