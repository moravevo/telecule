<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012152018 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_review DROP target_id, CHANGE rating rating DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F1896669CB06');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP PRIMARY KEY');
        $this->addSql('DROP INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre CHANGE movie_or_tv_show_id movie_or_tvshow_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F1896669CB06 FOREIGN KEY (movie_or_tvshow_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD PRIMARY KEY (movie_or_tvshow_id, genre_id)');
        $this->addSql('CREATE INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre (movie_or_tvshow_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F1896669CB06');
        $this->addSql('DROP INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre CHANGE movie_or_tvshow_id movie_or_tv_show_id INT NOT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F1896669CB06 FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('CREATE INDEX IDX_2207F1896669CB06 ON movie_and_tv_show_genre (movie_or_tv_show_id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD PRIMARY KEY (movie_or_tv_show_id, genre_id)');
        $this->addSql('ALTER TABLE user_review ADD target_id INT NOT NULL, CHANGE movie_or_tv_show_id movie_or_tv_show_id INT DEFAULT NULL, CHANGE rating rating INT NOT NULL');
    }
}
