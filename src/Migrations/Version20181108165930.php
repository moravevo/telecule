<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181108165930 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cast_role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cast_member ADD cast_role_id INT DEFAULT NULL, ADD celebrity_role_id INT NOT NULL, DROP celebrity_role');
        $this->addSql('ALTER TABLE cast_member ADD CONSTRAINT FK_691B8C64C21B8195 FOREIGN KEY (cast_role_id) REFERENCES cast_role (id)');
        $this->addSql('CREATE INDEX IDX_691B8C64C21B8195 ON cast_member (cast_role_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cast_member DROP FOREIGN KEY FK_691B8C64C21B8195');
        $this->addSql('DROP TABLE cast_role');
        $this->addSql('DROP INDEX IDX_691B8C64C21B8195 ON cast_member');
        $this->addSql('ALTER TABLE cast_member ADD celebrity_role VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP cast_role_id, DROP celebrity_role_id');
    }
}
