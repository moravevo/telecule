<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012150315 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, email_address VARCHAR(255) NOT NULL, profile_picture LONGTEXT NOT NULL, password VARCHAR(255) NOT NULL, bio LONGTEXT DEFAULT NULL, birthday DATETIME NOT NULL, date_created DATETIME NOT NULL, role_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_review (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, target_id INT NOT NULL, body LONGTEXT NOT NULL, rating INT NOT NULL, date_created DATETIME NOT NULL, INDEX IDX_1C119AFBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_and_tv_show (id INT AUTO_INCREMENT NOT NULL, title LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, thumbnail LONGTEXT DEFAULT NULL, short_description LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, premiere DATETIME DEFAULT NULL, rating DOUBLE PRECISION DEFAULT NULL, date_created DATETIME NOT NULL, slug LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_and_tv_show_genre (movie_or_tv_show_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_2207F1896669CB06 (movie_or_tv_show_id), INDEX IDX_2207F1894296D31F (genre_id), PRIMARY KEY(movie_or_tv_show_id, genre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, date_created DATETIME NOT NULL, author_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_review ADD CONSTRAINT FK_1C119AFBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F1896669CB06 FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre ADD CONSTRAINT FK_2207F1894296D31F FOREIGN KEY (genre_id) REFERENCES genre (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_review DROP FOREIGN KEY FK_1C119AFBA76ED395');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F1896669CB06');
        $this->addSql('ALTER TABLE movie_and_tv_show_genre DROP FOREIGN KEY FK_2207F1894296D31F');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_review');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE movie_and_tv_show');
        $this->addSql('DROP TABLE movie_and_tv_show_genre');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE article');
    }
}
