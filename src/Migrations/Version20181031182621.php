<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181031182621 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE homepage_recommendation (id INT AUTO_INCREMENT NOT NULL, movie_or_tv_show_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_CABC31F783B55A (movie_or_tv_show_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE homepage_recommendation ADD CONSTRAINT FK_CABC31F783B55A FOREIGN KEY (movie_or_tv_show_id) REFERENCES movie_and_tv_show (id)');
        $this->addSql('ALTER TABLE movie_and_tv_show ADD homepage_recommendation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie_and_tv_show ADD CONSTRAINT FK_FBA93501D0E8DF9C FOREIGN KEY (homepage_recommendation_id) REFERENCES homepage_recommendation (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBA93501D0E8DF9C ON movie_and_tv_show (homepage_recommendation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie_and_tv_show DROP FOREIGN KEY FK_FBA93501D0E8DF9C');
        $this->addSql('DROP TABLE homepage_recommendation');
        $this->addSql('DROP INDEX UNIQ_FBA93501D0E8DF9C ON movie_and_tv_show');
        $this->addSql('ALTER TABLE movie_and_tv_show DROP homepage_recommendation_id');
    }
}
