<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109074207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
		$array = ['Ryan Coogler', 'Stan Lee', 'Jack Kirby', 'Joe Robert Cole', 'Rachel Morrison', 'Ludwig Göransson', 'Chadwick Boseman', 'Micheal B. Jordan', 'Lupita Nyong\'o', 'Danai Gurira', 'Martin Freeman', 'Daniel Kaluuya', 'Letitia Wright', 'Kenneth Branagh', 'Larry Lieber', 'Don Payne', 'Haris Zambarloukos', 'Patrick Doyle', 'Chris Hemsworth', 'Tom Hiddleston', 'Anthony Hopkins', 'Natalie Portman', 'Colm Feore', 'Jaimie Alexander', 'Stellan Skarsgard', 'Adriana Barraza', 'Chad Stahelski', 'David Leitch', 'Derek Kolstad', 'Jonathan Sela', 'Tyler Bates', 'Joel J. Richard', 'Keanu Reeves', 'Michael Nyqvist', 'Alfie Allen', 'Adrianne Palicki', 'Bridget Moynahan', 'Dean Winters', 'Lance Reddick', 'Toby Leonard Moore', 'Ian McShane', 'John Leguizamo', 'Willem Dafoe', 'Peter Jackson', 'John Ronald Reuel Tolkien', 'Fran Walsh', 'Phillipa Boyens', 'Stephen Sicnlair', 'Andrew Lesnie', 'Howard Shore', 'Elijah Wood', 'Ian McKellen', 'Viggo Mortensen', 'Sean Astin', 'Liv Tyler', 'Billy Boyd', 'John Rhys-Davies', 'Dominic Monaghan', 'Christopher Lee', 'Miranda Otto', 'Brad Dourif', 'Ruben Fleischer', 'Scot Rosenberg', 'Jeff Pinkner', 'Kelly Marcel', 'Will Beall', 'Matthew Libatique', 'Tom Hardy', 'Michelle Williams', 'Riz Ahmed', 'Jenny Slate', 'Woody Harrelson', 'Michelle Lee', 'Sope Aluko', 'Reid Scott', 'Scott Haze', 'John Favreau', 'Mark Fergus', 'Hawk Ostby', 'Matt Holloway', 'Robert Downey Jr.', 'Terrence Howard', 'Jeff Bridges', 'Gwyneth Paltrow', 'Leslie Bibb', 'Shaun Toub', 'Faran Tahir', 'Clack Gregg', 'Bill Smitrovich', 'Justin Theorux', 'John Debney', 'Mickey Rourke', 'Samuel L. Jackson', 'Scarlett Johansson', 'Don Cheadle', 'Sam Rockwell', 'Kata Mara', 'Olivia Munn', 'Tim Guinee', 'Peyton Reed', 'Chris McKenna', 'Erik Sommers', 'Paul Rudd', 'Andrew Barrer', 'Gabriel Ferrari', 'Dante Spinotti', 'Christophe Beck', 'Evangeline Lilly', 'Michael Douglas', 'Michelle Pfeiffer', 'Michael Peña', 'Walton Goggins', 'Hannah John-Kamen', 'Laruence Fishburne', 'Joe Cornish', 'Edgar Wright', 'Adam McKay', 'Russel Carpenter', 'Corey Stoll', 'Bobby Cannavale', 'Judy Geer', 'John Slattery', 'Joe Chrest', 'Christopher Nolan', 'Wally Pfister', 'Hans Zimmer', 'Leonardo DiCaprio', 'Joseph Gordon-Levitt', 'Ellen Page', 'Ken Watanabe', 'Dileep Rao', 'Cillian Murphy', 'Tom Berenger'];

		$today = new \DateTime();

		foreach ($array as $person) {
			$this->addSql('INSERT INTO celebrity (full_name, date_created) VALUES(:full_name, :date_created)', [
				'full_name' => $person,
				'date_created' => $today->format('Y-m-d')
			]);
		}
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
