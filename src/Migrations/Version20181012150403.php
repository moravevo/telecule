<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012150403 extends AbstractMigration
{
	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('
			INSERT INTO role VALUES (NULL, \'guest\', 0), (NULL, \'admin\', 1), (NULL, \'superadmin\', 2)
		');
		$this->addSql('
			INSERT INTO user VALUES (NULL, \'Vojtěch\', \'Morávek\', \'vojtech.moravek\', \'vojtech.moravek@email.cz\', \'06daefa43534cb494c15ade16a840a30.jpeg\', \'$2y$10$1Hg38lxuetUD52B/RYb9MObaZFHj2WJlkSD.2nRDWhKYeaa60S8fe\', \'I like to eat and sleep\', \'2000-06-16\', \'2018-10-12\', 2)
		');
    }

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs

	}
}
