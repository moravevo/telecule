<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190127202754 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cast_member_role DROP FOREIGN KEY FK_653DBE00C21B8195');
        $this->addSql('ALTER TABLE cast_member_role ADD CONSTRAINT FK_653DBE00C21B8195 FOREIGN KEY (cast_role_id) REFERENCES cast_role (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cast_member_role DROP FOREIGN KEY FK_653DBE00C21B8195');
        $this->addSql('ALTER TABLE cast_member_role ADD CONSTRAINT FK_653DBE00C21B8195 FOREIGN KEY (cast_role_id) REFERENCES cast_role (id)');
    }
}
