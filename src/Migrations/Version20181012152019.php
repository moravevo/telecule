<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012152019 extends AbstractMigration
{
	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('
			INSERT INTO movie_and_tv_show VALUES (NULL, \'Test Movie\', \'movie\', \'thumbnail.png\', \'This is only a short description of a movie meant for testing the functionality.\', \'This is a full description of the movie. It should be longer than the short description so I had to write so much of nonsensical text. Sorry.\', \'2018-10-12\', 8.2, \'2018-10-12\', \'test-movie\'), (NULL, \'Test TV show\', \'tv_show\', \'thumbnail.png\', \'This is only a short description of a tv show meant for testing the functionality.\', \'This is a full description of the tv show. It should be longer than the short description so I had to write so much of nonsensical text. Sorry.\', \'2018-10-12\', 9.6, \'2018-10-12\', \'test-tv-show\')
		');

		$this->addSql('
			INSERT INTO genre VALUES
				(NULL, \'Action\'),
				(NULL, \'Adventure\'),
				(NULL, \'Comedy\'),
				(NULL, \'Crime\'),
				(NULL, \'Drama\'),
				(NULL, \'Horror\'),
				(NULL, \'Mystery\'),
				(NULL, \'Musical\'),
				(NULL, \'Sci-Fi\'),
				(NULL, \'War\'),
				(NULL, \'Western\')
		');

		$this->addSql('
			INSERT INTO user_review VALUES
				(NULL, 1, \'This is a test user review for a movie\', 7.6, \'2018-10-12\', 1),
				(NULL, 1, \'This is a test user review for a tv show\', 8.8, \'2018-10-12\', 2)
		');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs

	}
}
