<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrailerRepository")
 */
class Trailer
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="This field is required")
	 */
	private $title;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="This field is required")
	 */
	private $url;


	/**
	 * @ORM\Column(type="text")
	 * @Assert\Image(
	 *     maxSize = "2M"
	 * )
	 * @Assert\NotNull(message="This field is required")
	 */
	private $thumbnail;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $active;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $ordering;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getUrl(): ?string
	{
		return $this->url;
	}

	public function setUrl(string $url): self
	{
		$this->url = $url;

		return $this;
	}

	public function getThumbnail(): ?string
	{
		return $this->thumbnail;
	}

	public function setThumbnail(string $thumbnail): self
	{
		$this->thumbnail = $thumbnail;

		return $this;
	}

	public function getActive(): ?bool
	{
		return $this->active;
	}

	public function setActive(bool $active): self
	{
		$this->active = $active;

		return $this;
	}

	public function getOrdering(): ?int
	{
		return $this->ordering;
	}

	public function setOrdering(int $ordering): self
	{
		$this->ordering = $ordering;

		return $this;
	}

	public function __construct()
	{
		$this->active = TRUE;
	}
}
