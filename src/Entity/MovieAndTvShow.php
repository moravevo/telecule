<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieAndTvShowRepository")
 */
class MovieAndTvShow
{
	/**
	 * @ORM\ManyToMany(targetEntity="Genre")
	 * @ORM\JoinTable(name="movie_and_tv_show_genre",
	 *      joinColumns={@ORM\JoinColumn(name="movie_or_tv_show_id", referencedColumnName="id", onDelete="CASCADE")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="genre_id", referencedColumnName="id", onDelete="CASCADE")}
	 * )
	 */
	public $genres;

	/**
	 * @ORM\OneToMany(targetEntity="UserReview", mappedBy="movieOrTvShow")
	 * @OrderBy({"dateCreated" = "DESC"})
	 */
	private $userReviews;

	/**
	 * @ORM\OneToOne(targetEntity="HomepageRecommendation", mappedBy="movieOrTvShow")
	 */
	private $homepageRecommendation;

	/**
	 * @ORM\OneToMany(targetEntity="CastMember", mappedBy="movieOrTvShow", orphanRemoval=true)
	 */
	private $castMembers;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="This field is required")
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $type;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\Image(maxSize = "2M")
	 */
	private $thumbnail;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $shortDescription;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $premiere;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $rating;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateCreated;

	/**
	 * @ORM\Column(type="text")
	 */
	private $slug;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $length;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $visits;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getType(): ?string
	{
		return $this->type;
	}

	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	public function getThumbnail(): ?string
	{
		return $this->thumbnail;
	}

	public function setThumbnail(?string $thumbnail): self
	{
		$this->thumbnail = $thumbnail;

		return $this;
	}

	public function getShortDescription(): ?string
	{
		return $this->shortDescription;
	}

	public function setShortDescription(?string $shortDescription): self
	{
		$this->shortDescription = $shortDescription;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(?string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getPremiere(): ?\DateTimeInterface
	{
		return $this->premiere;
	}

	public function setPremiere(?\DateTimeInterface $premiere): self
	{
		$this->premiere = $premiere;

		return $this;
	}

	public function getRating(): ?float
	{
		return $this->rating;
	}

	public function setRating(?float $rating): self
	{
		$this->rating = $rating;

		return $this;
	}

	public function getDateCreated(): ?\DateTimeInterface
	{
		return $this->dateCreated;
	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

	public function getSlug(): ?string
	{
		return $this->slug;
	}

	public function setSlug(string $slug): self
	{
		$this->slug = $slug;

		return $this;
	}

	public function getLength(): ?int
	{
		return $this->length;
	}

	public function setLength(?int $length): self
	{
		$this->length = $length;

		return $this;
	}

	public function getVisits(): ?int
	{
		return $this->visits;
	}

	public function setVisits(?int $visits): self
	{
		$this->visits = $visits;

		return $this;
	}

	public function getGenres()
	{
		return $this->genres;
	}

	public function addGenre(?Genre $genre): self
	{
		$this->genres[] = $genre;

		return $this;
	}

	public function getUserReviews()
	{
		return $this->userReviews;
	}

	public function getHomepageRecommendation()
	{
		return $this->homepageRecommendation;
	}

	public function getCastMembers()
	{
		return $this->castMembers;
	}

	public function __construct()
	{
		$this->dateCreated = new \DateTime();
		$this->genres = new ArrayCollection();
		$this->userReviews = new ArrayCollection();
		$this->castMembers = new ArrayCollection();
	}

	public function __toString(){
		return $this->title;
	}
}
