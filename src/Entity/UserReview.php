<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserReviewRepository")
 */
class UserReview
{
	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="reviews")
	 */
	private $user;

	/**
	 * @ORM\ManyToOne(targetEntity="MovieAndTvShow", inversedBy="userReviews")
	 */
	private $movieOrTvShow;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $userId;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $movieOrTvShowId;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\NotBlank(message="This field is required")
	 */
	private $body;

	/**
	 * @ORM\Column(type="float")
	 * @Assert\NotBlank(message="This field is required")
	 * @Assert\NotNull(message="Fill in a number")
	 */
	private $rating;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateCreated;

	/**
	 * @ORM\Column(type="string")
	 */
	private $type;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUserId(): ?int
	{
		return $this->userId;
	}

	public function setUserId(int $userId): self
	{
		$this->userId = $userId;

		return $this;
	}

	public function getMovieOrTvShowId(): ?int
	{
		return $this->movieOrTvShowId;
	}

	public function setMovieOrTvShowId(int $movieOrTvShowId): self
	{
		$this->movieOrTvShowId = $movieOrTvShowId;

		return $this;
	}

	public function getBody(): ?string
	{
		return $this->body;
	}

	public function setBody(string $body): self
	{
		$this->body = $body;

		return $this;
	}

	public function getRating(): ?float
	{
		return $this->rating;
	}

	public function setRating(float $rating): self
	{
		$this->rating = $rating;

		return $this;
	}

	public function getDateCreated(): ?\DateTimeInterface
	{
		return $this->dateCreated;
	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function setUser(User $user)
	{
		$this->user = $user;

		return $this;
	}

	public function getMovieOrTvShow()
	{
		return $this->movieOrTvShow;
	}

	public function setMovieOrTvShow(MovieAndTvShow $movieOrTvShow)
	{
		$this->movieOrTvShow = $movieOrTvShow;
	}

	public function __construct()
	{
		$this->dateCreated = new \DateTime();
	}
}
