<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomepageRecommendationRepository")
 */
class HomepageRecommendation
{
	/**
	 * @ORM\OneToOne(targetEntity="MovieAndTvShow", inversedBy="homepageRecommendation")
	 */
	private $movieOrTvShow;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateUpdated;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $ordering;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getDateUpdated(): ?\DateTimeInterface
	{
		return $this->dateUpdated;
	}

	public function setDateUpdated(\DateTimeInterface $dateUpdated): self
	{
		$this->dateUpdated = $dateUpdated;

		return $this;
	}

	public function getMovieOrTvShow()
	{
		return $this->movieOrTvShow;
	}

	public function setMovieOrTvShow(MovieAndTvShow $movieOrTvShow)
	{
		$this->movieOrTvShow = $movieOrTvShow;
	}

	public function getOrdering(): ?int
	{
		return $this->ordering;
	}

	public function setOrdering(int $ordering): self
	{
		$this->ordering = $ordering;

		return $this;
	}

	public function __construct()
	{
		$this->dateUpdated = new \DateTime();
	}
}
