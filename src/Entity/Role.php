<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="This name is already in use"
 * )
 */
class Role
{
	/**
	 * @ORM\OneToMany(targetEntity="User", mappedBy="role")
	 */
	private $users;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="This field is required")
	 */
	private $name;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $deletable;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getDeletable()
	{
		return $this->deletable;
	}

	public function __construct()
	{
		$this->users = new ArrayCollection();
		$this->deletable = TRUE;
	}

	public function __toString()
	{
		return $this->name;
	}
}
