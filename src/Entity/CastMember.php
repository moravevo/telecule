<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CastMemberRepository")
 */
class CastMember
{
	/**
	 * @ORM\ManyToOne(targetEntity="Celebrity", inversedBy="moviesAndTvShows")
	 */
	private $celebrity;

	/**
	 * @ORM\ManyToOne(targetEntity="MovieAndTvShow", inversedBy="castMembers")
	 * @OrderBy({"premiere" = "DESC"})
	 */
	private $movieOrTvShow;

	/**
	 * @ORM\ManyToMany(targetEntity="CastRole")
	 * @ORM\JoinTable(name="cast_member_role",
	 *      joinColumns={@ORM\JoinColumn(name="cast_member_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="cast_role_id", referencedColumnName="id")}
	 *      )
	 */
	public $castRoles;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $celebrityId;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $movieOrTvShowId;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $premiere;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCelebrityId(): ?int
	{
		return $this->celebrityId;
	}

	public function setCelebrityId(int $celebrityId): self
	{
		$this->celebrityId = $celebrityId;

		return $this;
	}

	public function getMovieOrTvShowId(): ?int
	{
		return $this->movieOrTvShowId;
	}

	public function setMovieOrTvShowId(int $movieOrTvShowId): self
	{
		$this->movieOrTvShowId = $movieOrTvShowId;

		return $this;
	}

	public function getCelebrity()
	{
		return $this->celebrity;
	}

	public function setCelebrity(Celebrity $celebrity)
	{
		$this->celebrity = $celebrity;

		return $this;
	}

	public function getMovieOrTvShow()
	{
		return $this->movieOrTvShow;
	}

	public function setMovieOrTvShow(MovieAndTvShow $movieOrTvShow)
	{
		$this->movieOrTvShow = $movieOrTvShow;
	}

	public function getCastRoles()
	{
		return $this->castRoles;
	}

	public function addCastRole(?CastRole $castRole): self
	{
		$this->castRoles[] = $castRole;

		return $this;
	}

	public function getPremiere(): ?\DateTimeInterface
	{
		return $this->premiere;
	}

	public function setPremiere(?\DateTimeInterface $premiere): self
	{
		$this->premiere = $premiere;

		return $this;
	}

	public function __construct()
	{
		$this->castRoles = new ArrayCollection();
	}
}
