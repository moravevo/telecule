<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"emailAddress"},
 *     errorPath="emailAddress",
 *     message="This email is already in use"
 * )
 * @UniqueEntity(
 *     fields={"username"},
 *     errorPath="username",
 *     message="This username is already in use"
 * )
 */
class User
{
	/**
	 * @ORM\OneToMany(targetEntity="UserReview", mappedBy="user")
	 * @OrderBy({"dateCreated" = "DESC"})
	 */
	private $reviews;

	/**
	 * @ORM\ManyToOne(targetEntity="Role", inversedBy="users")
	 */
	private $role;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="This field is required")
	 * @Assert\Length(min=2, minMessage="Minimum of 2 characters is required")
	 */
	private $fullName;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="This field is required")
	 * @Assert\Length(min=2, minMessage="Minimum of 2 characters is required")
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\NotBlank(message="This field is required", groups={"login", "Default"})
	 * @Assert\Email(message="Fill in a valid email address", groups={"login", "forgotten-password", "Default"})
	 */
	private $emailAddress;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\Image(maxSize = "2M", groups={"Default"})
	 */
	private $profilePicture;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="This field is required", groups={"login", "Default", "change"})
	 */
	private $password;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $bio;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $birthday;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateCreated;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $roleId;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getFullName(): ?string
	{
		return $this->fullName;
	}

	public function setFullName(string $fullName): self
	{
		$this->fullName = $fullName;

		return $this;
	}

	public function getUsername(): ?string
	{
		return $this->username;
	}

	public function setUsername(string $username): self
	{
		$this->username = $username;

		return $this;
	}

	public function getEmailAddress(): ?string
	{
		return $this->emailAddress;
	}

	public function setEmailAddress(string $emailAddress): self
	{
		$this->emailAddress = $emailAddress;

		return $this;
	}

	public function getProfilePicture(): ?string
	{
		return $this->profilePicture;
	}

	public function setProfilePicture(?string $profilePicture): self
	{
		$this->profilePicture = $profilePicture;

		return $this;
	}

	public function getPassword(): ?string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}

	public function getBio(): ?string
	{
		return $this->bio;
	}

	public function setBio(?string $bio): self
	{
		$this->bio = $bio;

		return $this;
	}

	public function getBirthday(): ?\DateTimeInterface
	{
		return $this->birthday;
	}

	public function setBirthday(?\DateTimeInterface $birthday): self
	{
		$this->birthday = $birthday;

		return $this;
	}

	public function getDateCreated(): ?\DateTimeInterface
	{
		return $this->dateCreated;
	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

	public function getRoleId(): ?int
	{
		return $this->roleId;
	}

	public function setRoleId(?int $roleId): self
	{
		$this->roleId = $roleId;

		return $this;
	}

	public function getReviews()
	{
		return $this->reviews;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function setRole(Role $role)
	{
		$this->role = $role;

		return $this;
	}

	public function __construct()
	{
		$this->dateCreated = new \DateTime();
		$this->reviews = new ArrayCollection();
	}

	public function hasRoleId($roleId)
	{
		$session = new Session();
		$user = $session->get('user');

		if ($user && $user->roleId === $roleId) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function isAdmin()
	{
		$session = new Session();
		$user = $session->get('user');

		if ($user->getRoleId() === 2 || $user->getRoleId() === 3) {
			return TRUE;
		}

		return FALSE;
	}

	public function isSuperAdmin()
	{
		$session = new Session();
		$user = $session->get('user');

		if ($user->getRoleId() === 3) {
			return TRUE;
		}

		return FALSE;
	}

	public function isLoggedIn()
	{
		$session = new Session();
		$user = $session->get('user');

		if ($user === NULL) {
			return FALSE;
		}

		return TRUE;
	}

	public function checkPassword(Form $form)
	{
		if (empty($this->getPassword())) {
			$form->get('password')->get('password')->addError(new FormError('This field is required'));

			return FALSE;
		}

		return TRUE;
	}

	public function checkProfilePicture(Form $form)
	{
		$validExtensions = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

		/**
		 * @var UploadedFile $file
		 */
		$file = $form->get('profilePicture')->getData();

		if ($file === NULL) {
			$this->setProfilePicture('thumbnail.jpeg');
		} elseif ($file->getSize() >= 16777216) {
			$form->get('profilePicture')->addError(new FormError('Maximum size of image is 16MB'));

			return FALSE;

		} elseif (!in_array($file->guessExtension(), $validExtensions)) {
			$form->get('profilePicture')->addError(new FormError('Only images are allowed'));

			return FALSE;
		}

		return TRUE;
	}

	public function isSignupFormValid(Form $form)
	{
		$parameters = [];
		array_push($parameters, $this->checkPassword($form), $this->checkProfilePicture($form));

		$valid = TRUE;

		foreach ($parameters as $parameter) {
			if (!$parameter) {
				$valid = FALSE;
			}
		}

		return $valid;
	}
}
