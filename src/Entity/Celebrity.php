<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CelebrityRepository")
 * @UniqueEntity(
 *     fields={"fullName"},
 *     errorPath="fullName",
 *     message="This celebrity already exists"
 * )
 */
class Celebrity
{
	/**
	 * @ORM\OneToMany(targetEntity="CastMember", mappedBy="celebrity", orphanRemoval=true)
	 * @OrderBy({"premiere" = "DESC"})
	 */
	private $moviesAndTvShows;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="This field is required")
	 * @Assert\Length(min=2, minMessage="Minimum of 2 characters is required")
	 */
	private $fullName;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 * @Assert\Image(
	 *     maxSize = "2M"
	 * )
	 */
	private $picture;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $bio;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $birthday;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $dateCreated;

	/**
	 * @ORM\Column(type="text")
	 */
	private $slug;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $visits;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getFullName(): ?string
	{
		return $this->fullName;
	}

	public function setFullName(string $fullName): self
	{
		$this->fullName = $fullName;

		return $this;
	}

	public function getPicture(): ?string
	{
		return $this->picture;
	}

	public function setPicture(?string $picture): self
	{
		$this->picture = $picture;

		return $this;
	}

	public function getBio(): ?string
	{
		return $this->bio;
	}

	public function setBio(string $bio): self
	{
		$this->bio = $bio;

		return $this;
	}

	public function getBirthday(): ?\DateTimeInterface
	{
		return $this->birthday;
	}

	public function setBirthday(?\DateTimeInterface $birthday): self
	{
		$this->birthday = $birthday;

		return $this;
	}

	public function getDateCreated(): ?\DateTimeInterface
	{
		return $this->dateCreated;
	}

	public function setDateCreated(\DateTimeInterface $dateCreated): self
	{
		$this->dateCreated = $dateCreated;

		return $this;
	}

	public function getMoviesAndTvShows()
	{
		return $this->moviesAndTvShows;
	}

	public function getSlug(): ?string
	{
		return $this->slug;
	}

	public function setSlug(string $slug): self
	{
		$this->slug = $slug;

		return $this;
	}

	public function getVisits(): ?int
	{
		return $this->visits;
	}

	public function setVisits(?int $visits): self
	{
		$this->visits = $visits;

		return $this;
	}

	public function __construct()
	{
		$this->dateCreated = new \DateTime();
	}

	public function __toString(){
		return $this->fullName;
	}

	public function getCastRoles() {
		return $this->castRoles;
	}

	public $castRoles;
}
