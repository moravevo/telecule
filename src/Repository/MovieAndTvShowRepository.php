<?php

namespace App\Repository;

use App\Entity\MovieAndTvShow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MovieAndTvShow|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovieAndTvShow|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovieAndTvShow[]    findAll()
 * @method MovieAndTvShow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieAndTvShowRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, MovieAndTvShow::class);
	}

//    /**
//     * @return MovieAndTvShow[] Returns an array of MovieAndTvShow objects
//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('m')
			->andWhere('m.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('m.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?MovieAndTvShow
	{
		return $this->createQueryBuilder('m')
			->andWhere('m.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function countType($type)
	{
		return $this->createQueryBuilder('m')
			->select('count(m.id)')
			->where('m.type = :type')
			->setParameter('type', $type)
			->getQuery()
			->getSingleScalarResult();
	}

	public function findSimilar($movieOrTvShow)
	{
		$similarMovies = $this->createQueryBuilder('m')
			->leftJoin('m.genres', 'g')
			->where('m != :m')
			->andWhere('m.type = :type')
			->andWhere('g IN (:g)')
			->andWhere('m.thumbnail != :thumbnail')
			->setParameter('m', $movieOrTvShow)
			->setParameter('type', $movieOrTvShow->getType())
			->setParameter('g', $movieOrTvShow->getGenres())
			->setParameter('thumbnail', 'thumbnail.jpeg')
			->getQuery()
			->getResult();

		shuffle($similarMovies);

		return array_slice($similarMovies, 0, 4);
	}

	public function findByTitleAndType($title, $type)
	{
		return $this->createQueryBuilder('m')
			->select('m.id, m.title, m.slug, m.premiere')
			->where('m.type = :type')
			->andWhere('m.title LIKE :title')
			->setParameter('type', $type)
			->setParameter('title', '%' . $title . '%')
			->getQuery()
			->getResult();
	}

	public function search($type, $title, $lowestPremiereDate, $highestPremiereDate, $includeWithoutPremiere, $ratingLow, $ratingHigh, $includeWithoutRating, $minLength, $maxLength, $includeWithoutLength, $genres, $includeWithtoutGenres)
	{
		$query = $this->createQueryBuilder('m')
			->leftJoin('m.genres', 'genres')
			->where('m.type = :type')
			->andWhere('m.title LIKE :title');

		if ($includeWithoutPremiere) {
			$query->andWhere('((YEAR(m.premiere) BETWEEN :oldest AND :newest) OR m.premiere IS NULL)');
		} else {
			$query->andWhere('YEAR(m.premiere) BETWEEN :oldest AND :newest');
		}

		if ($includeWithoutRating) {
			$query->andWhere('((m.rating BETWEEN :low AND :high) OR m.rating IS NULL)');
		} else {
			$query->andWhere('m.rating BETWEEN :low AND :high');
		}

		if ($includeWithoutLength) {
			$query->andWhere('((m.length BETWEEN :minLength AND :maxLength) OR m.length IS NULL)');
		} else {
			$query->andWhere('m.length BETWEEN :minLength AND :maxLength');
		}

		$query->setParameter('type', $type)
			->setParameter('title', '%' . $title . '%')
			->setParameter('oldest', $lowestPremiereDate)
			->setParameter('newest', $highestPremiereDate)
			->setParameter('low', $ratingLow)
			->setParameter('high', $ratingHigh)
			->setParameter('minLength', $minLength)
			->setParameter('maxLength', $maxLength);

		if (!$includeWithtoutGenres) {
			$query->andWhere('genres IN (:genres)')
				->setParameter('genres', $genres);
		}

		return $query->getQuery()
			->getResult();
	}

	public function findOldestByType($type)
	{
		return $this->createQueryBuilder('m')
			->select('min(m.premiere)')
			->where('m.type = :type')
			->setParameter('type', $type)
			->getQuery()
			->getSingleScalarResult();
	}

	public function findNewestByType($type)
	{
		return $this->createQueryBuilder('m')
			->select('max(m.premiere)')
			->where('m.type = :type')
			->setParameter('type', $type)
			->getQuery()
			->getSingleScalarResult();
	}

	public function findMinLength()
	{
		return $this->createQueryBuilder('m')
			->select('min(m.length)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findMaxLength()
	{
		return $this->createQueryBuilder('m')
			->select('max(m.length)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findNewReleases()
	{
		return $this->createQueryBuilder('m')
			->where('DATEDIFF(NOW(), m.premiere) <= 30')
			->andWhere('m.premiere < NOW()')
			->andWhere('m.type = :type')
			->setParameter('type', 'movie')
			->orderBy('m.premiere', 'DESC')
			->getQuery()
			->getResult();
	}

	public function findMoviesAddedThisMonth()
	{
		$today = new \DateTime();
		$thisMonth = $today->format('m');

		return $this->createQueryBuilder('m')
			->where('MONTH(m.dateCreated) = :month')
			->andWhere('m.type = :type')
			->setParameter('type', 'movie')
			->setParameter('month', $thisMonth)
			->orderBy('m.dateCreated', 'desc')
			->getQuery()
			->getResult();
	}

	public function findTvShowsAddedThisMonth()
	{
		$today = new \DateTime();
		$thisMonth = $today->format('m');

		return $this->createQueryBuilder('m')
			->where('MONTH(m.dateCreated) = :month')
			->andWhere('m.type = :type')
			->setParameter('type', 'tv_show')
			->setParameter('month', $thisMonth)
			->orderBy('m.dateCreated', 'desc')
			->getQuery()
			->getResult();
	}

	public function findMaxVisits()
	{
		return $this->createQueryBuilder('m')
			->select('MAX(m.visits)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findjpg()
	{
		return $this->createQueryBuilder('m')
			->where('m.thumbnail LIKE :thumbnail')
			->setParameter('thumbnail', '%' . '.jpeg' . '%')
			->getQuery()
			->getResult();
	}
}
