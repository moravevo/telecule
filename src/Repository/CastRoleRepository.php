<?php

namespace App\Repository;

use App\Entity\CastRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CastRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method CastRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method CastRole[]    findAll()
 * @method CastRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CastRoleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CastRole::class);
    }

//    /**
//     * @return CastRole[] Returns an array of CastRole objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CastRole
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
