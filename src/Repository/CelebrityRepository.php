<?php

namespace App\Repository;

use App\Entity\Celebrity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Celebrity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Celebrity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Celebrity[]    findAll()
 * @method Celebrity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CelebrityRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Celebrity::class);
	}

//    /**
//     * @return Celebrity[] Returns an array of Actor objects
//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('a')
			->andWhere('a.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('a.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Actor
	{
		return $this->createQueryBuilder('a')
			->andWhere('a.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function countCelebrities()
	{
		return $this->createQueryBuilder('c')
			->select('count(c.id)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findByName($name)
	{
		return $this->createQueryBuilder('c')
			->select('c.id, c.fullName, c.slug')
			->where('c.fullName LIKE :fullName')
			->setParameter('fullName', '%' . $name . '%')
			->getQuery()
			->getResult();
	}

	public function findMinBirthday()
	{
		return $this->createQueryBuilder('c')
			->select('min(c.birthday)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findMaxBirthday()
	{
		return $this->createQueryBuilder('c')
			->select('max(c.birthday)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function search($fullName, $lowestBirthday, $highestBirthday, $includeWithoutBirthday)
	{
		$query = $this->createQueryBuilder('c')
			->where('c.fullName LIKE :fullName');

		if ($includeWithoutBirthday) {
			$query->andWhere('((YEAR(c.birthday) BETWEEN :oldest AND :newest) OR c.birthday IS NULL)');
		} else {
			$query->andWhere('YEAR(c.birthday) BETWEEN :oldest AND :newest');
		}

		$query->setParameter('fullName', '%' . $fullName . '%')
			->setParameter('oldest', $lowestBirthday)
			->setParameter('newest', $highestBirthday);

		return $query->getQuery()
			->getResult();
	}

	public function findBornThisMonth()
	{
		return $this->createQueryBuilder('c')
			->where('MONTH(c.birthday) = MONTH(NOW())')
			->orderBy('DAY(c.birthday)', 'ASC')
			->getQuery()
			->getResult();
	}

	public function findFullNames()
	{
		return $this->createQueryBuilder('c')
			->select('c.fullName')
			->getQuery()
			->getResult();
	}

	public function findCastRoles($id)
	{
		return $this->createQueryBuilder('c')
			->select('cr.name')
			->leftJoin('c.moviesAndTvShows', 'cm')
			->leftJoin('cm.castRoles', 'cr')
			->where('c.id = :id')
			->groupBy('cr.name')
			->setParameter('id', $id)
			->getQuery()
			->getResult();
	}

	public function findCelebritiesAddedThisMonth()
	{
		$today = new \DateTime();
		$thisMonth = $today->format('m');

		return $this->createQueryBuilder('c')
			->where('MONTH(c.dateCreated) = :month')
			->setParameter('month', $thisMonth)
			->orderBy('c.dateCreated', 'desc')
			->getQuery()
			->getResult();
	}
}
