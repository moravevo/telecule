<?php

namespace App\Repository;

use App\Entity\CastMember;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CastMember|null find($id, $lockMode = null, $lockVersion = null)
 * @method CastMember|null findOneBy(array $criteria, array $orderBy = null)
 * @method CastMember[]    findAll()
 * @method CastMember[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CastMemberRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, CastMember::class);
	}

//    /**
//     * @return CastMember[] Returns an array of CastMember objects
//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('c.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?CastMember
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/

	public function findCastMemberForMovieOrTvShowByCastRole($id, $role)
	{
		$query = $this->createQueryBuilder('cm')
			->select('c.fullName')
			->leftJoin('cm.castRoles', 'cr')
			->leftJoin('cm.movieOrTvShow', 'mots')
			->leftJoin('cm.celebrity', 'c')
			->where('cr.name = :cr')
			->andWhere('mots.id = :mots')
			->setParameter('cr', $role)
			->setParameter('mots', $id)
			->orderBy('ORD', 'ASC')
			->getQuery()
			->getScalarResult();

		return array_map('current', $query);
	}
}
