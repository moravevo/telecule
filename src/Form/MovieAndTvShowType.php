<?php

namespace App\Form;

use App\Entity\Genre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MovieAndTvShowType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$thumbnailHelp = NULL;

		if ($options['edit']) {
			$thumbnailHelp = 'Leave empty to keep the original picture';
		}

		$builder
			->add('title', TextType::class, [
				'empty_data' => ''
			])
			->add('thumbnail', FileType::class, [
				'label' => 'Thumbnail (optional)',
				'data_class' => NULL,
				'help' => $thumbnailHelp
			]);

		if ($options['removeThumbnail']) {
			$builder->add('removeThumbnail', CheckboxType::class, [
				'mapped' => FALSE
			]);
		} else {
			$builder->add('removeThumbnail', HiddenType::class, [
				'mapped' => FALSE,
				'empty_data' => FALSE
			]);
		}

		$builder->add('shortDescription', TextareaType::class, [
				'label' => 'Short description (optional)',
				'attr' => [
					'rows' => $options['textarea_rows']
				]
			])
			->add('description', TextareaType::class, [
				'label' => 'Description (optional)',
				'attr' => [
					'rows' => $options['textarea_rows']
				]
			])
			->add('premiere', DateType::class, [
				'widget' => 'single_text',
				'format' => 'dd. MM. yyyy',
				'html5' => FALSE,
				'help' => 'Only valid date format is "DD. MM. YYYY"',
				'empty_data' => '',
				'attr' => [
					'data-toggle' => 'datepicker',
					'autocomplete' => 'off'
				]
			]);

		if ($options['type'] === 'movie') {
			$builder->add('length', NumberType::class, [
				'help' => 'Length in minutes'
			]);
		}

			$builder->add('genres', EntityType::class, [
				'class' => Genre::class,
				'multiple' => TRUE,
				'attr' => [
					'class' => 'selectpicker'
				]
			])
			->add('rating', RangeType::class, [
				'invalid_message' => 'This field must be a number',
				'attr' => [
					'class' => 'custom-range',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'step' => 0.1,
					'min' => 0,
					'max' => 10
				]
			])
			->add('withoutRating', CheckboxType::class, [
				'mapped' => FALSE,
				'label' => 'Without rating',
				'attr' => [
					'class' => 'without-rating custom-control-input'
				],
				'data' => $options['withoutRating']
			])
			->add('type', HiddenType::class, [
				'data' => $options['type']
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => [
					'class' => 'btn-primary'
				]
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\MovieAndTvShow',
			'validation_groups' => 'Default',
			'textarea_rows' => 4,
			'edit' => FALSE,
			'type' => 'movie',
			'withoutRating' => FALSE,
			'removeThumbnail' => FALSE
		]);
	}
}