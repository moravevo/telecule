<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ChangePasswordType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('password', RepeatedType::class, [
				'type' => PasswordType::class,
				'invalid_message' => 'Passwords do not match',
				'first_name' => 'password',
				'first_options' => [
					'help' => 'Choose a strong password',
					'label' => 'Password',
					'attr' => [
						'autorcorrect' => 'off',
						'autocapitalize' => 'off',
						'spellcheck' => FALSE
					]
				],
				'second_name' => 'passwordCheck',
				'second_options' => [
					'help' => 'Re-enter your password',
					'label' => 'Password check',
					'attr' => [
						'autorcorrect' => 'off',
						'autocapitalize' => 'off',
						'spellcheck' => FALSE
					]
				]
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => ['class' => 'btn-primary']
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\User'
		]);
	}
}