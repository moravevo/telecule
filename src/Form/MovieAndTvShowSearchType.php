<?php

namespace App\Form;

use App\Controller\BaseController\MovieAndTvShowBaseController;
use App\Repository\GenreRepository;
use App\Entity\Genre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MovieAndTvShowSearchType extends AbstractType
{
	private $genreRepository;
	private $movieAndTvShowBaseController;

	public function __construct(
		GenreRepository $genreRepository,
		MovieAndTvShowBaseController $movieAndTvShowBaseController
	)
	{
		$this->genreRepository = $genreRepository;
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$oldest = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findOldestByType($options['type']));
		$newest = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findNewestByType($options['type']));
		$maxLength = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxLength();

		if ($options['type'] === 'movie') {
			$includeWithoutPremiereLabel = 'Include movies without premiere';
			$includeWithoutRatingLabel = 'Include not yet rated movies';
			$includeWithoutLengthLabel = 'Include movies without length';
			$placeholder = 'Movie title';
		} else {
			$includeWithoutPremiereLabel = 'Include TV Shows without premiere';
			$includeWithoutRatingLabel = 'Include not yet rated TV Shows';
			$includeWithoutLengthLabel = 'Include TV Shows without length';
			$placeholder = 'TV Show title';
		}

		$builder
			->add('title', TextType::class, [
				'attr' => [
					'placeholder' => $placeholder
				]
			])
			->add('advancedSearch', CheckboxType::class, [
				'mapped' => FALSE,
				'label' => 'Detailed searching',
				'attr' => [
					'class' => 'advanced-search custom-control-input'
				]
			])
			->add('lowestPremiereDate', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => $oldest->format('Y'),
				'attr' => [
					'data-limit' => $oldest->format('Y'),
					'class' => 'number-input-for-range'
				]
			])
			->add('highestPremiereDate', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => $newest->format('Y'),
				'attr' => [
					'data-limit' => $newest->format('Y'),
					'class' => 'number-input-for-range'
				]
			])
			->add('includeWithoutPremiere', CheckboxType::class, [
				'label' => $includeWithoutPremiereLabel,
				'mapped' => FALSE,
			])
			->add('ratingLow', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => '0',
				'attr' => [
					'data-limit' => '0',
					'data-step' => 0.1,
					'class' => 'number-input-for-range'
				]
			])
			->add('ratingHigh', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => '10',
				'attr' => [
					'data-limit' => '10',
					'class' => 'number-input-for-range'
				]
			])
			->add('includeWithoutRating', CheckboxType::class, [
				'label' => $includeWithoutRatingLabel,
				'mapped' => FALSE,
			]);

		if ($options['type'] === 'movie') {
			$builder->add('minLength', NumberType::class, [
				'empty_data' => '0',
				'mapped' => FALSE,
				'attr' => [
					'data-limit' => '0',
					'class' => 'number-input-for-range'
				]
			])
				->add('maxLength', NumberType::class, [
					'empty_data' => $maxLength,
					'mapped' => FALSE,
					'attr' => [
						'data-limit' => $maxLength,
						'class' => 'number-input-for-range'
					]
				])
				->add('includeWithoutLength', CheckboxType::class, [
					'label' => $includeWithoutLengthLabel,
					'mapped' => FALSE,
				]);
		}

		$builder->add('genres', EntityType::class, [
			'class' => Genre::class,
			'multiple' => TRUE,
			'attr' => [
				'class' => 'selectpicker',
			],
		])
			->add('submit', SubmitType::class, [
				'label' => 'Search',
				'attr' => [
					'class' => 'btn btn-primary'
				]
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\MovieAndTvShow',
			'validation_groups' => 'Search',
			'type' => 'movie'
		]);
	}
}