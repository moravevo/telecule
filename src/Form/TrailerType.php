<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TrailerType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$help = '';

		if ($options['edit'] === TRUE) {
			$help = 'Leave empty to keep the original picture';
		}

		$builder
			->add('title', TextType::class)
			->add('url', UrlType::class)
			->add('thumbnail', FileType::class, [
				'data_class' => NULL,
				'help' => $help,
				'empty_data' => 'NULL'
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => [
					'class' => 'btn-primary'
				]
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\Trailer',
			'validation_groups' => 'Default',
			'edit' => FALSE
		]);
	}
}