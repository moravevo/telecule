<?php

namespace App\Form;

use App\Entity\CastRole;
use App\Entity\Celebrity;
use App\Entity\MovieAndTvShow;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CastMemberType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		if (!$options['edit']) {
			$builder
				->add('celebrity', EntityType::class, [
					'class' => Celebrity::class,
					'mapped' => FALSE,
					'attr' => [
						'class' => 'selectpicker-live-search'
					]
				]);
		}

		if ($options['only_movie']) {
			$builder
				->add('movieOrTvShow', EntityType::class, [
					'class' => MovieAndTvShow::class,
					'mapped' => FALSE,
					'attr' => [
						'class' => 'selectpicker-live-search'
					]
				]);
		}

		$builder
			->add('castRoles', EntityType::class, [
				'class' => CastRole::class,
				'multiple' => TRUE,
				'attr' => [
					'class' => 'selectpicker'
				]
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => ['class' => 'btn-primary']
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\CastMember',
			'validation_groups' => 'Cast',
			'edit' => FALSE,
			'only_movie' => FALSE
		]);
	}
}