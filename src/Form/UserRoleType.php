<?php

namespace App\Form;

use App\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserRoleType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('role', EntityType::class, [
				'class' => Role::class,
				'attr' => [
					'class' => 'selectpicker'
				]
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => ['class' => 'btn-primary']
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\User',
			'edit' => FALSE,
			'validation_groups' => 'role'
		]);
	}
}