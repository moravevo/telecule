<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserReviewType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('body', TextareaType::class, [
				'label' => 'Review',
				'attr' => [
					'rows' => 4
				]
			]);

		if (!$options['admin']) {
			$builder->add('rating', RangeType::class, [
				'attr' => [
					'class' => 'custom-range',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'step' => 0.1,
					'min' => 0,
					'max' => 10
				]
			]);
		}

		$builder->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => [
					'class' => 'btn-primary'
				]
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\UserReview',
			'validation_groups' => 'Default',
			'admin' => FALSE
		]);
	}
}