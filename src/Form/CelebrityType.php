<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CelebrityType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$pictureHelp = NULL;

		if ($options['edit']) {
			$pictureHelp = 'Leave empty to keep the original picture';
		}

		$builder
			->add('fullName', TextType::class)
			->add('birthday', DateType::class, [
				'widget' => 'single_text',
				'format' => 'dd. M. yyyy',
				'html5' => FALSE,
				'help' => 'Only valid date format is "DD. MM. YYYY"',
				'attr' => [
					'data-toggle' => 'datepicker',
					'autocomplete' => 'off'
				]
			])
			->add('bio', TextareaType::class, [
				'label' => 'Bio (optional)',
				'attr' => [
					'rows' => 5
				]
			])
			->add('picture', FileType::class, [
				'label' => 'Picture (optional)',
				'data_class' => NULL,
				'help' => $pictureHelp
			]);

		if ($options['removePicture']) {
			$builder->add('removePicture', CheckboxType::class, [
				'mapped' => FALSE
			]);
		} else {
			$builder->add('removePicture', HiddenType::class, [
				'mapped' => FALSE
			]);
		}

		$builder->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => ['class' => 'btn-primary']
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\Celebrity',
			'edit' => FALSE,
			'removePicture' => FALSE
		]);
	}
}