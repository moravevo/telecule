<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SignUpType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$today = new \DateTime();

		$builder
			->add('fullName', TextType::class)
			->add('username', TextType::class, [
				'help' => 'Username is displayed to other users on the website'
			]);


		if (!$options['admin']) {
			$builder
				->add('emailAddress', TextType::class, [
					'help' => 'Email address is used for you to log in to your account'])
				->add('birthday', DateType::class, [
					'widget' => 'single_text',
					'format' => 'dd. M. yyyy',
					'html5' => FALSE,
					'help' => 'Only valid date format is "DD. MM. YYYY"',
					'label' => 'Birthday (optional)',
					'attr' => [
						'data-toggle' => 'datepicker',
						'autocomplete' => 'off'
					]
				]);
		}

		$builder->add('bio', TextareaType::class, [
			'label' => 'Bio (optional)'
		]);

		$builder->add('profilePicture', FileType::class, [
			'label' => 'Profile picture (optional)',
			'data_class' => NULL
		]);


		if ($options['removeProfilePicture']) {
			$builder->add('removeProfilePicture', CheckboxType::class, [
				'mapped' => FALSE
			]);
		} else {
			$builder->add('removeProfilePicture', HiddenType::class, [
				'mapped' => FALSE
			]);
		}

		if (!$options['admin']) {
			if (!$options['edit']) {
				$builder->add('password', RepeatedType::class, [
					'type' => PasswordType::class,
					'invalid_message' => 'Passwords do not match',
					'first_name' => 'password',
					'first_options' => [
						'empty_data' => '',
						'help' => 'Choose a strong password',
						'label' => 'Password',
						'attr' => [
							'autorcorrect' => 'off',
							'autocapitalize' => 'off',
							'spellcheck' => FALSE
						]
					],
					'second_name' => 'passwordCheck',
					'second_options' => [
						'empty_data' => '',
						'help' => 'Re-enter your password',
						'label' => 'Password check',
						'attr' => [
							'autorcorrect' => 'off',
							'autocapitalize' => 'off',
							'spellcheck' => FALSE
						]
					]
				]);
			}
		}


		$builder->add('submit', SubmitType::class, [
			'label' => 'Submit',
			'attr' => ['class' => 'btn-primary']
		])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\User',
			'edit' => FALSE,
			'admin' => FALSE,
			'removeProfilePicture' => FALSE
		]);
	}
}