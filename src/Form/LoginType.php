<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class LoginType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('emailAddress', TextType::class, [
				'attr' => [
					'class' => 'form-control radius-6',
					'data-input-email' => 'true'
				]
			])
			->add('password', PasswordType::class, [
				'attr' => [
					'class' => 'form-control radius-6',
					'autorcorrect' => 'off',
					'autocapitalize' => 'off',
					'spellcheck' => FALSE
				]
			])
			->add('submit', SubmitType::class, [
				'label' => 'Log in',
				'attr' => [
					'class' => 'btn btn-primary'
				]
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\User',
			'validation_groups' => 'login'
		]);
	}
}