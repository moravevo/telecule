<?php

namespace App\Form;

use App\Repository\CelebrityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CelebritySearchType extends AbstractType
{
	private $celebrityRepository;

	public function __construct(
		CelebrityRepository $celebrityRepository
	)
	{
		$this->celebrityRepository = $celebrityRepository;
	}


	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$oldest = new \DateTime($this->celebrityRepository->findMinBirthday());
		$newest = new \DateTime($this->celebrityRepository->findMaxBirthday());

		$builder
			->add('fullName', TextType::class, [
				'attr' => [
					'placeholder' => 'Celebrity name'
				]
			])
			->add('advancedSearch', CheckboxType::class, [
				'mapped' => FALSE,
				'label' => 'Detailed searching',
				'attr' => [
					'class' => 'advanced-search custom-control-input'
				]
			])
			->add('lowestBirthday', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => $oldest->format('Y'),
				'attr' => [
					'data-limit' => $oldest->format('Y'),
					'class' => 'number-input-for-range'
				]
			])
			->add('highestBirthday', NumberType::class, [
				'mapped' => FALSE,
				'empty_data' => $newest->format('Y'),
				'attr' => [
					'data-limit' => $newest->format('Y'),
					'class' => 'number-input-for-range'
				]
			])
			->add('includeWithoutBirthday', CheckboxType::class, [
				'label' => 'Include celebrities without birthday',
				'mapped' => FALSE,
			])
			->add('submit', SubmitType::class, [
				'label' => 'Submit',
				'attr' => ['class' => 'btn-primary']
			])
			->setRequired(FALSE);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'App\\Entity\\Celebrity',
			'validation_groups' => 'Search'
		]);
	}
}