<?php

namespace App\Service;

class Paginator
{
	public function paginate($data, $page, $perPage = 10, $alwaysDisplay = 5, $url)
	{
		if ($perPage === NULL || $perPage <= 0) {
			$perPage = 20;
		}

		$pages = ceil(count($data) / $perPage);

		if ($page === NULL || !is_numeric($page) || $page > $pages) {
			$page = 0;
		} elseif ($page > 0) {
			$page = $page - 1;
		}

		$pageArray = range(1, (int)$pages);
		$toLeft = floor($alwaysDisplay / 2);

		if ($alwaysDisplay % 2 === 0) {
			$toLeft = $toLeft - 1;
		}

		if ($page - ($toLeft + 1) >= 0) {
			$pageArray_ = array_slice($pageArray, $page - $toLeft, $alwaysDisplay);
			if (count($pageArray_) < $alwaysDisplay && $pages > $alwaysDisplay) {
				$displayed = abs(count($pageArray_) - $alwaysDisplay);

				$pageArray_ = array_slice($pageArray, $page - $toLeft - $displayed, $alwaysDisplay);
			} elseif ($pages < $alwaysDisplay) {
				$pageArray_ = $pageArray;
			}
		} else {
			$pageArray_ = array_slice($pageArray, 0, $alwaysDisplay);
		}

		$offset = $page * $perPage;

		$data = array_slice($data, $offset, $perPage);

		if ((int)$pages === 0) {
			$pageArray_ = NULL;
		}

		return [$data, $pageArray_, $page + 1, $pages, $url, $perPage];
	}
}