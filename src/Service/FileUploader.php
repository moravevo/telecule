<?php

namespace App\Service;

use App\Controller\AdminBaseController;
use Gumlet\ImageResize;
use Symfony\Component\Filesystem\Filesystem;
use Intervention\Image\Image;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FileUploader extends AdminBaseController
{
	public $filesystem;
	public $session;

	public function __construct()
	{
		$this->filesystem = new Filesystem();
		$this->session = new Session();
	}

	public function upload(UploadedFile $file, $targetDirectory, int $smallResizeX = NULL, int $smallResizeY = NULL, int $normalResizeX = NULL, int $normalResizeY = NULL)
	{
		if (!$file->getSize()) {
			return FALSE;
		}

		$fileName = md5(uniqid());
		$fileNameWithExtension = $fileName . '.' . $file->guessExtension();

		try {
			$file->move($targetDirectory, $fileNameWithExtension);

			$image = new ImageResize($targetDirectory . $fileNameWithExtension);
			$image->save($targetDirectory . $fileName . '.jpeg');

			if ($normalResizeX != NULL && $normalResizeY != NULL) {
				$image = new ImageResize($targetDirectory . $fileNameWithExtension);
				$image->crop($normalResizeX, $normalResizeY);
				$image->save($targetDirectory . 'normal/' . $fileName . '.jpeg');
			}

			if ($smallResizeX != NULL && $smallResizeY != NULL) {
				$image = new ImageResize($targetDirectory . $fileNameWithExtension);
				$image->crop($smallResizeX, $smallResizeY);
				$image->save($targetDirectory . 'small/' . $fileName . '.jpeg');
			}

			$this->filesystem->remove($targetDirectory . $fileNameWithExtension);
		} catch (FileException $e) {
			// Do something
		}

		return $fileName . '.jpeg';
	}
}