<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Controller\BaseController\MovieAndTvShowBaseController;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AdminBaseController
{
	private $movieAndTvShowBaseController;

	public function __construct(
		MovieAndTvShowBaseController $movieAndTvShowBaseController
	)
	{
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
	}

	/**
	 * @Route("/admin", name="admin", schemes={"http"})
	 */
	public function index()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movies = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['type' => 'movie'], ['dateCreated' => 'desc']);
		$tvShows = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['type' => 'tv_show'], ['dateCreated' => 'desc']);
		$celebrities = $this->movieAndTvShowBaseController->celebrityRepository->findBy([], ['dateCreated' => 'desc']);

		$moviesThisMonth = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMoviesAddedThisMonth();
		$tvShowsThisMonth = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findTvShowsAddedThisMonth();
		$celebritiesThisMonth = $this->movieAndTvShowBaseController->celebrityRepository->findCelebritiesAddedThisMonth();

		$maxVisits = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxVisits();
		$reviews = $this->movieAndTvShowBaseController->userReviewRepository->findBy([], ['dateCreated' => 'DESC'], 8);

		return $this->render('/admin/index.html.twig', [
			'moviesTotal' => $movies,
			'moviesAddedThisMonth' => $moviesThisMonth,
			'tvShowsTotal' => $tvShows,
			'tvShowsAddedThisMonth' => $tvShowsThisMonth,
			'celebritiesTotal' => $celebrities,
			'celebritiesAddedThisMonth' => $celebritiesThisMonth,
			'maxVisits' => $maxVisits,
			'reviews' => $reviews
		]);
	}
}