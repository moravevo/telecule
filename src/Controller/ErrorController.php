<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AdminBaseController
{
	/**
	 * @Route("/404", schemes={"http"})
	 */
	public function fourOFour()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		return $this->render('404.html.twig');
	}

	/**
	 * @Route("/403", schemes={"http"})
	 */
	public function fourOThree()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		return $this->render('403.html.twig');
	}

	/**
	 * @Route("/500", schemes={"http"})
	 */
	public function fiveOO()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		return $this->render('500.html.twig');
	}
}
