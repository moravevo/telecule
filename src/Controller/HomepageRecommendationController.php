<?php

namespace App\Controller;

use App\Repository\HomepageRecommendationRepository;
use App\Service\Paginator;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;


class HomepageRecommendationController extends AdminBaseController
{
	private $homepageRecommendationRepository;

	public function __construct(
		HomepageRecommendationRepository $homepageRecommendationRepository
	)
	{
		$this->homepageRecommendationRepository = $homepageRecommendationRepository;
	}

	/**
	 * @Route("/admin/homepage-recommendations", name="admin_recommendations", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$recommendations = $this->homepageRecommendationRepository->findAll();

		$paginated = $paginator->paginate($recommendations, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/homepage-recommendations');

		return $this->render('admin/homepage/homepage_recommendations/index.html.twig', [
			'recommendations' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($recommendations)
		]);
	}

	/**
	 * @Route("/admin/homepage-recommendations/delete/{id}", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$entityManager = $this->getDoctrine()->getManager();
		$recommendation = $this->homepageRecommendationRepository->find($id);

		try {
			$entityManager->remove($recommendation);
			$entityManager->flush();

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'Recommendation was successfully deleted');
			return $this->redirectToRoute('admin_recommendations');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}

	}

	/**
	 * @Route("/admin/homepage-recommendations/change-order", name="admin_recommendations_order", schemes={"http"})
	 */
	public function changeOrder(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$recommendations = $this->homepageRecommendationRepository->findBy([], ['ordering' => 'ASC']);

		$ids = $request->query->get('ids');
		$entityManager = $this->getDoctrine()->getManager();

		if ($ids != NULL) {
			$ids = str_replace('[', '', $ids);
			$ids = str_replace(']', '', $ids);
			$ids = explode(',', $ids);

			$allTrailers = new ArrayCollection($this->homepageRecommendationRepository->findAll());
			$trailers_ = [];
			$i = 1;

			foreach ($ids as $id) {
				$trailer = $this->homepageRecommendationRepository->find($id);
				$trailer->setOrdering($i);
				$trailers_[] = $trailer;
				try {
					$entityManager->persist($trailer);
					$entityManager->flush();
					$allTrailers->removeElement($trailer);
					$i++;
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			foreach ($allTrailers as $t) {
				$t->setOrdering($i);
				try {
					$entityManager->persist($t);
					$entityManager->flush();

					$i++;
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'Recommendations successfully reordered');
			return $this->redirectToRoute('admin_recommendations');
		}

		return $this->render('admin/homepage/homepage_recommendations/change_order.html.twig', [
			'recommendations' => $recommendations
		]);
	}

	/**
	 * @Route("/admin/remove-multiple/homepage-recommendations", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->homepageRecommendationRepository->find($id);

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($complaint);
			$entityManager->flush();

		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}