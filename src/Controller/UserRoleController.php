<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Form\RoleType;
use App\Repository\RoleRepository;
use App\Service\Paginator;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class UserRoleController extends AdminBaseController
{
	public $user;
	public $session;
	private $roleRepository;
	private $userRepository;

	public function __construct(
		RoleRepository $roleRepository,
		UserRepository $userRepository
	)
	{
		$this->roleRepository = $roleRepository;
		$this->userRepository = $userRepository;
		$this->session = new Session();
		$this->user = $this->session->get('user');
	}

	/**
	 * @Route("/admin/user-roles", name="admin_user_roles", schemes={"http"})
	 */
	public function index(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$userRoles = $this->roleRepository->findAll();

		$paginated = $paginator->paginate($userRoles, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/user-roles');


		return $this->render('/admin/users/user_roles/index.html.twig', [
			'userRoles' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($userRoles)
		]);
	}

	/**
	 * @Route("/admin/user-role/add", name="add_user_role", schemes={"http"})
	 */
	public function add(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$userRole = new Role();

		$form = $this->createForm(RoleType::class, $userRole);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($userRole);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'User role was successfully added. To limit the access for this role, contact a programmer');
				return $this->redirectToRoute('admin_user_roles');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/users/user_roles/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/user-roles/delete/{id}", name="delete_user_role", schemes={"http"})
	 */
	public function deleteCastRole($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$userRole = $this->roleRepository->find($id);

		if (!$userRole->getDeletable()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'This user role can\'t be deleted');
			return $this->redirectToRoute('admin_user_roles');
		}

		$user = $this->user;
		$flashbag = $this->session->getFlashBag();

		if ($user != NULL) {
			if ($this->userRepository->findBy(['roleId' => $userRole->getId()])) {
				$this->session->getFlashBag()->add('danger', 'Some users have this role assigned');

				return $this->redirectToReferer($request);
			} else {
				try {
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->remove($userRole);
					$entityManager->flush();

					$flashbag->add('primary', 'User role was successfully deleted');
					return $this->redirectToRoute('admin_user_roles');
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}
		}

		$flashbag->add('warning', 'You are not allowed to view this page');
		return $this->redirectToRoute('homepage');
	}
}