<?php

namespace App\Controller;

use App\Entity\PasswordReset;
use App\Entity\User;
use App\Form\LoginType;
use Gumlet\ImageResize;
use App\Form\SignUpType;
use App\Form\UserRoleType;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use App\Service\Paginator;
use Intervention\Image\File;
use App\Repository\PasswordResetRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\ChangePasswordType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\ForgottenPasswordType;


class UserController extends AdminBaseController
{
	public $filesystem;
	private $userRepository;
	private $roleRepository;
	private $passwordResetRepository;
	public $session;
	public $user;

	public function __construct(
		UserRepository $userRepository,
		RoleRepository $roleRepository,
		PasswordResetRepository $passwordResetRepository
	)
	{
		$this->filesystem = new Filesystem();
		$this->userRepository = $userRepository;
		$this->roleRepository = $roleRepository;
		$this->passwordResetRepository = $passwordResetRepository;
		$this->session = new Session();
		$this->user = $this->session->get('user');
	}

	/**
	 * @Route("/signup", name="signup", schemes={"http"})
	 */
	public function signup(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$user = new User();

		$form = $this->createForm(SignUpType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$user = $form->getData();

			$user->setPassword(password_hash($user->getPassword(), PASSWORD_DEFAULT));
			$role = $this->roleRepository->find(1);
			$user->setRole($role);

			$fileName = 'thumbnail.jpeg';
			$file = $form->get('profilePicture')->getData();

			if ($file != NULL) {
				$fileName = $fileUploader->upload($file, $this->getParameter('user_pictures_directory'), 200, 200, 500, 500);

				if ($fileName === FALSE) {
					$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

					return $this->redirectToReferer($request);
				}
			}

			$user->setProfilePicture($fileName);

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($user);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'You were successfully signed up!');

				return $this->redirectToRoute('login');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/user/signup.html.twig', [
			'form' => $form->createView()
		]);

	}

	/**
	 * @Route("/user/edit", name="user_edit", schemes={"http"})
	 */
	public function edit(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$user = $this->userRepository->find($this->user->getId());
		$originalProfilePicture = $user->getProfilePicture();
		$removeProfilePicture = FALSE;

		if ($originalProfilePicture != NULL && $originalProfilePicture != 'thumbnail.jpeg') {
			$removeProfilePicture = TRUE;
		}

		$form = $this->createForm(SignUpType::class, $user, [
			'edit' => TRUE,
			'removeProfilePicture' => $removeProfilePicture
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			if ($user->getProfilePicture() != NULL) {
				$fileName = 'thumbnail.jpeg';
				$file = $form->get('profilePicture')->getData();

				if ($file != NULL) {
					$fileName = $fileUploader->upload($file, $this->getParameter('user_pictures_directory'), 200, 200, 500, 500);

					if ($fileName === FALSE) {
						$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

						return $this->redirectToReferer($request);
					}
				}

				$user->setProfilePicture($fileName);

				if ($user->getProfilePicture() != 'thumbnail.jpeg' && $originalProfilePicture != 'thumbnail.jpeg') {
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . $originalProfilePicture);
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $originalProfilePicture);
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $originalProfilePicture);
				}
			} else {
				$user->setProfilePicture($originalProfilePicture);
			}

			if ($form->get('removeProfilePicture')->getData() === TRUE) {
				$user->setProfilePicture('thumbnail.jpeg');

				$this->filesystem->remove($this->getParameter('user_pictures_directory') . $originalProfilePicture);
				$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $originalProfilePicture);
				$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $originalProfilePicture);
			}

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->merge($user);
				$entityManager->flush();

				$this->session->set('user', $user);

				$this->addFlash('primary', 'Your info was successfully updated');

				return $this->redirectToRoute("profile", [
					'username' => $user->getUsername()
				]);
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/user/edit.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/login", name="login", schemes={"http"})
	 */
	public function login(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if ($this->session->get('user')) {
			$this->session->getFlashBag()->add('warning', 'You are already logged in!');

			return $this->redirectToRoute('homepage');
		}

		$user = new User();

		$form = $this->createForm(LoginType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$user = $form->getData();

			$userSearch = $this->userRepository->findOneBy([
				'emailAddress' => $user->getEmailAddress()
			]);

			if ($userSearch && password_verify($user->getPassword(), $userSearch->getPassword())) {
				$this->session->set('user', $userSearch);
				$this->session->getFlashBag()->add('primary', 'You were successfully logged in');

				return $this->redirectToRoute('homepage');
			} else {
				$this->session->getFlashBag()->add('danger', 'Invalid email or password');

				return $this->redirectToRoute('login');
			}
		}

		return $this->render('/user/login.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/logout", name="logout", schemes={"http"})
	 */
	public function logout(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$this->session->get('user');
		$this->session->invalidate();

		$this->session->getFlashBag()->add('info', 'The user was successfully logged out');

		return $this->redirectToRoute('login');
	}


	/**
	 * @Route("/user/change-password", schemes={"http"})
	 */
	public function changePassword(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$user = $this->userRepository->find($this->user->getId());
		$pass = $user->getPassword();

		$user_ = new User();

		$form = $this->createForm(ChangePasswordType::class, $user_, [
			'validation_groups' => 'change'
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			if (password_verify($user_->getPassword(), $pass)) {
				$form->addError(new FormError('You already use this password'));
			} else {
				$user->setPassword(password_hash($user_->getPassword(), PASSWORD_DEFAULT));

				try {
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->merge($user);
					$entityManager->flush();

					$this->session->set('user', $user);

					$this->addFlash('primary', 'Your password was successfully changed');

					return $this->redirectToRoute("profile", [
						'username' => $user->getUsername()
					]);
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}
		}

		return $this->render('/user/change_password.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/user/{username}", name="profile", schemes={"http"})
	 */
	public function profile($username)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$userSearch = $this->userRepository->findOneBy([
			'username' => $username
		]);

		if ($userSearch === NULL) {
			return $this->render('404.html.twig');
		}

		return $this->render('/user/profile.html.twig', [
			'userSearch' => $userSearch,
			'requestedUser' => $username
		]);
	}

	/**
	 * @Route("/admin/users", name="admin_users", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$users = $this->userRepository->findAll();

		$paginated = $paginator->paginate($users, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/users');

		return $this->render('/admin/users/users/index.html.twig', [
			'users' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($users)
		]);
	}

	/**
	 * @Route("/admin/user/{id}/assign-role", name="assign_user_role", schemes={"http"})
	 */
	public function assignUserRole($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$user = $this->userRepository->find($id);

		if ($this->user->getId() === $user->getId()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'You can\'t change your own role');
			return $this->redirectToRoute('admin_users');
		}

		if ($user->getRoleId() === 3) {
			$this->addFlash('warning', 'This user\'s role can\'t be changed');

			return $this->redirectToRoute('admin_users');
		} else {
			$form = $this->createForm(UserRoleType::class, $user);

			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				try {
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->persist($user);
					$entityManager->flush();

					$this->session->getFlashBag()->add('primary', 'User role was successfully assigned to user');
					return $this->redirectToRoute('admin_users');
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			return $this->render('/admin/users/users/assign_role.html.twig', [
				'form' => $form->createView()
			]);
		}
	}

	/**
	 * @Route("/admin/user/edit/{id}", name="admin_user_edit", schemes={"http"})
	 */
	public function adminEdit($id, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$user = $this->userRepository->find($id);

		if ($this->user->getId() === $user->getId()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'You can\'t edit yourself in the administration');
			return $this->redirectToRoute('admin_users');
		}

		$originalProfilePicture = $user->getProfilePicture();
		$removeProfilePicture = FALSE;

		if ($originalProfilePicture != NULL && $originalProfilePicture != 'thumbnail.jpeg') {
			$removeProfilePicture = TRUE;
		}

		$form = $this->createForm(SignUpType::class, $user, [
			'admin' => TRUE,
			'removeProfilePicture' => $removeProfilePicture
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			if ($user->getProfilePicture() != NULL) {
				$fileName = 'thumbnail.jpeg';
				$file = $form->get('profilePicture')->getData();

				if ($file != NULL) {
					$fileName = $fileUploader->upload($file, $this->getParameter('user_pictures_directory'), 200, 200, 500, 500);

					if ($fileName === FALSE) {
						$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

						return $this->redirectToReferer($request);
					}
				}

				$user->setProfilePicture($fileName);

				if ($user->getProfilePicture() != 'thumbnail.jpeg' && $originalProfilePicture != 'thumbnail.jpeg') {
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . $originalProfilePicture);
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $originalProfilePicture);
					$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $originalProfilePicture);
				}
			} else {
				$user->setProfilePicture($originalProfilePicture);
			}

			if ($form->get('removeProfilePicture')->getData() === TRUE) {
				$user->setProfilePicture('thumbnail.jpeg');

				$this->filesystem->remove($this->getParameter('user_pictures_directory') . $originalProfilePicture);
				$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $originalProfilePicture);
				$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $originalProfilePicture);
			}

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($user);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'The user was successfully edited');
				return $this->redirectToRoute('admin_users');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/users/users/edit.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/user/delete/{id}", name="admin_user_delete", schemes={"http"})
	 */
	public function adminDelete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$user = $this->userRepository->find($id);

		if ($this->user->getId() === $user->getId()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'You can\'t delete yourself');
			return $this->redirectToRoute('admin_users');
		}

		if ($user->getProfilePicture() != 'thumbnail.jpeg') {
			$this->filesystem->remove($this->getParameter('user_pictures_directory') . $user->getProfilePicture());
			$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $user->getProfilePicture());
			$this->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $user->getProfilePicture());
		}

		try {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($user);
			$entityManager->flush();
			$this->session->getFlashBag()->add('primary', 'The user was successfully deleted');
			return $this->redirectToRoute('admin_users');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}

	}

	/**
	 * @Route("/user/{username}/load-more", schemes={"http"})
	 */
	public function loadMore($username, Request $request)
	{
		$data = $request->query->get('count');

		$user = $this->userRepository->findOneBy(['username' => $username]);
		$appearances = [];
		$count = count($user->getReviews()->toArray());
		$last = FALSE;

		if (empty(array_slice($user->getReviews()->toArray(), $data + 5, 6))) {
			$last = TRUE;
		}

		foreach (array_slice($user->getReviews()->toArray(), $data, 5) as $review) {
			$type = $review->getMovieOrTvShow()->getType();
			if ($type === 'tv_show') {
				$type = 'tv-show';
				$imgUrl = 'tvshows';
			} else {
				$imgUrl = 'movies';
			}

			$appearances[] = [
				'title' => $review->getMovieOrTvShow()->getTitle(),
				'thumbnail' => $review->getMovieOrTvShow()->getThumbnail(),
				'imgUrl' => $imgUrl,
				'slug' => $review->getMovieOrTvShow()->getSlug(),
				'body' => $review->getBody(),
				'type' => $type
			];
		}

		return new JsonResponse([
			$appearances,
			$last
		]);
	}

	/**
	 * @Route("/forgotten-password", name="forgotten_password", schemes={"http"})
	 */
	public function forgottenPassword(Request $request, \Swift_Mailer $mailer)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if ($this->isLoggedIn()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view this page while logged in');
			return $this->redirectToRoute('homepage');
		}

		$user = new User();

		$form = $this->createForm(ForgottenPasswordType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$user = $this->userRepository->findOneBy(['emailAddress' => $form->get('emailAddress')->getData()]);

			if ($user === NULL) {
				$form->addError(new FormError('No user found with this email address'));
			} else {
				if ($this->passwordResetRepository->findOneBy(['emailAddress' => $user->getEmailAddress()])) {
					$this->session->getFlashBag()->add('warning', 'You have already requested a password reset');
					return $this->redirectToRoute('forgotten_password');
				}

				$verificationCode = uniqid();
				$passwordReset = new PasswordReset();
				$passwordReset->setEmailAddress($user->getEmailAddress());
				$passwordReset->setVerificationCode($verificationCode);

				try {
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->persist($passwordReset);
					$entityManager->flush();

					try {
						$message = (new \Swift_Message('Forgotten password'))
							->setFrom('support@telecule.com')
							->setTo($user->getEmailAddress())
							->setBody(
								$this->renderView('email/forgotten_password.html.twig', [
									'user_' => $user,
									'verificationCode' => $verificationCode
								]),
								'text/html'
							);

						$mailer->send($message);

						$this->session->getFlashBag()->add('primary', 'Check your inbox to complete your password reset');
						return $this->redirectToRoute('login');
					} catch (\Exception $e) {
						$this->session->getFlashBag()->add('danger', $e->getMessage());
						return $this->redirectToReferer($request);
					}
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}

			}
		}

		return $this->render('/user/forgotten_password.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/reset-password/{verificationCode}", schemes={"http"})
	 */
	public function resetPassword($verificationCode, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$resetPasswordRequest = $this->passwordResetRepository->findOneBy(['verificationCode' => $verificationCode]);

		if ($resetPasswordRequest != NULL) {
			$user = $this->userRepository->findOneBy(['emailAddress' => $resetPasswordRequest->getEmailAddress()]);
			$pass = $user->getPassword();

			$user_ = new User();

			$form = $this->createForm(ChangePasswordType::class, $user_, [
				'validation_groups' => 'change'
			]);

			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				if (password_verify($user_->getPassword(), $pass)) {
					$form->addError(new FormError('You already use this password'));
				} else {
					$user->setPassword(password_hash($user_->getPassword(), PASSWORD_DEFAULT));

					try {
						$entityManager = $this->getDoctrine()->getManager();
						$entityManager->merge($user);
						$entityManager->remove($resetPasswordRequest);
						$entityManager->flush();

						$this->addFlash('primary', 'Your password was successfully changed');

						return $this->redirectToRoute('login');
					} catch (\Exception $e) {
						$this->session->getFlashBag()->add('danger', $e->getMessage());
						return $this->redirectToReferer($request);
					}
				}
			}
		} else {
			$this->session->getFlashBag()->add('warning', 'You are not allowed to view the requested page');
			return $this->redirectToRoute('homepage');
		}

		return $this->render('/user/change_password.html.twig', [
			'form' => $form->createView(),
			'user_' => $user
		]);
	}

	/**
	 * @Route("/test", schemes={"http"})
	 */
	public function test(\Swift_Mailer $mailer)
	{
		$user = $this->userRepository->find(1);
		$verificationCode = 'ahojda :)';

		$message = (new \Swift_Message('Forgotten password'))
			->setFrom('support@telecule.com')
			->setTo($user->getEmailAddress())
			->setBody(
				$this->renderView('email/forgotten_password.html.twig', [
					'user_' => $user,
					'verificationCode' => $verificationCode
				]),
				'text/html'
			);

		$mailer->send($message);
	}

	/**
	 * @Route("/admin/remove-multiple/users", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->userRepository->find($id);

			if ($complaint->getProfilePicture() != 'thumbnail.jpeg') {
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('user_pictures_directory') . $complaint->getProfilePicture());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('user_pictures_directory') . 'small/' . $complaint->getProfilePicture());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('user_pictures_directory') . 'normal/' . $complaint->getProfilePicture());
			}

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($complaint);
			$entityManager->flush();
		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}
