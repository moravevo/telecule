<?php

namespace App\Controller;

use App\Form\MovieAndTvShowSearchType;
use App\Controller\BaseController\MovieAndTvShowBaseController;
use App\Entity\UserReview;
use Couchbase\RegexpSearchQuery;
use Gumlet\ImageResize;
use App\Entity\MovieAndTvShow;
use App\Form\MovieAndTvShowType;
use App\Form\UserReviewType;
use App\Service\Slugger;
use App\Entity\CastMember;
use App\Form\CastMemberType;
use App\Repository\UserReviewRepository;
use App\Repository\UserRepository;
use App\Service\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class TvShowController extends AdminBaseController
{
	private $movieAndTvShowBaseController;
	public $session;
	public $user;
	private $userReviewRepository;
	private $userRepository;
	public $slugger;

	public function __construct(
		MovieAndTvShowBaseController $movieAndTvShowBaseController,
		UserReviewRepository $userReviewRepository,
		UserRepository $userRepository
	)
	{
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
		$this->session = new Session();
		$this->user = $this->session->get('user');
		$this->userRepository = $userRepository;
		$this->userReviewRepository = $userReviewRepository;
		$this->slugger = new Slugger();
	}

	/**
	 * @Route("/tv-shows", name="tvshows", schemes={"http"})
	 */
	public function index(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$form = $this->createForm(MovieAndTvShowSearchType::class, NULL, [
			'type' => 'tv_show'
		]);

		$form->handleRequest($request);

		$result = NULL;

		if ($form->isSubmitted() && $form->isValid()) {
			if (!$form->get('advancedSearch')->getData() && $form->get('title')->getData() === NULL) {
				$form->get('title')->addError(new FormError('This field is required'));
			} else {
				if (!$form->get('advancedSearch')->getData()) {
					$lowestPremiereDate = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findOldestByType('tv_show'));
					$highestPremiereDate = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findNewestByType('tv_show'));
					$includeWithoutPremiere = TRUE;
					$ratingLow = "0";
					$ratingHigh = "10";
					$includeWithoutRating = TRUE;
					$minLength = "0";
					$maxLength = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxLength();
					$includeWithoutLength = TRUE;
					$genres = $this->movieAndTvShowBaseController->genreRepository->findAll();
					$includeWithoutGenres = TRUE;
				} else {
					$lowestPremiereDate = date_create_from_format('Y', $form->get('lowestPremiereDate')->getData());
					$highestPremiereDate = date_create_from_format('Y', $form->get('highestPremiereDate')->getData());
					$includeWithoutPremiere = $form->get('includeWithoutPremiere')->getData();
					$ratingLow = $form->get('ratingLow')->getData();
					$ratingHigh = $form->get('ratingHigh')->getData();
					$includeWithoutRating = $form->get('includeWithoutRating')->getData();
					$minLength = "0";
					$maxLength = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxLength();
					$includeWithoutLength = TRUE;
					$genres = $form->get('genres')->getData();
					$includeWithoutGenres = FALSE;

					if ($genres[0] === NULL) {
						$includeWithoutGenres = TRUE;
					}
				}

				$results = $this->movieAndTvShowBaseController->movieAndTvShowRepository->search(
					'tv_show',
					$form->get('title')->getData(),
					$lowestPremiereDate,
					$highestPremiereDate,
					$includeWithoutPremiere,
					$ratingLow,
					$ratingHigh,
					$includeWithoutRating,
					$minLength,
					$maxLength,
					$includeWithoutLength,
					$genres,
					$includeWithoutGenres
				);

				return $this->render('/tv_shows/search_results.html.twig', [
					'form' => $form->createView(),
					'results' => $results,
					'searchedString' => $form->get('title')->getData(),
					'returnedResults' => count($results)
				]);
			}
		}

		$recentlyAdded = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'tv_show'
			],
			[
				'dateCreated' => 'DESC'
			],
			25
		);

		$bestRatedTvShows = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'tv_show'
			],
			[
				'rating' => 'DESC'
			],
			10
		);

		$tvShows = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['type' => 'tv_show']);

		return $this->render('/tv_shows/index.html.twig', [
			'form' => $form->createView(),
			'count' => count($tvShows),
			'recentlyAdded' => $recentlyAdded,
			'bestRatedTvShows' => $bestRatedTvShows
		]);
	}

	/**
	 * @Route("/tv-show/{slug}", name="tv_show_detail", schemes={"http"})
	 */
	public function detail($slug, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findOneBy([
			'type' => 'tv_show',
			'slug' => $slug
		]);

		if ($tvShow === NULL) {
			return $this->render('404.html.twig');
		}

		$tvShow->setVisits($tvShow->getVisits() + 1);
		$this->movieAndTvShowBaseController->entityManager->persist($tvShow);
		$this->movieAndTvShowBaseController->entityManager->flush();

		$userReview = new UserReview();
		$form = $this->createForm(UserReviewType::class, $userReview);

		if ($this->movieAndTvShowBaseController->handleUserReview($form, $request, $tvShow)) {
			return $this->redirect($request->getUri());
		}

		if ($this->movieAndTvShowBaseController->userReviewRepository->findOneBy(['user' => $this->user, 'movieOrTvShow' => $tvShow])) {
			$form_ = NULL;
		} else {
			$form_ = $form->createView();
		}

		$highlightedReviewAuthor = $this->userRepository->findOneBy([
			'username' => $request->get('highlightedReview')
		]);

		$highlightedReview = NULL;

		$reviews = $tvShow->getUserReviews();
		$reviewCount = $reviews->count();
		$sum = NULL;

		foreach ($reviews as $review) {
			$sum += $review->getRating();
		}

		if ($sum != NULL && $reviewCount != 0) {
			$average = $sum / $reviewCount;
		} else {
			$average = NULL;
		}

		if ($highlightedReviewAuthor != NULL) {
			$highlightedReview = $this->userReviewRepository->findOneBy([
				'userId' => $highlightedReviewAuthor->getId(),
				'movieOrTvShowId' => $tvShow->getId()
			]);

			if ($highlightedReview != NULL) {
				$reviews->removeElement($highlightedReview);
			}
		}

		$reviews = $reviews->slice(0, 5);

		$view = $this->render('/tv_shows/detail.html.twig', [
			'movieOrTvShow' => $tvShow,
			'form' => $form_,
			'similar' => $this->movieAndTvShowBaseController->getSimilar($tvShow),
			'highlightedReview' => $highlightedReview,
			'reviews' => $reviews,
			'averageRaw' => $average,
			'average' => round($average, 1)
		]);

		return $view;
	}

	/**
	 * @Route("/admin/tv-shows", name="admin_tv_shows", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$tvShows = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['type' => 'tv_show']);

		$paginated = $paginator->paginate($tvShows, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/tv-shows');

		return $this->render('/admin/movies_and_tv_shows/tv_shows/index.html.twig', [
			'tvShows' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($tvShows)
		]);
	}

	/**
	 * @Route("/admin/tv-show/add", name="add_tv_show", schemes={"http"})
	 */
	public function add(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movieOrTvShow = new MovieAndTvShow();

		$form = $this->createForm(MovieAndTvShowType::class, $movieOrTvShow, [
			'type' => 'tv_show'
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();
			$slug = $this->slugger->createSlug($movieOrTvShow->getTitle());

			if ($movieOrTvShow->getPremiere() === NULL) {
				$movieOrTvShow->setPremiere(NULL);
			}

			if ($this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['slug' => $slug])) {
				if ($movieOrTvShow->getPremiere() === NULL) {
					$slug = $slug . '-' . uniqid();
				} else {
					$slug = $slug . '-' . $movieOrTvShow->getPremiere()->format('Y');
				}
			}

			$movieOrTvShow->setSlug($slug);

			$fileName = 'thumbnail.jpeg';
			$file = $form->get('thumbnail')->getData();

			if ($file != NULL) {
				$fileName = $fileUploader->upload($file, $this->getParameter($type . '_thumbnails_directory'), 160, 225, 736, 1035);

				if ($fileName === FALSE) {
					$this->session->getFlashBag()->add('danger', 'The file is too large. Maximum allowed size is 2MB.');
					return $this->redirectToReferer($request);
				}
			}

			$movieOrTvShow->setThumbnail($fileName);

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($movieOrTvShow);
				$entityManager->flush();

				$typeReadable = ($type === 'movie') ? 'Movie' : 'TV Show';

				$this->session->getFlashBag()->add('primary', $typeReadable . ' was successfully added');
				return $this->redirectToRoute('admin_tv_shows');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}


		return $this->render('/admin/movies_and_tv_shows/tv_shows/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/tv-shows/delete/{id}", name="delete_tv_show", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->find($id);

		$this->movieAndTvShowBaseController->removeMovieOrTvShow($tvShow, $request);

		return $this->redirectToRoute('admin_tv_shows');
	}

	/**
	 * @Route("/admin/tv-shows/edit/{id}", name="edit_tv_show", schemes={"http"})
	 */
	public function edit($id, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movieOrTvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->find($id);
		$originalThumbnail = $movieOrTvShow->getThumbnail();
		$originalTitle = $movieOrTvShow->getTitle();
		$withoutRating = FALSE;
		$removeThumbnail = FALSE;

		if ($movieOrTvShow->getRating() === NULL) {
			$withoutRating = TRUE;
		}

		if ($originalThumbnail != NULL && $originalThumbnail != 'thumbnail.jpeg') {
			$removeThumbnail = TRUE;
		}

		$missingInfo = $this->movieAndTvShowBaseController->getMissingInfo($movieOrTvShow);

		$form = $this->createForm(MovieAndTvShowType::class, $movieOrTvShow, [
			'edit' => TRUE,
			'type' => 'tv_show',
			'withoutRating' => $withoutRating,
			'removeThumbnail' => $removeThumbnail
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();

			if ($movieOrTvShow->getThumbnail() != NULL) {
				$fileName = 'thumbnail.jpeg';
				$file = $form->get('thumbnail')->getData();

				if ($file != NULL) {
					$fileName = $fileUploader->upload($file, $this->getParameter($type . '_thumbnails_directory'), 160, 225, 736, 1035);

					if ($fileName === FALSE) {
						$this->session->getFlashBag()->add('danger', 'The file is too large. Maximum allowed size is 2MB.');
						return $this->redirectToReferer($request);
					}
				}

				$movieOrTvShow->setThumbnail($fileName);

				if ($movieOrTvShow->getThumbnail() != 'thumbnail.jpeg' && $originalThumbnail != 'thumbnail.jpeg') {
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $originalThumbnail);
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $originalThumbnail);
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $originalThumbnail);
				}
			} else {
				$movieOrTvShow->setThumbnail($originalThumbnail);
			}

			if ($form->get('removeThumbnail') && $form->get('removeThumbnail')->getData() === TRUE) {
				$movieOrTvShow->setThumbnail('thumbnail.jpeg');

				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $originalThumbnail);
				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $originalThumbnail);
				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $originalThumbnail);
			}

			if ($movieOrTvShow->getLength() === 0) {
				$movieOrTvShow->setLength(NULL);
			}

			if ($originalTitle != $movieOrTvShow->getTitle()) {
				$slug = $this->slugger->createSlug($movieOrTvShow->getTitle());

				if ($this->movieAndTvShowRepository->findBy(['slug' => $slug])) {
					$slug = $slug . '-' . $movieOrTvShow->getPremiere()->format('Y');
				}

				$movieOrTvShow->setSlug($slug);
			}

			$entityManager = $this->getDoctrine()->getManager();

			foreach ($movieOrTvShow->getCastMembers() as $castMember) {
				$castMember->setPremiere($movieOrTvShow->getPremiere());
				$entityManager->persist($castMember);
				$entityManager->flush();
			}

			try {
				$entityManager->persist($movieOrTvShow);
				$entityManager->flush();
				$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'The movie ' . $movieOrTvShow->getTitle() . ' was successfully edited');
				return $this->redirectToReferer($request);
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}

		}

		$similar = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findSimilar($movieOrTvShow);

		return $this->render('/admin/movies_and_tv_shows/tv_shows/edit.html.twig', [
			'form' => $form->createView(),
			'movieOrTvShow' => $movieOrTvShow,
			'castMembers' => $movieOrTvShow->getCastMembers(),
			'similar' => $similar,
			'missingInfo' => $missingInfo
		]);
	}

	/**
	 * @Route("/admin/tv-show/{id}/new-cast-member", name="assign_tv_show_cast_member", schemes={"http"})
	 */
	public function assignCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->find($id);

		$castMember = new CastMember();
		$form = $this->createForm(CastMemberType::class, $castMember);

		if ($this->movieAndTvShowBaseController->assignCastMember($form, $request, $tvShow, $castMember)) {
			return $this->redirectToRoute('edit_tv_show', [
				'id' => $id,
				'_fragment' => 'tab-cast'
			]);
		}

		return $this->render('/admin/movies_and_tv_shows/tv_shows/cast.html.twig', [
			'form' => $form->createView(),
			'tvShow' => $tvShow
		]);
	}

	/**
	 * @Route("/admin/tv-show/cast-member/delete/{id}", schemes={"http"})
	 */
	public function removeCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->movieAndTvShowBaseController->castMemberRepository->find($id);
		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->find($castMember->getMovieOrTvShowId());

		if ($this->movieAndTvShowBaseController->removeCastMember($castMember, $request)) {
			$this->movieAndTvShowBaseController->flashbag->add('primary', 'The cast member was successfully removed from this tv show');
		} else {
			$this->movieAndTvShowBaseController->flashbag->add('danger', 'Something went wrong');
		}

		return $this->redirectToRoute('edit_tv_show', [
			'id' => $tvShow->getId(),
			'_fragment' => 'tab-cast'
		]);
	}

	/**
	 * @Route("/admin/tv-show/cast-member/edit/{id}", name="edit_tv_show_cast_member", schemes={"http"})
	 */
	public function editCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->movieAndTvShowBaseController->castMemberRepository->find($id);
		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->find($castMember->getMovieOrTvShowId());

		$form = $this->createForm(CastMemberType::class, $castMember, [
			'edit' => TRUE
		]);

		if ($this->movieAndTvShowBaseController->editCastMember($form, $request, $castMember)) {
			return $this->redirectToRoute('edit_tv_show', [
				'id' => $tvShow->getId(),
				'_fragment' => 'tab-cast'
			]);
		}

		return $this->render('/admin/movies_and_tv_shows/tv_shows/cast_edit.html.twig', [
			'form' => $form->createView(),
			'_fragment' => 'tab-cast'
		]);
	}

	/**
	 * @Route("/tv-shows/leaderboard", name="tvShow_leaderboard", schemes={"http"})
	 */
	public function leaderboard()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$bestRatedTvShows = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'tv_show'
			],
			[
				'rating' => 'DESC'
			],
			100
		);

		return $this->render('/tv_shows/leaderboard.html.twig', [
			'bestRatedTvShows' => $bestRatedTvShows,
			'count' => count($bestRatedTvShows)
		]);
	}

	/**
	 * @Route("/tv-shows/resize", schemes={"http"})
	 */
	public function resize()
	{
		$movies = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findjpg();

		foreach ($movies as $user) {
			$originalPicture = $user->getThumbnail();

			if ($originalPicture != 'thumbnail.jpeg') {
				$fileWithExtension = $originalPicture;
				$filename = str_replace('.jpeg', '', $fileWithExtension);

				$image = new ImageResize($this->getParameter('tv_show_thumbnails_directory') . $fileWithExtension, IMAGETYPE_JPEG);
				$image->crop(225, 160);
				$image->save($this->getParameter('tv_show_thumbnails_directory') . 'small/' . $filename . '.jpeg', IMAGETYPE_JPEG);
			}
		}
	}

	/**
	 * @Route("/tv-show/{slug}/load-more", schemes={"http"})
	 */
	public function loadMore($slug, Request $request)
	{
		$data = $request->query->get('count');
		$hr = $request->query->get('highlightedReview');
		$user = $this->userRepository->findOneBy(['username' => $hr]);

		$tvShow = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findOneBy(['slug' => $slug, 'type' => 'tv_show']);
		$reviews = $tvShow->getUserReviews();

		if ($user != NULL) {
			$highlightedReview = $this->userReviewRepository->findOneBy([
				'user' => $user,
				'movieOrTvShow' => $this->movieAndTvShowBaseController->movieAndTvShowRepository->findOneBy(['slug' => $slug, 'type' => 'tv_show'])
			]);

			if ($highlightedReview != NULL) {
				$reviews->removeElement($highlightedReview);
			}
		}

		$reviews = $reviews->slice($data, 5);
		$return = [];
		$last = FALSE;

		if (empty($tvShow->getUserReviews()->slice($data + 5, 6))) {
			$last = TRUE;
		}

		foreach ($reviews as $review) {
			$return[] = [
				'body' => $review->getBody(),
				'photo' => $review->getUser()->getProfilePicture(),
				'rating' => $review->getRating(),
				'date' => $review->getDateCreated()->format('d. m. Y'),
				'username' => $review->getUser()->getUsername()
			];
		}

		return new JsonResponse([
			$return,
			$last
		]);
	}
}