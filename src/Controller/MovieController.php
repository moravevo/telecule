<?php

namespace App\Controller;

use App\Controller\BaseController\MovieAndTvShowBaseController;
use App\Entity\CastMember;
use App\Entity\MovieAndTvShow;
use App\Entity\UserReview;
use App\Form\CastMemberType;
use App\Form\MovieAndTvShowSearchType;
use App\Form\MovieAndTvShowType;
use App\Form\UserReviewType;
use App\Repository\CastMemberRepository;
use App\Repository\UserRepository;
use App\Repository\MovieAndTvShowRepository;
use App\Service\FileUploader;
use App\Service\Paginator;
use App\Service\Slugger;
use Gumlet\ImageResize;
use Symfony\Component\Filesystem\Filesystem;
use App\Repository\UserReviewRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class MovieController extends AdminBaseController
{
	public $filesystem;
	public $session;
	private $slugger;
	public $user;
	private $movieAndTvShowRepository;
	private $userReviewRepository;
	private $castMemberRepository;
	private $movieAndTvShowBaseController;
	private $userRepository;

	public function __construct(
		MovieAndTvShowBaseController $movieAndTvShowBaseController,
		MovieAndTvShowRepository $movieAndTvShowRepository,
		UserReviewRepository $userReviewRepository,
		UserRepository $userRepository,
		CastMemberRepository $castMemberRepository
	)
	{
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
		$this->filesystem = new Filesystem();
		$this->movieAndTvShowRepository = $movieAndTvShowRepository;
		$this->session = new Session();
		$this->slugger = new Slugger();
		$this->user = $this->session->get('user');
		$this->userRepository = $userRepository;
		$this->userReviewRepository = $userReviewRepository;
		$this->castMemberRepository = $castMemberRepository;
	}

	/**
	 * @Route("/movies", name="movies", schemes={"http"})
	 */
	public function index(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$form = $this->createForm(MovieAndTvShowSearchType::class);

		$form->handleRequest($request);

		$result = NULL;

		if ($form->isSubmitted() && $form->isValid()) {
			if (!$form->get('advancedSearch')->getData() && $form->get('title')->getData() === NULL) {
				$form->get('title')->addError(new FormError('This field is required'));
			} else {
				if (!$form->get('advancedSearch')->getData()) {
					$lowestPremiereDate = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findOldestByType('movie'));
					$highestPremiereDate = new \DateTime($this->movieAndTvShowBaseController->movieAndTvShowRepository->findNewestByType('movie'));
					$includeWithoutPremiere = TRUE;
					$ratingLow = "0";
					$ratingHigh = "10";
					$includeWithoutRating = TRUE;
					$minLength = "0";
					$maxLength = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxLength();
					$includeWithoutLength = TRUE;
					$genres = $this->movieAndTvShowBaseController->genreRepository->findAll();
					$includeWithoutGenres = TRUE;
				} else {
					$lowestPremiereDate = date_create_from_format('Y', $form->get('lowestPremiereDate')->getData());
					$highestPremiereDate = date_create_from_format('Y', $form->get('highestPremiereDate')->getData());
					$includeWithoutPremiere = $form->get('includeWithoutPremiere')->getData();
					$ratingLow = $form->get('ratingLow')->getData();
					$ratingHigh = $form->get('ratingHigh')->getData();
					$includeWithoutRating = $form->get('includeWithoutRating')->getData();
					$minLength = $form->get('minLength')->getData();
					$maxLength = $form->get('maxLength')->getData();
					$includeWithoutLength = $form->get('includeWithoutLength')->getData();
					$genres = $form->get('genres')->getData();
					$includeWithoutGenres = FALSE;

					if ($genres[0] === NULL) {
						$includeWithoutGenres = TRUE;
					}
				}

				$results = $this->movieAndTvShowBaseController->movieAndTvShowRepository->search(
					'movie',
					$form->get('title')->getData(),
					$lowestPremiereDate,
					$highestPremiereDate,
					$includeWithoutPremiere,
					$ratingLow,
					$ratingHigh,
					$includeWithoutRating,
					$minLength,
					$maxLength,
					$includeWithoutLength,
					$genres,
					$includeWithoutGenres
				);

				return $this->render('/movies/search_results.html.twig', [
					'form' => $form->createView(),
					'results' => $results,
					'searchedString' => $form->get('title')->getData(),
					'returnedResults' => count($results)
				]);
			}
		}

		$recentlyAdded = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'movie'
			],
			[
				'dateCreated' => 'DESC'
			],
			25
		);

		$bestRatedMovies = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'movie'
			],
			[
				'rating' => 'DESC'
			],
			10
		);

		$movies = $this->movieAndTvShowRepository->findBy(['type' => 'movie']);

		return $this->render('/movies/index.html.twig', [
			'form' => $form->createView(),
			'count' => count($movies),
			'recentlyAdded' => $recentlyAdded,
			'bestRatedMovies' => $bestRatedMovies
		]);
	}

	/**
	 * @Route("/movie/{slug}", name="movie_detail", schemes={"http"})
	 */
	public function detail($slug, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$movie = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findOneBy([
			'type' => 'movie',
			'slug' => $slug
		]);

		if ($movie === NULL) {
			return $this->render('404.html.twig');
		}

		$movie->setVisits($movie->getVisits() + 1);
		$this->movieAndTvShowBaseController->entityManager->persist($movie);
		$this->movieAndTvShowBaseController->entityManager->flush();

		$userReview = new UserReview();
		$form = $this->createForm(UserReviewType::class, $userReview);

		if ($this->movieAndTvShowBaseController->handleUserReview($form, $request, $movie)) {
			return $this->redirect($request->getUri());
		}

		$today = new \DateTime();
		$premiere = $movie->getPremiere();

		if ($this->userReviewRepository->findOneBy(['user' => $this->user, 'movieOrTvShow' => $movie]) || $today < $premiere) {
			$form_ = NULL;
		} else {
			$form_ = $form->createView();
		}

		$highlightedReviewAuthor = $this->userRepository->findOneBy([
			'username' => $request->get('highlightedReview')
		]);

		$highlightedReview = NULL;

		$reviews = $movie->getUserReviews();
		$reviewCount = $reviews->count();
		$sum = NULL;

		foreach ($reviews as $review) {
			$sum += $review->getRating();
		}

		if ($sum != NULL && $reviewCount != 0) {
			$average = $sum / $reviewCount;
		} else {
			$average = NULL;
		}

		if ($highlightedReviewAuthor != NULL) {
			$highlightedReview = $this->userReviewRepository->findOneBy([
				'userId' => $highlightedReviewAuthor->getId(),
				'movieOrTvShowId' => $movie->getId()
			]);

			if ($highlightedReview != NULL) {
				$reviews->removeElement($highlightedReview);
			}
		}

		$reviews = $reviews->slice(0, 5);

		return $this->render('/movies/detail.html.twig', [
			'movieOrTvShow' => $movie,
			'form' => $form_,
			'similar' => $this->movieAndTvShowBaseController->getSimilar($movie),
			'highlightedReview' => $highlightedReview,
			'reviews' => $reviews,
			'averageRaw' => $average,
			'average' => round($average, 1)
		]);
	}

	/**
	 * @Route("/admin/movies", name="admin_movies", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movies = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['type' => 'movie']);

		$paginated = $paginator->paginate($movies, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/movies');

		return $this->render('/admin/movies_and_tv_shows/movies/index.html.twig', [
			'movies' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($movies)
		]);
	}

	/**
	 * @Route("/admin/movie/add", name="add_movie", schemes={"http"})
	 */
	public function addMovie(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movieOrTvShow = new MovieAndTvShow();

		$form = $this->createForm(MovieAndTvShowType::class, $movieOrTvShow, [
			'type' => 'movie'
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();
			$slug = $this->slugger->createSlug($movieOrTvShow->getTitle());

			if ($movieOrTvShow->getPremiere() === NULL) {
				$movieOrTvShow->setPremiere(NULL);
			}

			if ($this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(['slug' => $slug])) {
				if ($movieOrTvShow->getPremiere() === NULL) {
					$slug = $slug . '-' . uniqid();
				} else {
					$slug = $slug . '-' . $movieOrTvShow->getPremiere()->format('Y');
				}
			}

			$movieOrTvShow->setSlug($slug);

			$fileName = 'thumbnail.jpeg';
			$file = $form->get('thumbnail')->getData();

			if ($file != NULL) {
				$fileName = $fileUploader->upload($file, $this->getParameter($type . '_thumbnails_directory'), 160, 225, 736, 1035);

				if ($fileName === FALSE) {
					$this->session->getFlashBag()->add('danger', 'The file is too large. Maximum allowed size is 2MB.');
					return $this->redirectToReferer($request);
				}
			}

			$movieOrTvShow->setThumbnail($fileName);

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($movieOrTvShow);
				$entityManager->flush();
				$typeReadable = ($type === 'movie') ? 'Movie' : 'TV Show';

				$this->session->getFlashBag()->add('primary', $typeReadable . ' was successfully added');
				return $this->redirectToRoute('admin_movies');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/movies_and_tv_shows/movies/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/movies/delete/{id}", name="delete_movie", schemes={"http"})
	 */
	public function deleteMovie($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movie = $this->movieAndTvShowRepository->find($id);

		$this->movieAndTvShowBaseController->removeMovieOrTvShow($movie, $request);

		return $this->redirectToRoute('admin_movies');
	}

	/**
	 * @Route("/admin/movies/edit/{id}", name="edit_movie", schemes={"http"})
	 */
	public function edit($id, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movieOrTvShow = $this->movieAndTvShowRepository->find($id);
		$originalThumbnail = $movieOrTvShow->getThumbnail();
		$withoutRating = FALSE;
		$removeThumbnail = FALSE;
		$originalTitle = $movieOrTvShow->getTitle();

		if ($movieOrTvShow->getRating() === NULL) {
			$withoutRating = TRUE;
		}

		if ($originalThumbnail != NULL && $originalThumbnail != 'thumbnail.jpeg') {
			$removeThumbnail = TRUE;
		}

		$form = $this->createForm(MovieAndTvShowType::class, $movieOrTvShow, [
			'edit' => TRUE,
			'type' => 'movie',
			'withoutRating' => $withoutRating,
			'removeThumbnail' => $removeThumbnail
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();

			if ($movieOrTvShow->getThumbnail() != NULL) {
				$fileName = 'thumbnail.jpeg';
				$file = $form->get('thumbnail')->getData();

				if ($file != NULL) {
					$fileName = $fileUploader->upload($file, $this->getParameter($type . '_thumbnails_directory'), 160, 225, 736, 1035);

					if ($fileName === FALSE) {
						$this->session->getFlashBag()->add('danger', 'The file is too large. Maximum allowed size is 2MB.');
						return $this->redirectToReferer($request);
					}
				}

				$movieOrTvShow->setThumbnail($fileName);

				if ($movieOrTvShow->getThumbnail() != 'thumbnail.jpeg' && $originalThumbnail != 'thumbnail.jpeg') {
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $originalThumbnail);
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $originalThumbnail);
					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $originalThumbnail);
				}
			} else {
				$movieOrTvShow->setThumbnail($originalThumbnail);
			}

			if ($form->get('removeThumbnail') && $form->get('removeThumbnail')->getData() === TRUE) {
				$movieOrTvShow->setThumbnail('thumbnail.jpeg');

				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $originalThumbnail);
				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $originalThumbnail);
				$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $originalThumbnail);
			}

			if ($movieOrTvShow->getLength() === 0) {
				$movieOrTvShow->setLength(NULL);
			}

			if ($originalTitle != $movieOrTvShow->getTitle()) {
				$slug = $this->slugger->createSlug($movieOrTvShow->getTitle());

				if ($this->movieAndTvShowRepository->findBy(['slug' => $slug])) {
					$slug = $slug . '-' . $movieOrTvShow->getPremiere()->format('Y');
				}

				$movieOrTvShow->setSlug($slug);
			}

			$entityManager = $this->getDoctrine()->getManager();

			foreach ($movieOrTvShow->getCastMembers() as $castMember) {
				$castMember->setPremiere($movieOrTvShow->getPremiere());
				$entityManager->persist($castMember);
				$entityManager->flush();
			}

			try {
				$entityManager->persist($movieOrTvShow);
				$entityManager->flush();

				$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'The movie ' . $movieOrTvShow->getTitle() . ' was successfully edited');
				return $this->redirectToReferer($request);
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		$similarMovies = $this->movieAndTvShowRepository->findSimilar($movieOrTvShow);

		$missingInfo = $this->movieAndTvShowBaseController->getMissingInfo($movieOrTvShow);

		return $this->render('/admin/movies_and_tv_shows/movies/edit.html.twig', [
			'form' => $form->createView(),
			'movieOrTvShow' => $movieOrTvShow,
			'castMembers' => $movieOrTvShow->getCastMembers(),
			'similar' => $similarMovies,
			'missingInfo' => $missingInfo
		]);
	}

	/**
	 * @Route("/movie/recommend/{id}/{anchor}", name="recommend_movie", schemes={"http"})
	 */
	public function recommend($id, Request $request, $anchor = NULL)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		return $this->movieAndTvShowBaseController->recommendMovie(
			$this->movieAndTvShowRepository->find($id), $request, $anchor
		);
	}

	/**
	 * @Route("/movie/unrecommend/{id}/{anchor}", name="unrecommend_movie", schemes={"http"})
	 */
	public function unrecommend($id, Request $request, $anchor = NULL)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		return $this->movieAndTvShowBaseController->unrecommendMovie(
			$this->movieAndTvShowRepository->find($id), $request, $anchor
		);
	}

	/**
	 * @Route("/admin/movie/{id}/new-cast-member", name="assign_movie_cast_member", schemes={"http"})
	 */
	public function assignCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$movie = $this->movieAndTvShowRepository->find($id);

		$castMember = new CastMember();
		$form = $this->createForm(CastMemberType::class, $castMember);

		if ($this->movieAndTvShowBaseController->assignCastMember($form, $request, $movie, $castMember)) {
			return $this->redirectToRoute('edit_movie', [
				'id' => $id,
				'_fragment' => 'tab-cast'
			]);
		}

		return $this->render('/admin/movies_and_tv_shows/movies/cast.html.twig', [
			'form' => $form->createView(),
			'movie' => $movie
		]);
	}

	/**
	 * @Route("/admin/movie/cast-member/delete/{id}", schemes={"http"})
	 */
	public function removeCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->castMemberRepository->find($id);
		$movie = $this->movieAndTvShowRepository->find($castMember->getMovieOrTvShowId());

		if ($this->movieAndTvShowBaseController->removeCastMember($castMember, $request)) {
			$this->movieAndTvShowBaseController->flashbag->add('primary', 'The cast member was successfully removed from this movie');
		} else {
			$this->movieAndTvShowBaseController->flashbag->add('danger', 'Something went wrong');
		}

		return $this->redirectToRoute('edit_movie', [
			'id' => $movie->getId(),
			'_fragment' => 'tab-cast'
		]);
	}

	/**
	 * @Route("/admin/movie/cast-member/edit/{id}", name="edit_movie_cast_member", schemes={"http"})
	 */
	public function editCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->castMemberRepository->find($id);
		$movie = $this->movieAndTvShowRepository->find($castMember->getMovieOrTvShowId());

		$form = $this->createForm(CastMemberType::class, $castMember, [
			'edit' => TRUE
		]);

		if ($this->movieAndTvShowBaseController->editCastMember($form, $request, $castMember)) {
			return $this->redirectToRoute('edit_movie', [
				'id' => $movie->getId(),
				'_fragment' => 'tab-cast'
			]);
		}

		return $this->render('/admin/movies_and_tv_shows/movies/cast_edit.html.twig', [
			'form' => $form->createView(),
			'_fragment' => 'tab-cast'
		]);
	}

	/**
	 * @Route("/in-cinemas", name="in_cinemas", schemes={"http"})
	 */
	public function inCinemas()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$newReleases = $this->movieAndTvShowRepository->findNewReleases();

		return $this->render('/movies/in_cinemas.html.twig', [
			'newReleases' => $newReleases,
			'count' => count($newReleases)
		]);
	}

	/**
	 * @Route("/movies/leaderboard", name="movie_leaderboard", schemes={"http"})
	 */
	public function leaderboard()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$bestRatedMovies = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findBy(
			[
				'type' => 'movie'
			],
			[
				'rating' => 'DESC'
			],
			100
		);

		return $this->render('/movies/leaderboard.html.twig', [
			'bestRatedMovies' => $bestRatedMovies,
			'count' => count($bestRatedMovies)
		]);
	}

	/**
	 * @Route("/movie/{slug}/load-more", schemes={"http"})
	 */
	public function loadMore($slug, Request $request)
	{
		$data = $request->query->get('count');
		$hr = $request->query->get('highlightedReview');
		$user = $this->userRepository->findOneBy(['username' => $hr]);

		$movie = $this->movieAndTvShowRepository->findOneBy(['slug' => $slug, 'type' => 'movie']);
		$reviews = $movie->getUserReviews();

		if ($user != NULL) {
			$highlightedReview = $this->userReviewRepository->findOneBy([
				'user' => $user,
				'movieOrTvShow' => $this->movieAndTvShowRepository->findOneBy(['slug' => $slug, 'type' => 'movie'])
			]);

			if ($highlightedReview != NULL) {
				$reviews->removeElement($highlightedReview);
			}
		}

		$reviews = $reviews->slice($data, 5);
		$return = [];
		$last = FALSE;

		if (empty($movie->getUserReviews()->slice($data + 5, 6))) {
			$last = TRUE;
		}

		foreach ($reviews as $review) {
			$return[] = [
				'body' => $review->getBody(),
				'photo' => $review->getUser()->getProfilePicture(),
				'rating' => $review->getRating(),
				'date' => $review->getDateCreated()->format('d. m. Y'),
				'username' => $review->getUser()->getUsername()
			];
		}

		return new JsonResponse([
			$return,
			$last
		]);
	}
}