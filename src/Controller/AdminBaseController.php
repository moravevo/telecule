<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class AdminBaseController extends AbstractController
{
	private $allowed = [2, 3, 5];
	private $message = 'You are not allowed to view this page';


	/**
	 * @var Session
	 */
	public $session;

	/**
	 * @var User
	 */
	public $user;

	/**
	 * @var Filesystem
	 */
	public $filesystem;

	public function redirectToReferer(Request $request, $anchor = NULL)
	{
		$anchor = $anchor != NULL ? '#' . $anchor : $anchor;

		return $this->redirect($request->headers->get('referer') . $anchor);
	}

	public function getAccessUtilities()
	{
		$this->session = new Session();
		$this->user = $this->session->get('user');
		$this->filesystem = new Filesystem();

		return $this;
	}

	public function isLoggedIn()
	{
		if ($this->getAccessUtilities()->user === NULL) {
			return FALSE;
		}

		return TRUE;
	}

	public function isSuperadmin()
	{
		if ($this->getAccessUtilities()->user->getRoleId() != 3) {
			return FALSE;
		}

		return TRUE;
	}

	public function isAdmin()
	{
		if ($this->getAccessUtilities()->user->getRoleId() != 2) {
			return FALSE;
		}

		return TRUE;
	}

	public function isModerator()
	{
		if ($this->getAccessUtilities()->user->getRoleId() != 5) {
			return FALSE;
		}

		return TRUE;
	}

	public function isAtLeastModerator()
	{
		if (!in_array($this->getAccessUtilities()->user->getRoleId(), [3, 2, 5])) {
			return FALSE;
		}

		return TRUE;
	}

	public function isAtLeastAdmin()
	{
		if (!in_array($this->getAccessUtilities()->user->getRoleId(), [3, 2])) {
			return FALSE;
		}

		return TRUE;
	}

	public function isAtLeastSuperadmin()
	{
		if (!in_array($this->getAccessUtilities()->user->getRoleId(), [3])) {
			return FALSE;
		}

		return TRUE;
	}
}