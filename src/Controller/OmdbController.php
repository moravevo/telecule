<?php

namespace App\Controller;

use App\Entity\CastMember;
use App\Repository\CastRoleRepository;
use App\Entity\Celebrity;
use App\Entity\Genre;
use App\Entity\MovieAndTvShow;
use App\Form\MovieAndTvShowType;
use App\Service\FileUploader;
use Jleagle\Imdb\Imdb;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\BaseController\MovieAndTvShowBaseController;
use Gumlet\ImageResize;

class OmdbController extends AdminBaseController
{
	private $movieAndTvShowBaseController;
	private $castRoleRepository;
	public $filesystem;
	public $session;

	public function __construct(
		MovieAndTvShowBaseController $movieAndTvShowBaseController,
		CastRoleRepository $castRoleRepository
	)
	{
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
		$this->castRoleRepository = $castRoleRepository;
		$this->filesystem = new Filesystem();
		$this->session = new Session();
	}

	/**
	 * @Route("/admin/omdb", name="omdb", schemes={"http"})
	 */
	public function index(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$form = $this->createFormBuilder()
			->add('title', TextType::class)
			->add('submit', SubmitType::class, [
				'attr' => [
					'class' => 'btn btn-primary'
				]
			])
			->getForm();

		$form_ = $form->createView();

		$form->handleRequest($request);

		$search = NULL;

		if ($form->isSubmitted()) {
			Imdb::setApiKey('30982c48');

			try {
				$search = Imdb::search($form->get('title')->getData());
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}

			$searchHelpers = [];

			foreach ($search as $result) {
				$searchHelpers[] = [
					'title' => $result->toArray()['title'],
					'year' => $result->toArray()['year'],
					'imdbId' => $result->toArray()['imdbId']
				];
			}

			$search = $searchHelpers;
		}

		return $this->render('omdb/index.html.twig', [
			'form' => $form_,
			'search' => $search
		]);
	}

	/**
	 * @Route("/omdb/add/{imdbId}", name="omdb_add", schemes={"http"})
	 */
	public function add($imdbId, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		Imdb::setApiKey('30982c48');
		$movieOrTvShow = new MovieAndTvShow();

		try {
			$oneResult = Imdb::retrieve($imdbId)->toArray();
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToRoute('omdb');
		}

		$thumbnailUrl = $oneResult['poster'];

		$title = $oneResult['title'];
		$premiere = NULL;
		$length = str_replace(' min', '', $oneResult['runtime']);
		$rating = $oneResult['imdbRating'];
		$type = $oneResult['type'];
		$shortDescription = $oneResult['plot'];
		$description = $oneResult['plot'];
		$genres = array_slice(explode(', ', $oneResult['genre']), 0, 3);
		$celebrities = explode(', ', $oneResult['actors']);
		$withoutRating = FALSE;

		if ($thumbnailUrl != 'N/A') {
			$thumbnail = md5(uniqid()) . '.jpeg';
		} else {
			$thumbnail = 'thumbnail.jpeg';
		}

		$existingGenresRaw = $this->movieAndTvShowBaseController->genreRepository->findNames();
		$existingGenres = [];

		foreach ($existingGenresRaw as $existingGenreRaw) {
			$existingGenres[] = $existingGenreRaw['name'];
		}

		if ($genres != 'N/A') {
			foreach ($genres as $genre) {
				if (!in_array($genre, $existingGenres)) {
					$newGenre = new Genre();
					$newGenre->setName($genre);
					$this->movieAndTvShowBaseController->entityManager->persist($newGenre);
					$this->movieAndTvShowBaseController->entityManager->flush();
				}
			}
		}

		$existingCelebritiesRaw = $this->movieAndTvShowBaseController->celebrityRepository->findFullNames();
		$existingCelebrities = [];

		foreach ($existingCelebritiesRaw as $existingCelebrityRaw) {
			$existingCelebrities[] = $existingCelebrityRaw['fullName'];
		}

		if ($celebrities != 'N/A') {
			foreach ($celebrities as $celebrity) {
				if (!in_array($celebrity, $existingCelebrities)) {
					$newCelebrity = new Celebrity();
					$newCelebrity->setFullName($celebrity);
					$newCelebrity->setSlug($this->movieAndTvShowBaseController->slugger->createSlug($celebrity));
					$newCelebrity->setPicture('thumbnail.jpeg');
					$this->movieAndTvShowBaseController->entityManager->persist($newCelebrity);
					$this->movieAndTvShowBaseController->entityManager->flush();
				}
			}
		}

		if ($type === 'series') {
			$type = 'tv_show';
			$length = NULL;
		}

		if ($rating === 'N/A') {
			$rating = NULL;
			$withoutRating = TRUE;
		}

		if ($description === 'N/A') {
			$description = NULL;
			$shortDescription = $description;
		}

		if ($length === 'N/A') {
			$length = NULL;
		}

		if ($oneResult['released'] != 'N/A') {
			$premiere = new \DateTime($oneResult['released']);
		}

		$movieOrTvShow->setTitle($title)
			->setPremiere($premiere)
			->setLength($length)
			->setRating($rating)
			->setShortDescription($shortDescription)
			->setType($type)
			->setThumbnail($thumbnail)
			->setDescription($description);

		if ($genres != 'N/A') {
			foreach ($genres as $genre) {
				$movieOrTvShow->addGenre($this->movieAndTvShowBaseController->genreRepository->findOneBy([
					'name' => $genre
				]));
			}
		}

		$form = $this->createForm(MovieAndTvShowType::class, $movieOrTvShow, [
			'type' => $type,
			'withoutRating' => $withoutRating
		]);

		if ($this->movieAndTvShowBaseController->addMovieOrTvShow($form, $request, $movieOrTvShow, $fileUploader)) {
			if ($celebrities != 'N/A') {
				foreach ($celebrities as $celebrity) {
					$castMember = new CastMember();
					$castMember->setCelebrity($this->movieAndTvShowBaseController->celebrityRepository->findOneBy([
						'fullName' => $celebrity
					]));
					$castMember->setMovieOrTvShow($movieOrTvShow);
					$castMember->setPremiere($movieOrTvShow->getPremiere());
					$castMember->addCastRole($this->castRoleRepository->findOneBy([
						'name' => 'Actor'
					]));
					
					$this->movieAndTvShowBaseController->entityManager->persist($castMember);
					$this->movieAndTvShowBaseController->entityManager->flush();
				}
			}

			if ($thumbnailUrl != 'N/A') {
				if (copy(str_replace('SX300', 'SX1200', $thumbnailUrl), $this->getParameter($type . '_thumbnails_directory') . $thumbnail)) {
					$thumbWihtoutExtension = str_replace('.jpeg', '', $thumbnail);

					$image = new ImageResize($this->getParameter($type . '_thumbnails_directory') . $thumbnail);
					$image->save($this->getParameter($type . '_thumbnails_directory') . $thumbWihtoutExtension . '.jpeg');

					$image = new ImageResize($this->getParameter($type . '_thumbnails_directory') . $thumbnail);
					$image->crop(160, 225);
					$image->save($this->getParameter($type . '_thumbnails_directory') . 'small/' . $thumbWihtoutExtension . '.jpeg');

					$image2 = new ImageResize($this->getParameter($type . '_thumbnails_directory') . $thumbnail);
					$image2->crop(736, 1035);
					$image2->save($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $thumbWihtoutExtension . '.jpeg');

					$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $thumbnail);

					$movieOrTvShow->setThumbnail($thumbWihtoutExtension . '.jpeg');

					try {
						$this->movieAndTvShowBaseController->entityManager->persist($movieOrTvShow);
						$this->movieAndTvShowBaseController->entityManager->flush();
					} catch (\Exception $e) {
						$this->session->getFlashBag()->add('danger', $e->getMessage());
						return $this->redirectToReferer($request);
					}
				} else {
					$this->session->getFlashBag()->add('warning', 'Something went wrong while uploading the picture, everything else is alright');
				}
			}

			return $this->redirectToRoute('omdb');
		}

		return $this->render('/omdb/add.html.twig', [
			'form' => $form->createView()
		]);
	}
}
