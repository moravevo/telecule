<?php

namespace App\Controller;

use App\Entity\CastMember;
use App\Entity\Celebrity;
use App\Entity\MovieAndTvShow;
use App\Repository\CastMemberRepository;
use App\Service\Paginator;
use Gumlet\ImageResize;
use App\Form\CelebritySearchType;
use App\Form\CelebrityType;
use App\Repository\CelebrityRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CastMemberType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Slugger;
use Symfony\Component\Form\FormError;


class CelebrityController extends AdminBaseController
{
	public $entityManager;
	public $filesystem;
	private $celebrityRepository;
	private $castMemberRepository;
	public $session;
	private $slugger;
	public $user;

	public function __construct(
		EntityManagerInterface $entityManagerInterface,
		CelebrityRepository $celebrityRepository,
		CastMemberRepository $castMemberRepository
	)
	{
		$this->entityManager = $entityManagerInterface;
		$this->filesystem = new Filesystem();
		$this->celebrityRepository = $celebrityRepository;
		$this->castMemberRepository = $castMemberRepository;
		$this->session = new Session();
		$this->slugger = new Slugger();
		$this->user = $this->session->get('user');
	}

	/**
	 * @Route("/celebrities", name="celebrities", schemes={"http"})
	 */
	public function index(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$form = $this->createForm(CelebritySearchType::class);

		$form->handleRequest($request);

		$result = NULL;

		if ($form->isSubmitted() && $form->isValid()) {
			if (!$form->get('advancedSearch')->getData() && $form->get('fullName')->getData() === NULL) {
				$form->get('fullName')->addError(new FormError('This field is required'));
			} else {
				if (!$form->get('advancedSearch')->getData()) {
					$lowestBirthday = new \DateTime($this->celebrityRepository->findMinBirthday());
					$highestBirthday = new \DateTime($this->celebrityRepository->findMaxBirthday());
					$includeWithoutBirthday = TRUE;
				} else {
					$lowestBirthday = date_create_from_format('Y', $form->get('lowestBirthday')->getData());
					$highestBirthday = date_create_from_format('Y', $form->get('highestBirthday')->getData());
					$includeWithoutBirthday = $form->get('includeWithoutBirthday')->getData();
				}

				$results = $this->celebrityRepository->search(
					$form->get('fullName')->getData(),
					$lowestBirthday,
					$highestBirthday,
					$includeWithoutBirthday
				);

				foreach ($results as $celebrity) {
					$castRoles = [];

					foreach ($this->celebrityRepository->findCastRoles($celebrity->getId()) as $castRole) {
						$castRoles[] = $castRole['name'];
					}

					$celebrity->castRoles = $castRoles;
				}

				return $this->render('/celebrities/search_results.html.twig', [
					'form' => $form->createView(),
					'results' => $results,
					'searchedString' => $form->get('fullName')->getData(),
					'returnedResults' => count($results)
				]);
			}
		}

		$recentlyAdded = $this->celebrityRepository->findBy(
			[],
			[
				'dateCreated' => 'DESC'
			],
			25
		);

		$mostVisited = $this->celebrityRepository->findBy(
			[],
			[
				'visits' => 'DESC'
			],
			10
		);

		foreach ($mostVisited as $celebrity) {
			$castRoles = [];

			foreach ($this->celebrityRepository->findCastRoles($celebrity->getId()) as $castRole) {
				$castRoles[] = $castRole['name'];
			}

			$celebrity->castRoles = $castRoles;
		}

		return $this->render('/celebrities/index.html.twig', [
			'form' => $form->createView(),
			'recentlyAdded' => $recentlyAdded,
			'mostVisited' => $mostVisited,
			'result' => $result
		]);
	}

	/**
	 * @Route("/admin/celebrities", name="admin_celebrities", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$celebrityRepository = $this->celebrityRepository;

		$celebrities = $celebrityRepository->findAll();

		$paginated = $paginator->paginate($celebrities, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/celebrities');

		return $this->render('/admin/celebrities/celebrities/index.html.twig', [
			'celebrities' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($celebrities)
		]);
	}

	/**
	 * @Route("/admin/celebrity/add", name="add_celebrity", schemes={"http"})
	 */
	public function add(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$celebrity = new Celebrity();

		$form = $this->createForm(CelebrityType::class, $celebrity);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$celebrity = $form->getData();
			$slug = $this->slugger->createSlug($celebrity->getFullName());

			$celebrity->setSlug($slug);

			$fileName = 'thumbnail.jpeg';
			$file = $form->get('picture')->getData();

			if ($file != NULL) {
				$fileName = $fileUploader->upload($file, $this->getParameter('celebrity_pictures_directory'), 200, 200, 500, 500);

				if ($fileName === FALSE) {
					$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

					return $this->redirectToReferer($request);
				}
			}

			$celebrity->setPicture($fileName);

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($celebrity);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'The celebrity was successfully added to the database');

				return $this->redirectToRoute('admin_celebrities');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}


		}

		return $this->render('/admin/celebrities/celebrities/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/celebrities/delete/{id}", name="delete_celebrity", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$celebrity = $this->celebrityRepository->find($id);
		$flashbag = $this->session->getFlashBag();

		if ($celebrity->getPicture() != 'thumbnail.jpeg') {
			$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . $celebrity->getPicture());
			$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'small/' . $celebrity->getPicture());
		}

		try {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($celebrity);
			$entityManager->flush();

			$flashbag->add('primary', 'Celebrity was successfully deleted');
			return $this->redirectToRoute('admin_celebrities');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/celebrities/edit/{id}", name="edit_celebrity", schemes={"http"})
	 */
	public function edit($id, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$celebrity = $this->celebrityRepository->find($id);
		$originalPicture = $celebrity->getPicture();
		$missingInfo = $this->getMissingInfo($celebrity);
		$removePicture = FALSE;

		if ($originalPicture != NULL && $originalPicture != 'thumbnail.jpeg') {
			$removePicture = TRUE;
		}

		$form = $this->createForm(CelebrityType::class, $celebrity, [
			'edit' => TRUE,
			'removePicture' => $removePicture
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			if ($celebrity->getPicture() != NULL) {
				$fileName = 'thumbnail.jpeg';
				$file = $form->get('picture')->getData();

				if ($file != NULL) {
					$fileName = $fileUploader->upload($file, $this->getParameter('celebrity_pictures_directory'), 200, 200, 500, 500);


					if ($fileName === FALSE) {
						$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

						return $this->redirectToReferer($request);
					}
				}

				$celebrity->setPicture($fileName);

				if ($celebrity->getPicture() != 'thumbnail.jpeg' && $originalPicture != 'thumbnail.jpeg') {
					$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . $originalPicture);
					$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'small/' . $originalPicture);
					$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'normal/' . $originalPicture);
				}
			} else {
				$celebrity->setPicture($originalPicture);
			}

			if ($form->get('removePicture')->getData() === TRUE) {
				$celebrity->setPicture('thumbnail.jpeg');

				$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . $originalPicture);
				$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'small/' . $originalPicture);
				$this->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'normal/' . $originalPicture);
			}

			$slug = $this->slugger->createSlug($celebrity->getFullName());

			$celebrity->setSlug($slug);

			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($celebrity);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'The celebrity ' . $celebrity->getFullName() . ' was successfully edited');
				return $this->redirectToRoute($request->get('_route'), array('id' => $celebrity->getId()));
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/celebrities/celebrities/edit.html.twig', [
			'form' => $form->createView(),
			'celebrity' => $celebrity,
			'missingInfo' => $missingInfo
		]);
	}

	/**
	 * @Route("/admin/celebrities/search", schemes={"http"})
	 */
	public function search(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$data = $request->getContent();
		$responses = array_map(function ($response) {
			return array(
				'id' => $response['id'],
				'displayValue' => $response['fullName'],
				'targetUrl' => 'celebrity/' . $response['slug']
			);
		}, $this->celebrityRepository->findByName($data));

		return new JsonResponse([
			$responses
		]);
	}

	/**
	 * @param Celebrity $celebrity
	 * @return array $missing
	 */
	public function getMissingInfo($celebrity)
	{
		$missing = [];

		if ($celebrity->getBirthday() === NULL) {
			$missing[] = 'No set birthday';
		}

		if ($celebrity->getBio() === NULL) {
			$missing[] = 'No bio';
		}

		if ($celebrity->getPicture() === 'thumbnail.jpeg') {
			$missing[] = 'Default picture';
		}

		if ($celebrity->getMoviesAndTvShows()[0] === NULL) {
			$missing[] = 'Not assigned to any movie or TV Show';
		}

		return $missing;
	}

	function sortFunction($a, $b)
	{
		return $a["premiere"] < $b["premiere"];
	}

	/**
	 * @Route("/celebrity/{slug}", name="celebrity", schemes={"http"})
	 */
	public function profile($slug, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$celebrity = $this->celebrityRepository->findOneBy([
			'slug' => $slug
		]);

		if ($celebrity != NULL) {
			$celebrity->setVisits($celebrity->getVisits() + 1);
			$this->entityManager->persist($celebrity);
			$this->entityManager->flush();
		}

		return $this->render('/celebrities/profile.html.twig', [
			'celebrity' => $celebrity,
			'requestedCelebrity' => $slug
		]);
	}

	/**
	 * @Route("/born-this-month", name="born_this_month", schemes={"http"})
	 */
	public function bornThisMonth()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$bornThisMonth = $this->celebrityRepository->findBornThisMonth();

		foreach ($bornThisMonth as $celebrity) {
			$castRoles = [];

			foreach ($this->celebrityRepository->findCastRoles($celebrity->getId()) as $castRole) {
				$castRoles[] = $castRole['name'];
			}

			$celebrity->castRoles = $castRoles;
		}

		return $this->render('/celebrities/born_this_month.html.twig', [
			'bornThisMonth' => $bornThisMonth,
			'count' => count($bornThisMonth)
		]);
	}

	/**
	 * @Route("/celebrity/{slug}/load-more", schemes={"http"})
	 */
	public function loadMore($slug, Request $request)
	{
		$data = $request->query->get('count');

		$celebrity = $this->celebrityRepository->findOneBy(['slug' => $slug]);
		$appearances = [];
		$count = count($celebrity->getMoviesAndTvShows()->toArray());
		$last = FALSE;

		if (empty(array_slice($celebrity->getMoviesAndTvShows()->toArray(), $data + 5, 6))) {
			$last = TRUE;
		}

		foreach (array_slice($celebrity->getMoviesAndTvShows()->toArray(), $data, 5) as $show) {
			$genres = [];
			foreach ($show->getMovieOrTvShow()->getGenres() as $genre) {
				$genres[] = $genre->getName();
			}

			$type = $show->getMovieOrTvShow()->getType();

			if ($type === 'tv_show') {
				$type = 'tv-show';
				$imgUrl = 'tvshows';
			} else {
				$imgUrl = 'movies';
			}

			$premiere = $show->getMovieOrTvShow()->getPremiere();

			if ($premiere === NULL) {
				$premiere = 'No premiere';
			} else {
				$premiere = $premiere->format('d. m. Y');
			}

			$appearances[] = [
				'title' => $show->getMovieOrTvShow()->getTitle(),
				'thumbnail' => $show->getMovieOrTvShow()->getThumbnail(),
				'imgUrl' => $imgUrl,
				'premiere' => $premiere,
				'genres' => $genres,
				'slug' => $show->getMovieOrTvShow()->getSlug(),
				'type' => $type
			];
		}

		return new JsonResponse([
			$appearances,
			$last
		]);
	}

	/**
	 * @Route("/admin/celebrities/{id}/assign-to-movie", schemes={"http"})
	 */
	public function assingToMovie($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$celebrity = $this->celebrityRepository->findOneBy(['id' => $id]);

		$castMember = new CastMember();

		$form = $this->createForm(CastMemberType::class, $castMember, [
			'edit' => TRUE,
			'only_movie' => TRUE
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$castMember->setCelebrity($celebrity);
			$castMember->setMovieOrTvShow($form->get('movieOrTvShow')->getData());
			$castMember->setPremiere($form->get('movieOrTvShow')->getData()->getPremiere());

			$exists = $this->castMemberRepository->findOneBy([
				'movieOrTvShowId' => $form->get('movieOrTvShow')->getData()->getId(),
				'celebrityId' => $celebrity->getId()
			]);

			if (!$exists) {
				try {
					$this->entityManager->persist($castMember);
					$this->entityManager->flush();
					$this->session->getFlashBag()->add('primary', 'The cast member was successfully added');
					return $this->redirectToRoute('edit_celebrity', [
						'id' => $celebrity->getId()
					]);
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}

			} else {
				$form->get('movieOrTvShow')->addError(new FormError('This person is already a cast member for this movie'));
			}
		}

		return $this->render('/admin/celebrities/celebrities/assign.html.twig', [
			'form' => $form->createView(),
			'celebrity' => $celebrity
		]);
	}

	/**
	 * @Route("/admin/cast-member/{id}/remove", schemes={"http"})
	 */
	public function deleteCastMember($id, Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->castMemberRepository->find($id);

		try {
			$this->entityManager->remove($castMember);
			$this->entityManager->flush();
			$this->session->getFlashBag()->add('primary', 'The celebrity was successfully removed from the movie');
			return $this->redirectToReferer($request);
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}

	}

	/**
	 * @Route("/admin/cast-member/{id}/edit", schemes={"http"})
	 */
	public function editCastMember($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castMember = $this->castMemberRepository->find($id);

		$form = $this->createForm(CastMemberType::class, $castMember, [
			'edit' => TRUE
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->entityManager->persist($castMember);
				$this->entityManager->flush();
				$this->session->getFlashBag()->add('primary', 'The celebrity was successfully removed from the movie');
				return $this->redirectToRoute('edit_celebrity', [
					'id' => $castMember->getCelebrity()->getId()
				]);
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/celebrities/celebrities/change_role.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/remove-multiple/celebrities", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->celebrityRepository->find($id);

			if ($complaint->getPicture() != 'thumbnail.jpeg') {
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('celebrity_pictures_directory') . $complaint->getPicture());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'small/' . $complaint->getPicture());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter('celebrity_pictures_directory') . 'normal/' . $complaint->getPicture());
			}

			$this->entityManager->remove($complaint);
			$this->entityManager->flush();
		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}