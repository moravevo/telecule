<?php

namespace App\Controller;

use Intervention\Image\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Filesystem\Filesystem;

class BaseController extends AdminBaseController
{
	public $session;
	public $flashbag;
	public $filesystem;

	public function __construct(
	)
	{
		$this->filesystem = new Filesystem();
		$this->session = new Session();
		$this->flashbag = $this->session->getFlashBag();
	}

	/**
	 * @Route("/licenses")
	 */
	public function licenses()
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		return $this->render('licenses.html.twig');
	}
}