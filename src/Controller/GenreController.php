<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\User;
use App\Form\GenreType;
use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Service\Paginator;
use Symfony\Component\Routing\Annotation\Route;


class GenreController extends AdminBaseController
{
	/**
	 * @var User
	 */
	public $user;

	public $session;

	private $genreRepository;

	private $paginator;

	public function __construct(
		GenreRepository $genreRepository
	)
	{
		$this->genreRepository = $genreRepository;
		$this->session = new Session();
		$this->user = $this->session->get('user');
		$this->paginator = new Paginator();
	}

	/**
	 * @Route("/admin/genres", name="admin_genres", schemes={"http"})
	 */
	public function index(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$genres = $this->genreRepository->findAll();

		$paginated = $paginator->paginate($genres, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/genres');

		return $this->render('/admin/movies_and_tv_shows/genres/index.html.twig', [
			'genres' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($genres)
		]);
	}

	/**
	 * @Route("/admin/genre/add", name="add_genre", schemes={"http"})
	 */
	public function addGenre(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$genre = new Genre();

		$form = $this->createForm(GenreType::class, $genre);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($genre);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'Genre was successfully added');
				return $this->redirectToRoute('admin_genres');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/movies_and_tv_shows/genres/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/genres/delete/{id}", name="delete_genre", schemes={"http"})
	 */
	public function deleteGenre($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$genre = $this->genreRepository->find($id);
		$flashbag = $this->session->getFlashBag();

		try {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($genre);
			$entityManager->flush();

			$flashbag->add('primary', 'Genre was successfully deleted');
			return $this->redirectToRoute('admin_genres');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/genres/edit/{id}", name="edit_genre", schemes={"http"})
	 */
	public function editGenre($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$genre = $this->genreRepository->find($id);
		$flashbag = $this->session->getFlashBag();

		$form = $this->createForm(GenreType::class, $genre);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->merge($genre);
				$entityManager->flush();

				$flashbag->add('primary', 'Genre with id was successfully edited');
				return $this->redirectToRoute('admin_genres');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/movies_and_tv_shows/genres/edit.html.twig', [
			'form' => $form->createView(),
			'genre' => $genre
		]);
	}
}