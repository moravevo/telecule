<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserReview;
use App\Form\UserReviewType;
use App\Repository\UserReviewRepository;
use App\Service\Paginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class UserReviewController extends AdminBaseController
{
	public $user;
	private $userReviewRepository;
	public $session;
	private $entityManager;

	public function __construct(
		UserReviewRepository $userReviewRepository,
		EntityManagerInterface $entityManager
	)
	{
		$this->session = new Session();
		$this->user = $this->session->get('user');
		$this->userReviewRepository = $userReviewRepository;
		$this->entityManager = $entityManager;
	}

	/**
	 * @Route("/user_review/delete/{id}", name="delete_user_review", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$userReviewRepository = $this->getDoctrine()->getRepository(UserReview::class);
		$userReview = $userReviewRepository->find($id);
		$user = $this->user;
		$flashbag = $this->session->getFlashBag();

		if ($user != NULL && $userReview && $userReview->getUser()->getId() === $user->getId()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->remove($userReview);
				$entityManager->flush();
				$type = $userReview->getMovieOrTvShow()->getType();
				$typeReadable = 'Movie';

				if ($type === 'tv_show') {
					$typeReadable = 'TV Show';
				}

				$flashbag->add('primary', $typeReadable . ' review was successfully deleted');
				return $this->redirectToRoute($type . '_detail', array('slug' => $userReview->getMovieOrTvShow()->getSlug()));
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}

		}

		$flashbag->add('warning', 'You are not allowed to view this page');
		return $this->redirectToRoute('homepage');
	}

	/**
	 * @Route("/user_review/edit/{id}", name="edit_user_review", schemes={"http"})
	 */
	public function edit($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$userReviewRepository = $this->getDoctrine()->getRepository(UserReview::class);
		$userReview = $userReviewRepository->find($id);
		$user = $this->user;
		$flashbag = $this->session->getFlashBag();
		$form_ = NULL;

		if ($user != NULL && $userReview && $userReview->getUser()->getId() === $user->getId()) {
			$form = $this->createForm(UserReviewType::class, $userReview);

			$type = $userReview->getMovieOrTvShow()->getType();
			$typeReadable = 'movie';

			if ($type === 'tv_show') {
				$typeReadable = 'TV Show';
			}

			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				try {
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->persist($userReview);
					$entityManager->flush();

					$flashbag->add('primary', ucfirst($typeReadable) . ' review was successfully edited');
					return $this->redirectToRoute($type . '_detail', array('slug' => $userReview->getMovieOrTvShow()->getSlug()));
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			$form_ = $form->createView();

			return $this->render('/user_reviews/edit.html.twig', [
				'form' => $form_,
				'typeReadable' => $typeReadable,
				'movieOrTvShow' => $userReview->getMovieOrTvShow()
			]);
		}

		$flashbag->add('warning', 'You are not allowed to view this page');
		return $this->redirectToRoute('homepage');
	}

	/**
	 * @Route("/admin/reviews", name="admin_user_reviews", schemes={"http"})
	 */
	public function listAllUserReviews(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$userReviews = $this->userReviewRepository->findAll();

		$paginated = $paginator->paginate($userReviews, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/reviews');

		return $this->render('/admin/movies_and_tv_shows/reviews/reviews.html.twig', [
			'reviews' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($userReviews)
		]);
	}

	/**
	 * @Route("/admin/reviews/delete/{id}", name="admin_delete_user_review", schemes={"http"})
	 */
	public function adminDelete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$review = $this->userReviewRepository->find($id);

		try {
			$this->entityManager->remove($review);
			$this->entityManager->flush();

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'The review was successfully deleted');
			return $this->redirectToRoute('admin_user_reviews');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/reviews/edit/{id}", name="admin_edit_user_review", schemes={"http"})
	 */
	public function adminEdit($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastAdmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$review = $this->userReviewRepository->find($id);

		$form = $this->createForm(UserReviewType::class, $review, [
			'admin' => TRUE
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->entityManager->persist($review);
				$this->entityManager->flush();

				$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'The review was successfully edited');
				return $this->redirectToRoute('admin_user_reviews');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/movies_and_tv_shows/reviews/edit.html.twig', [
			'form' => $form->createView(),
			'review' => $review
		]);
	}

	/**
	 * @Route("/admin/remove-multiple/user-reviews", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->userReviewRepository->find($id);
			$this->entityManager->remove($complaint);
			$this->entityManager->flush();
		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}
