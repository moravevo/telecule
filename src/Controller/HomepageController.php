<?php

namespace App\Controller;

use App\Entity\Complaint;
use App\Repository\TrailerRepository;
use Gumlet\ImageResize;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Repository\HomepageRecommendationRepository;
use App\Repository\CelebrityRepository;
use Doctrine\ORM\EntityManagerInterface;
use function MongoDB\BSON\fromJSON;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\CelebrityController;
use App\Controller\BaseController\MovieAndTvShowBaseController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\MovieAndTvShowRepository;
use App\Repository\UserReviewRepository;


class HomepageController extends AdminBaseController
{
	private $celebrityController;
	private $movieAndTvShowBaseController;
	private $movieAndTvShowRepository;
	private $homepageRecommendationRepository;
	private $celebrityRepository;
	private $userReviewRepository;
	private $entityManager;
	private $trailerRepository;

	public function __construct(
		TrailerRepository $trailerRepository,
		CelebrityController $celebrityController,
		MovieAndTvShowBaseController $movieAndTvShowBaseController,
		MovieAndTvShowRepository $movieAndTvShowRepository,
		HomepageRecommendationRepository $homepageRecommendationRepository,
		CelebrityRepository $celebrityRepository,
		UserReviewRepository $userReviewRepository,
		EntityManagerInterface $entityManagerInterface
	)
	{
		$this->trailerRepository = $trailerRepository;
		$this->celebrityController = $celebrityController;
		$this->movieAndTvShowBaseController = $movieAndTvShowBaseController;
		$this->movieAndTvShowRepository = $movieAndTvShowRepository;
		$this->homepageRecommendationRepository = $homepageRecommendationRepository;
		$this->celebrityRepository = $celebrityRepository;
		$this->userReviewRepository = $userReviewRepository;
		$this->entityManager = $entityManagerInterface;
	}

	/**
	 * @Route("/", name="homepage", schemes={"http"})
	 */
	public function index(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$recommendedMovies = $this->homepageRecommendationRepository->findBy([], [
			'ordering' => 'ASC'
		]);
		$newReleases = array_slice($this->movieAndTvShowRepository->findNewReleases(), 0, 5);
		$bornThisMonth = array_slice($this->celebrityRepository->findBornThisMonth(), 0, 5);
		$popularMoviesAndTvShows = $this->movieAndTvShowRepository->findBy(
			[],
			[
				'visits' => 'DESC'
			],
			6
		);
		$newestUserReviews = $this->userReviewRepository->findBy(
			[],
			[
				'dateCreated' => 'DESC'
			],
			4
		);

		foreach ($bornThisMonth as $celebrity) {
			$castRoles = [];

			foreach ($this->celebrityRepository->findCastRoles($celebrity->getId()) as $castRole) {
				$castRoles[] = $castRole['name'];
			}

			$celebrity->castRoles = $castRoles;
		}

		$complaint = new Complaint();

		$form = $this->createFormBuilder($complaint)
			->add('message', TextareaType::class, [
				'label' => 'Describe your problem',
				'attr' => [
					'rows' => 4
				],
				'help' => 'This form is completely anonymous (even while logged in)'
			])
			->add('submit', SubmitType::class, [
				'attr' => [
					'class' => 'btn btn-primary'
				]
			])
			->setRequired(FALSE)
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->entityManager->persist($complaint);
				$this->entityManager->flush();

				$this->movieAndTvShowBaseController->flashbag->add('primary', 'Your message was successfully sent');
				return $this->redirectToRoute('homepage');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		$trailers = $this->trailerRepository->findBy(
			[
				'active' => TRUE
			],
			[
				'ordering' => 'ASC'
			]
		);

		$maxVisits = $this->movieAndTvShowBaseController->movieAndTvShowRepository->findMaxVisits();

		return $this->render('/homepage/index.html.twig', [
			'recommendedMovies' => $recommendedMovies,
			'newReleases' => $newReleases,
			'bornThisMonth' => $bornThisMonth,
			'popularMoviesAndTvShows' => $popularMoviesAndTvShows,
			'newestUserReviews' => $newestUserReviews,
			'form' => $form->createView(),
			'trailers' => $trailers,
			'maxVisits' => $maxVisits
		]);
	}

	/**
	 * @Route("/quick-search", schemes={"http"})
	 */
	public function quickSearch(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		$celebrities = $this->celebrityController->search($request);
		$movies = $this->movieAndTvShowBaseController->searchMovies($request);
		$tvShows = $this->movieAndTvShowBaseController->searchTvShows($request);

		$responses = [
			'type' => 'quick-search',
			'celebrities' => $celebrities->getContent(),
			'movies' => $movies->getContent(),
			'tvShows' => $tvShows->getContent()
		];

		return new JsonResponse([
			$responses
		]);
	}

	/**
	 * @Route("/test")
	 */
	public function test(\Swift_Mailer $mailer)
	{
		$message = (new \Swift_Message('Forgotten password'))
			->setFrom('support@telecule.com')
			->setTo('vojtech.moravek@email.cz')
			->setBody(
				$this->renderView('email/test.html.twig'),
				'text/html'
			);

		$mailer->send($message);
	}
}