<?php

namespace App\Controller;

use App\Entity\CastRole;
use App\Form\CastRoleType;
use App\Repository\CastRoleRepository;
use App\Service\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class CastRoleController extends AdminBaseController
{
	private $allowed = [2, 3, 5];
	private $castRoleRepository;

	public function __construct(
		CastRoleRepository $castRoleRepository
	)
	{
		$this->castRoleRepository = $castRoleRepository;
	}

	/**
	 * @Route("/admin/cast-roles", name="admin_cast_roles", schemes={"http"})
	 */
	public function index(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}


		$castRoles = $this->castRoleRepository->findAll();

		$paginated = $paginator->paginate($castRoles, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/cast-roles');

		return $this->render('/admin/celebrities/cast_roles/index.html.twig', [
			'castRoles' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($castRoles)
		]);
	}

	/**
	 * @Route("/admin/cast-role/add", name="add_cast_role", schemes={"http"})
	 */
	public function addCastRole(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castRole = new CastRole();

		$form = $this->createForm(CastRoleType::class, $castRole);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($castRole);
				$entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'Cast role was successfully added');
				return $this->redirectToRoute('admin_cast_roles');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/celebrities/cast_roles/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/cast-roles/delete/{id}", name="delete_cast_role", schemes={"http"})
	 */
	public function deleteCastRole($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castRole = $this->castRoleRepository->find($id);

		try {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($castRole);
			$entityManager->flush();

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'Cast role was successfully deleted');
			return $this->redirectToRoute('admin_cast_roles');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/cast-roles/edit/{id}", name="edit_cast_role", schemes={"http"})
	 */
	public function editCastRole($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$castRole = $this->castRoleRepository->find($id);

		$form = $this->createForm(CastRoleType::class, $castRole);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->merge($castRole);
				$entityManager->flush();

				$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'Cast role with id was successfully edited');
				return $this->redirectToRoute('admin_cast_roles');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/celebrities/cast_roles/edit.html.twig', [
			'form' => $form->createView(),
			'castRole' => $castRole
		]);
	}
}