<?php

namespace App\Controller;

use App\Entity\Trailer;
use App\Form\TrailerType;
use App\Service\FileUploader;
use App\Service\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\TrailerRepository;
use Symfony\Component\Routing\Annotation\Route;

class TrailerController extends AdminBaseController
{
	private $entityManager;
	private $trailerRepository;
	private $baseController;

	public function __construct(
		EntityManagerInterface $entityManagerInterface,
		TrailerRepository $trailerRepository,
		BaseController $baseController
	)
	{
		$this->entityManager = $entityManagerInterface;
		$this->trailerRepository = $trailerRepository;
		$this->baseController = $baseController;
	}

	/**
	 * @Route("/admin/trailers", name="admin_trailers", schemes={"http"})
	 */
	public function index(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailers = $this->trailerRepository->findAll();

		$paginated = $paginator->paginate($trailers, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/trailers');

		return $this->render('admin/homepage/trailers/index.html.twig', [
			'trailers' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($trailers)
		]);
	}

	/**
	 * @Route("/admin/trailer/add", name="add_trailer", schemes={"http"})
	 */
	public function add(Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailer = new Trailer();

		$form = $this->createForm(TrailerType::class, $trailer);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$file = $form->get('thumbnail')->getData();

			$fileName = $fileUploader->upload($file, $this->getParameter('trailer_thumbnails_directory'), NULL, NULL, 1285, 476);

			if ($fileName === FALSE) {
				$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

				return $this->redirectToReferer($request);
			}

			$trailer->setThumbnail($fileName);

			$trailer->setOrdering($this->trailerRepository->findHighestOrdering() + 1);

			try {
				$this->entityManager->persist($trailer);
				$this->entityManager->flush();
				$this->baseController->flashbag->add('primary', 'The trailer ' . $trailer->getTitle() . ' was successfully added');
				return $this->redirectToRoute('admin_trailers');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('admin/homepage/trailers/add.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/trailers/delete/{id}", name="delete_trailer", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailer = $this->trailerRepository->find($id);

		try {
			$this->entityManager->remove($trailer);
			$this->entityManager->flush();
			$this->baseController->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . $trailer->getThumbnail());
			$this->baseController->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . 'normal/' . $trailer->getThumbnail());

			$this->baseController->flashbag->add('primary', 'The trailer was successfully deleted');
			return $this->redirectToRoute('admin_trailers');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/trailers/edit/{id}", name="edit_trailer", schemes={"http"})
	 */
	public function edit($id, Request $request, FileUploader $fileUploader)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailer = $this->trailerRepository->find($id);

		$originalThumbnail = $trailer->getThumbnail();

		$form = $this->createForm(TrailerType::class, $trailer, [
			'edit' => TRUE,
			'validation_groups' => 'edit'
		]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			if ($trailer->getThumbnail() === 'NULL') {
				$trailer->setThumbnail($originalThumbnail);
			} else {
				$this->baseController->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . $originalThumbnail);
				$this->baseController->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . 'normal/' . $originalThumbnail);
				$file = $form->get('thumbnail')->getData();

				$fileName = $fileUploader->upload($file, $this->getParameter('trailer_thumbnails_directory'), NULL, NULL, 1285, 476);

				if ($fileName === FALSE) {
					$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'The file is too large to upload. Maximum allowed size is 2MB');

					return $this->redirectToReferer($request);
				}

				$trailer->setThumbnail($fileName);
			}

			try {
				$this->entityManager->persist($trailer);
				$this->entityManager->flush();

				$this->baseController->flashbag->add('primary', 'The trailer ' . $trailer->getTitle() . ' was successfully edited');
				return $this->redirectToRoute('admin_trailers');
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}
		}

		return $this->render('/admin/homepage/trailers/edit.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/admin/trailers/toggle-active/{id}", name="toggle_active_trailer", schemes={"http"})
	 */
	public function toggleActive($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailer = $this->trailerRepository->find($id);

		$trailer->setActive(!$trailer->getActive());

		try {
			$this->entityManager->persist($trailer);
			$this->entityManager->flush();

			$message = 'activated';

			if ($trailer->getActive() === FALSE) {
				$message = 'deactivated';
			}

			$this->baseController->flashbag->add('primary', 'Trailer ' . $trailer->getTitle() . ' was successfully ' . $message);
			return $this->redirectToRoute('admin_trailers');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/remove-multiple/trailers", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->trailerRepository->find($id);

			$this->getAccessUtilities()->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . $complaint->getThumbnail());
			$this->getAccessUtilities()->filesystem->remove($this->getParameter('trailer_thumbnails_directory') . 'normal/' . $complaint->getThumbnail());

			$this->entityManager->remove($complaint);
			$this->entityManager->flush();
		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}

	/**
	 * @Route("/admin/toggle-multiple/trailers", schemes={"http"})
	 */
	public function toggleMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastSuperadmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->trailerRepository->find($id);
			$complaint->setActive(!$complaint->getActive());

			$this->entityManager->persist($complaint);
			$this->entityManager->flush();

		}
		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items\' states were switched');
		return $this->redirectToReferer($request);
	}

	/**
	 * @Route("/admin/trailers/change-order", name="admin_trailer_order", schemes={"http"})
	 */
	public function changeOrder(Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$trailers = $this->trailerRepository->findBy(['active' => 1], ['ordering' => 'ASC']);

		$ids = $request->query->get('ids');

		if ($ids != NULL) {
			$ids = str_replace('[', '', $ids);
			$ids = str_replace(']', '', $ids);
			$ids = explode(',', $ids);

			$allTrailers = new ArrayCollection($this->trailerRepository->findAll());
			$trailers_ = [];
			$i = 1;

			foreach ($ids as $id) {
				$trailer = $this->trailerRepository->find($id);
				$trailer->setOrdering($i);
				$trailers_[] = $trailer;
				try {
					$this->entityManager->persist($trailer);
					$this->entityManager->flush();
					$allTrailers->removeElement($trailer);
					$i++;
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			foreach ($allTrailers as $t) {
				$t->setOrdering($i);
				try {
					$this->entityManager->persist($t);
					$this->entityManager->flush();

					$i++;
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			}

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'Trailers successfully reordered');
			return $this->redirectToRoute('admin_trailers');
		}

		return $this->render('admin/homepage/trailers/change_order.html.twig', [
			'trailers' => $trailers
		]);
	}
}
