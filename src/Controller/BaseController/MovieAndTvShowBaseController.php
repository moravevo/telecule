<?php

namespace App\Controller\BaseController;

use App\Controller\AdminBaseController;
use App\Repository\GenreRepository;
use App\Controller\BaseController;
use App\Entity\CastMember;
use App\Entity\HomepageRecommendation;
use App\Entity\MovieAndTvShow;
use App\Repository\CelebrityRepository;
use App\Repository\CastMemberRepository;
use App\Repository\HomepageRecommendationRepository;
use App\Repository\MovieAndTvShowRepository;
use App\Repository\UserReviewRepository;
use App\Service\FileUploader;
use App\Service\Slugger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class MovieAndTvShowBaseController extends AdminBaseController
{
	public $entityManager;
	public $filesystem;
	public $flashbag;
	public $castMemberRepository;
	public $celebrityRepository;
	public $homepageRecommendationRepository;
	public $movieAndTvShowRepository;
	public $userReviewRepository;
	public $session;
	public $slugger;
	public $user;
	public $genreRepository;

	public function __construct(
		EntityManagerInterface $entityManagerInterface,
		CastMemberRepository $castMemberRepository,
		CelebrityRepository $celebrityRepository,
		HomepageRecommendationRepository $homepageRecommendationRepository,
		MovieAndTvShowRepository $movieAndTvShowRepository,
		UserReviewRepository $userReviewRepository,
		GenreRepository $genreRepository
	)
	{
		$this->entityManager = $entityManagerInterface;
		$this->filesystem = new Filesystem();
		$this->genreRepository = $genreRepository;
		$this->castMemberRepository = $castMemberRepository;
		$this->celebrityRepository = $celebrityRepository;
		$this->homepageRecommendationRepository = $homepageRecommendationRepository;
		$this->movieAndTvShowRepository = $movieAndTvShowRepository;
		$this->userReviewRepository = $userReviewRepository;
		$this->session = new Session();
		$this->slugger = new Slugger();
		$this->flashbag = $this->session->getFlashBag();
		$this->user = $this->session->get('user');
	}

	/**
	 * @param MovieAndTvShow $movieOrTvShow
	 * @return object
	 */
	public function getSimilar($movieOrTvShow)
	{
		return $this->movieAndTvShowRepository->findSimilar($movieOrTvShow);
	}

	/**
	 * @param MovieAndTvShow $movie
	 * @param Request $request
	 * @return RedirectResponse
	 */
	public function recommendMovie($movie, Request $request, $anchor = NULL)
	{
		$homepageRecommendation = new HomepageRecommendation();
		$homepageRecommendation->setMovieOrTvShow($movie);
		$homepageRecommendation->setOrdering($this->homepageRecommendationRepository->findHighestOrdering() + 1);

		try {
			$this->entityManager->persist($homepageRecommendation);
			$this->entityManager->flush();

			$this->flashbag->add('primary', $movie->getTitle() . ' was successfully added to recommendations');
			return $this->redirectToReferer($request, $anchor);
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @param MovieAndTvShow $movie
	 * @param Request $request
	 * @return RedirectResponse
	 */
	public function unrecommendMovie($movie, Request $request, $anchor = NULL)
	{
		$movieRecommendation = $movie->getHomepageRecommendation();

		try {
			$this->entityManager->remove($movieRecommendation);
			$this->entityManager->flush();

			$this->flashbag->add('primary', $movie->getTitle() . ' was successfully removed from recommendations');
			return $this->redirectToReferer($request, $anchor);
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/movies/search", schemes={"http"})
	 */
	public function searchMovies(Request $request)
	{
		$data = $request->getContent();
		$responses = array_map(function ($response) {
			$premiere = $response['premiere'];

			if ($premiere != NULL) {
				$premiere = $premiere->format('Y');
			}

			return array(
				'id' => $response['id'],
				'displayValue' => $response['title'],
				'premiere' => $premiere,
				'targetUrl' => 'movie/' . $response['slug']
			);
		}, $this->movieAndTvShowRepository->findByTitleAndType($data, 'movie'));

		return new JsonResponse([
			$responses
		]);
	}

	/**
	 * @Route("/admin/tv-shows/search", schemes={"http"})
	 */
	public function searchTvShows(Request $request)
	{
		$data = $request->getContent();
		$responses = array_map(function ($response) {
			$premiere = $response['premiere'];

			if ($premiere != NULL) {
				$premiere = $premiere->format('Y');
			}

			return array(
				'id' => $response['id'],
				'displayValue' => $response['title'],
				'premiere' => $premiere,
				'targetUrl' => 'tv-show/' . $response['slug']
			);
		}, $this->movieAndTvShowRepository->findByTitleAndType($data, 'tv_show'));

		return new JsonResponse([
			$responses
		]);
	}

	/**
	 * @param FormInterface $form
	 * @param Request $request
	 * @param MovieAndTvShow $movieOrTvShow
	 */
	public function handleUserReview($form, $request, $movieOrTvShow)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$userReview = $form->getData();

			$exits = $this->userReviewRepository->findOneBy(
				[
					'user' => $this->user,
					'movieOrTvShow' => $movieOrTvShow
				]
			);

			if (!$exits) {
				$userReview->setUser($this->user);
				$userReview->setMovieOrTvShow($movieOrTvShow);
				$userReview->setType($movieOrTvShow->getType());

				try {
					$this->entityManager->merge($userReview);
					$this->entityManager->flush();
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}

				return TRUE;
			} else {
				$type = 'movie';
				if ($movieOrTvShow->getType() === 'tv_show') {
					$type = 'TV Show';
				}
				$form->addError(new FormError('You have already reviewed this ' . $type));

				return FALSE;
			}
		}
	}

	/**
	 * @param FormInterface $form
	 * @param Request $request
	 * @param MovieAndTvShow $movieOrTvShow
	 * @param FileUploader $fileUploader
	 * @return boolean
	 */
	public function addMovieOrTvShow($form, $request, $movieOrTvShow, $fileUploader)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();
			$slug = $this->slugger->createSlug($movieOrTvShow->getTitle());

			if ($movieOrTvShow->getPremiere() === NULL) {
				$movieOrTvShow->setPremiere(NULL);
			}

			if ($this->movieAndTvShowRepository->findBy(['slug' => $slug])) {
				$slug = $slug . '-' . $movieOrTvShow->getPremiere()->format('Y');
			}

			$movieOrTvShow->setSlug($slug);

			$fileName = 'thumbnail.jpeg';
			$file = $form->get('thumbnail')->getData();

			if ($file != NULL) {
				$fileName = $fileUploader->upload($file, $this->getParameter($type . '_thumbnails_directory'), 160, 225, 736, 1035);

				if ($fileName === FALSE) {
					return FALSE;
				}
			}

			$movieOrTvShow->setThumbnail($fileName);

			try {
				$this->entityManager->persist($movieOrTvShow);
				$this->entityManager->flush();
				$typeReadable = ($type === 'movie') ? 'Movie' : 'TV Show';

				$this->session->getFlashBag()->add('primary', $typeReadable . ' was successfully added');
				return TRUE;
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());
				return $this->redirectToReferer($request);
			}

		}

		return FALSE;
	}

	/**
	 * @param MovieAndTvShow $movieOrTvShow
	 */
	public function removeMovieOrTvShow($movieOrTvShow, $request)
	{
		$type = $movieOrTvShow->getType();

		if ($movieOrTvShow->getThumbnail() != 'thumbnail.jpeg') {
			$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $movieOrTvShow->getThumbnail());
			$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $movieOrTvShow->getThumbnail());
			$this->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $movieOrTvShow->getThumbnail());
		}

		try {
			$this->entityManager->remove($movieOrTvShow);
			$this->entityManager->flush();

			$typeReadable = ($type === 'movie') ? 'Movie' : 'TV Show';

			$this->flashbag->add('primary', $typeReadable . ' was successfully deleted');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @param FormInterface $form
	 * @param Request $request
	 * @param MovieAndTvShow $movieOrTvShow
	 * @param CastMember $castMember
	 * @return boolean
	 */
	public function assignCastMember($form, $request, $movieOrTvShow, $castMember)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$castMember->setCelebrity($form->get('celebrity')->getData());
			$castMember->setMovieOrTvShow($movieOrTvShow);
			$castMember->setPremiere($movieOrTvShow->getPremiere());

			$exists = $this->castMemberRepository->findOneBy([
				'movieOrTvShowId' => $movieOrTvShow->getId(),
				'celebrityId' => $castMember->getCelebrity()->getId()
			]);

			if (!$exists) {
				try {
					$this->entityManager->persist($castMember);
					$this->entityManager->flush();

					$this->session->getFlashBag()->add('primary', 'The cast member was successfully added');

					return TRUE;
				} catch (\Exception $e) {
					$this->session->getFlashBag()->add('danger', $e->getMessage());
					return $this->redirectToReferer($request);
				}
			} else {
				$form->get('celebrity')->addError(new FormError('This person is already a cast member for this movie'));

				return FALSE;
			}
		}

		return FALSE;
	}

	/**
	 * @param CastMember $castMember
	 * @return boolean
	 */
	public function removeCastMember($castMember, $request)
	{
		try {
			$this->entityManager->remove($castMember);
			$this->entityManager->flush();

			return TRUE;
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @param FormInterface $form
	 * @param Request $request
	 * @param CastMember $castMember
	 * @return boolean
	 */
	public function editCastMember($form, $request, $castMember)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$this->entityManager->persist($castMember);
				$this->entityManager->flush();

				$this->session->getFlashBag()->add('primary', 'The cast member was successfully edited');

				return TRUE;
			} catch (\Exception $e) {
				$this->session->getFlashBag()->add('danger', $e->getMessage());

				return $this->redirectToReferer($request);
			}
		}

		return FALSE;
	}

	/**
	 * @param MovieAndTvShow $movieOrTvShow
	 * @return array $missing
	 */
	public function getMissingInfo($movieOrTvShow)
	{
		$missing = [];

		if ($movieOrTvShow->getThumbnail() === 'thumbnail.jpeg') {
			$missing[] = 'Default thumbnail';
		}

		if ($movieOrTvShow->getShortDescription() === NULL) {
			$missing[] = 'No short description';
		}

		if ($movieOrTvShow->getDescription() === NULL) {
			$missing[] = 'No description';
		}

		if ($movieOrTvShow->getPremiere() === NULL) {
			$missing[] = 'No premiere';
		}

		if ($movieOrTvShow->getType() === 'movie' && $movieOrTvShow->getLength() === NULL) {
			$missing[] = 'No length';
		}

		if ($movieOrTvShow->getGenres()[0] === NULL) {
			$missing[] = 'No genres assigned';
		}

		if ($movieOrTvShow->getRating() === NULL) {
			$missing[] = 'Not yet rated';
		}

		if ($movieOrTvShow->getCastMembers()[0] === NULL) {
			$missing[] = 'No assigned cast members';
		}

		return $missing;
	}

	/**
	 * @Route("/chart/movieAndTvShowVisits", schemes={"http"})
	 */
	public function getChartMovieAndTvShowVisits()
	{
		$moviesAndTvShows = $this->movieAndTvShowRepository->findBy(
			[],
			[
				'visits' => 'DESC'
			],
			10
		);

		$response = [];

		foreach ($moviesAndTvShows as $movieAndTvShow) {
			$response[] = [
				'title' => $movieAndTvShow->getTitle(),
				'visits' => $movieAndTvShow->getVisits()
			];
		}

		return new JsonResponse([
			$response
		]);
	}

	/**
	 * @Route("/admin/remove-multiple/movies-or-tv-shows", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastModerator()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->movieAndTvShowRepository->find($id);
			$type = $complaint->getType();

			if ($complaint->getThumbnail() != 'thumbnail.jpeg') {
				$this->getAccessUtilities()->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . $complaint->getThumbnail());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'small/' . $complaint->getThumbnail());
				$this->getAccessUtilities()->filesystem->remove($this->getParameter($type . '_thumbnails_directory') . 'normal/' . $complaint->getThumbnail());
			}

			$this->entityManager->remove($complaint);
			$this->entityManager->flush();
		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}