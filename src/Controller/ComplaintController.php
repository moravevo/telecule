<?php

namespace App\Controller;

use App\Service\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ComplaintRepository;
use Doctrine\ORM\EntityManagerInterface;

class ComplaintController extends AdminBaseController
{
	private $allowed = [2, 3];
	private $complaintRepository;
	private $entityManager;

	public function __construct(
		ComplaintRepository $complaintRepository,
		EntityManagerInterface $entityManager
	)
	{
		$this->complaintRepository = $complaintRepository;
		$this->entityManager = $entityManager;
	}

	/**
	 * @Route("/admin/complaints", name="admin_complaints", schemes={"http"})
	 */
	public function admin(Request $request, Paginator $paginator)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastAdmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$complaints = $this->complaintRepository->findAll();

		$paginated = $paginator->paginate($complaints, $request->query->get('page'), $request->query->get('perPage'), 5, '/admin/complaints');

		return $this->render('/admin/complaints/index.html.twig', [
			'complaints' => $paginated[0],
			'pages' => $paginated[1],
			'currentPage' => $paginated[2],
			'lastPage' => $paginated[3],
			'url' => $paginated[4],
			'perPage' => $paginated[5],
			'count' => count($complaints)
		]);
	}

	/**
	 * @Route("/admin/complaints/delete/{id}", name="delete_complaint", schemes={"http"})
	 */
	public function delete($id, Request $request)
	{
		$this->get('twig')->addGlobal('user', $this->getAccessUtilities()->session->get('user'));
		$this->get('twig')->addGlobal('flashes', $this->getAccessUtilities()->session->getFlashBag()->all());

		if (!$this->isLoggedIn() || !$this->isAtLeastAdmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$complaint = $this->complaintRepository->find($id);

		try {
			$this->entityManager->remove($complaint);
			$this->entityManager->flush();

			$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'The complaint was successfully deleted');
			return $this->redirectToRoute('admin_complaints');
		} catch (\Exception $e) {
			$this->session->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirectToReferer($request);
		}
	}

	/**
	 * @Route("/admin/remove-multiple/complaints", schemes={"http"})
	 */
	public function removeMultiple(Request $request)
	{
		if (!$this->isLoggedIn() || !$this->isAtLeastAdmin()) {
			$this->getAccessUtilities()->session->getFlashBag()->add('warning', 'You are not allowed to view the required page');
			return $this->redirectToRoute('homepage');
		}

		$ids = $request->query->get('ids');

		if ($ids === NULL) {
			$this->getAccessUtilities()->session->getFlashBag()->add('danger', 'Something went wrong');
			return $this->redirectToReferer($request);
		}

		$ids = str_replace('[', '', $ids);
		$ids = str_replace(']', '', $ids);
		$ids = explode(',', $ids);

		foreach ($ids as $id) {
			$complaint = $this->complaintRepository->find($id);

			$this->entityManager->remove($complaint);
			$this->entityManager->flush();

		}

		$this->getAccessUtilities()->session->getFlashBag()->add('primary', 'All selected items were successfully deleted');
		return $this->redirectToReferer($request);
	}
}